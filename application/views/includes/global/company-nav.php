<div id="company-nav-container"<?php echo ($this->uri->segment(1) == 'company' || $this->uri->segment(1) == 'grocers' || $this->uri->segment(1) == 'restaurants') ? 'style="display:block;"' : ''; ?>>
	<div id="company-nav">
		<nav>
			
			<ul>
				<li class="on first"><a href="/company/awards">Awards</a></li>
				<li><a href="/company/community">Community</a></li>
				<li><a href="/company/contact-us">Contact Us</a></li>
				<li><a href="/company/employment">Employment</a></li>
				<li><a href="/company/faqs">FAQS</a></li>
				<li class="last"><a href="/company/food-service">Food Service</a></li>
				<li><a href="/company/about-us">About Us</a></li>
				<li><a href="/company/media-center">Media Center</a></li>
				<li><a href="/company/related-links">Related Links</a></li>
				<li><a href="/company/school-milk-program">School Milk Program</a></li>
				<li class="last"><a href="http://hilanddairy.com/image-library/" target="_blank">Image Library</a>

			</ul>
			
			<!--
			<ul class="top">
				<li class="on"><a href="/company/awards">Awards</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/community">Community</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/contact-us">Contact Us</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/employment">Employment</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/faqs">FAQS</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/food-service">Food Service</a></li>
			</ul>
			<ul class="bottom">
				<li><a href="/company/about-us">About Us</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/media-center">Media Center</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/related-links">Related Links</a></li>
				<li class="spacer">|</li>
				<li><a href="/company/school-milk-program">School Milk Program</a></li>
			</ul>
			-->
		</nav>
	</div>
</div>