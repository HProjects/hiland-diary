<div class="content-container">
	<div class="home-recipes">
		<h2><span class="hidden">Family Fun Getaway</span></h2>
		<a class="home-img" href="/familyfun">
			<img src="/img/mobile-home-familyfun.jpg" alt="Enter to Win a Family Fun Getaway" />
		</a>
	</div>
	
	<div class="home-recipes">
		<h2><span class="hidden">Recipes</span></h2>
		<a class="home-img" href="/recipes">
			<img src="/img/mobile-home-recipe.jpg" alt="View all recipes" />
		</a>
	</div>
	
	<div class="home-products">
		<h2><span class="hidden">Products</span></h2>
		<a class="home-img" href="/products"><img src="/img/mobile-home-product.jpg" alt="View all products" /></a>
	</div>
	
	<div class="home-coupons">
		<h2><span class="hidden">Coupons</span></h2>
		<a class="home-img" href="/coupons"><img src="/img/mobile-home-coupon.jpg" alt="View available coupons" /></a>
	</div>
	
	<hr style="margin:30px 0;" />
	
	<div class="home-mission">
		<h2><span class="hidden">Our Mission</span></h2>
		<a class="home-img" href="/our-mission"><img src="/img/mobile-home-mission.jpg" alt="Our Mission" /></a>
	</div>
	
	<div class="home-promos">
		<h2><span class="hidden">Promotions</span></h2>
		<a class="home-img" href="/out-and-about"><img src="/img/mobile-home-promos.jpg" alt="Promotions" /></a>
	</div>
	
	<hr style="margin:30px 0;" />
	
	<div class="home-recipe-spotlight">
		<h2><span class="hidden">Recipes</span></h2>
		<a class="home-img" href="/recipes/appetizers/layered-bean-dip">
			<img src="/img/mobile-home-recipe-spotlight.jpg" alt="Make this recipe" />
		</a>
	</div>
</div>