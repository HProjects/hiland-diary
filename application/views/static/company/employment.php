<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- EMPLOYMENT -->
		<h1 class="fontface">Employment</h1>
		<p>Hiland Dairy Foods Company, LLC is an award-winning company that offers excellent pay and benefits. We look for applicants with a stable work history, related job experience and a positive attitude. Successful applicants must pass a drug screening as well as a physical. Applications are accepted for current job openings only. Contact one of <a href="/company/about-us">our local plants</a> and not this website, about employment opportunities.</p>
		<p>Hiland Dairy Foods Company, LLC is an Equal Opportunity Employer.</p>
		
		<h2>Job Categories</h2>
		<p>All Hiland Dairy manufacturing locations and distribution centers include these types of positions:</p>
			<ul>
				<li>Clerical</li>
				<li>Manufacturing</li>
				<li>Production</li>
				<li>Sales</li>
				<li>Distribution</li>
			</ul>
			
		<h2>Benefits</h2>
		<p>Hiland Dairy provides benefits to our full-time associates for a reasonable cost. These benefits include:</p>
			<ul>
				<li>Medical Insurance with Prescription Card</li>
				<li>Dental Insurance</li>
				<li>Life Insurance</li>
				<li>Section 125 Cafeteria Plan</li>
				<li>Pension Plan</li>
				<li>Paid Vacation</li>
				<li>Paid Holidays</li>
				<li>Educational Assistance</li>
			</ul>
<!-- =end EMPLOYMENT -->

	</div>
</div>