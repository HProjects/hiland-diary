﻿<div id="content-container">
			<div id="content" class="home">
			
	<!-- MAIN ROTATOR -->	
				<div class="slideWrap"><div class="main-rotator">
					<a href="#" class="prev-button"><span class="hidden">RotateLeft</span></a>
					<div class="main-rotator-wrapper">
						<div class="feature-ads first">
							<div class="ad main-ad">
								<img src="/img/ad_large_famfun.jpg"  alt="" />
								<a href="/familyfun"><span class="hidden">Enter Now</span></a>
							</div>
							<div class="ad top-ad">
								<img src="/img/ad1_ur_nohormone2.jpg" alt="" />
								<a href="/our-mission"><span class="hidden">Learn More</span></a>
							</div>
							<div class="ad bottom-ad">
								<img src="/img/ad1_bl_social2.jpg" alt="" />
								<a href="http://www.facebook.com/HilandDairy" target="_blank"><span class="hidden">Like Us</span></a>
							</div>
						</div>
						
						
						<div class="feature-ads third">
							<div class="ad main-ad">
								<img src="/img/ad3_lg_pink.jpg"  alt="" />
								<a href="/pink"><span class="hidden">More Information</span></a>
							</div>
							<div class="ad top-ad">
								<a href="http://www.facebook.com/HilandDairy" target="_blank"><img src="/img/ad3_ur_pink.jpg" alt="" border="0" /></a>
							</div>
							<div class="ad bottom-ad">
								<a href="/pink"><img src="/img/ad3_br_pink.jpg" alt="" border="0" /></a>
							</div>
						</div>
						
						
						<div class="feature-ads second">
							<div class="ad main-ad">
								<img src="/img/homepage_ad_large_lactose.jpg"  alt="" />
								<a href="/products/lactose-free-milk/vanilla-and-almond"><span class="hidden">Learn More</span></a>
								<div class="ad main-ad-b"><a href="/products/iced-coffee/mocha"><span class="hidden">Learn More</span></a></div>
							</div>
							<!--
<div class="ad main-ad">
								<img src="/img/homepage_ad_large_fettucine.jpg" alt="" />
								<a href="/recipes/pasta/spicy-sweet-pepper-fettuccine"><span class="hidden">Make this recipe</span></a>
							</div>
-->
							<div class="ad top-ad">
								<img src="/img/ad2_ur_refuel2.jpg" alt="" />
								<a href="/out-and-about"><span class="hidden">Refuel with Chocolate Milk</span></a>
							</div>
							<div class="ad bottom-ad">
								<img src="/img/ad2_br_coupons3.jpg" alt="" />
								<a href="/coupons"><span class="hidden">Start Saving</span></a>
							</div>
						</div>
					</div>
					<a href="#" class="next-button"><span class="hidden">RotateRight</span></a>
				</div></div>
	<!-- =end MAIN ROTATOR -->				
				
	<!-- PRODUCTS ROTATOR -->		
				<div class="rotator-products">
					<a href="#" class="prev-product-button"><span class="hidden">Rotate Left</span></a>
					<div class="rotator-products-wrapper">
						<div class="products-list">
							<div class="single-product"><a href="/products/milks/2-percent-white"><img src="/img/home_prod_milk.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/juice/apple-juice"><img src="/img/home_prod_applejuice.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/creams-buttermilks/half-and-half"><img src="/img/home_prod_halfnhalf.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/red-diamond-tea/unsweet-tea"><img src="/img/home_prod_tea.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/cheeses/shredded-mozzarella"><img src="/img/home_prod_cheese.png" width="168" height="146"></a></div>
						</div>
						<div class="products-list">
							<div class="single-product"><a href="/products/butters/salted-butter-block"><img src="/img/home_prod_butter.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/cottage-cheese/large-curd-cottage-cheese"><img src="/img/home_prod_cottagecheese.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/dips/jalapeno-fiesta-dip"><img src="/img/home_prod_dips.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/red-diamond-tea/green-tea"><img src="/img/home_prod_greentea.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/yogurts/probiotic-healthwise-yogurt-vanilla"><img src="/img/home_prod_healthwise.png" width="168" height="146"></a></div>
						</div>
						<div class="products-list">
							<div class="single-product"><a href="/products/creams-buttermilks/whipping-cream-40-percent"><img src="/img/home_prod_heavywhip.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/sour-cream/all-natural-sour-cream"><img src="/img/home_prod_sourcream.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/yogurts/lowfat-yogurt-raspberry"><img src="/img/home_prod_yogurt.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/yogurts/yophoria-yogurt-smoothie-pina-colada"><img src="/img/home_prod_yophoria.png" width="168" height="146"></a></div>
							<div class="single-product"><a href="/products/juice/orange-juice"><img src="/img/home_prod_orangejuice.png" width="168" height="146"></a></div>
						</div>
					</div>
					<a href="#" class="next-product-button"><span class="hidden">Rotate Right</span></a>
				</div>
	<!-- =end PRODUCTS ROTATOR -->
				
				<div id="home_tert_ads">
					<div id="home_tert_coups">
						<img src="/img/home_hdr_coups2.jpg" width="448" height="172" border="0" />
						<div class="tertads_in"><form action="http://hilanddairy.cmail1.com/.aspx/s/84919/" method="post"><p><label for="l84919-84919">E-mail:</label><br /><input name="cm-84919-84919" type="email" id="cm-84919-84919" size="55" required /></p><p><label for="zipcode">Zip Code:</label><br /><input type="text" name="cm-f-89371" id="zipcode" size="55" required /></p><p><input type="submit" value="Subscribe" id="sub" name="sub" /></p>
           					</form>
							<!--
<form>
								Name<br />
								<input type="text" name="name" size="55">
								<p>E-mail Address<br />
								<input type="text" name="email" size="55"></p>
								<p><a href="#" id="home_btn_startsaving"><span>Start Saving</span></a></p>
							</form>
-->
						</div>
					</div>
					<div id="home_tert_recipes">
						<div id="homerecipeshighlight" style="background:url('/img/home_hdr_recipes.jpg') no-repeat;">
							<img src="/img/home_recipe_spotlight.png" width="448" height="172" border="0" />
						</div>
						<div class="tertads_in">
							<div class="rechead">New Red Potato Salad</div>
							<!--<div class="recipeofthemonthhome"><a href="/recipes/appetizers/layered_bean_dip"><img src="/img/recipes/home_rotm_01_2013.jpg" width="90" height="56" border="0"></a></div>--><p>Be the hit of any picnic with this easy and delicious potato salad!</p>
							<p><a href="/recipes/salads/new-red-potato-salad" id="home_btn_makethis"><span>Make This</span></a>
							<div class="rectimes">
							Prep Time: 15 minutes</br>
							Cook Time: 25 minutes
							</div>
						</div>
					</div>
				</div>
			
				

		</div>
		<script src="/js/jquery-cycle.js" type="text/javascript"></script>
		<script>
			//-------------------------------------------------
			// HOME ROTATOR
			//-------------------------------------------------
				$('.main-rotator .main-rotator-wrapper').cycle({
					fx: 'scrollHorz',
					speed: 300,
					timeout: 8000,
					fit: true,
					prev: '.prev-button',
					next: '.next-button',
					pause: true
				});
			//-------------------------------------------------
			// ROTATOR PRODUCTS
			//-------------------------------------------------
				$('.rotator-products .rotator-products-wrapper').cycle({
					fx: 'scrollHorz',
					speed: 300,
					timeout: 0,
					fit: true,
					prev: '.prev-product-button',
					next: '.next-product-button'
				});
		</script>
			<br clear="all">
	</div>