<div class="content-container">
	<h1 class="fontface">Promotions</h1>
	<p>At Hiland Dairy, we know the importance of being a good neighbor and community member. We actively support local and regional causes, events and organizations that seek to help make our communities better places to live.</p>
	<h2>Refuel with Chocolate Milk</h2>
	<p>Did you know that Hiland Dairy chocolate milk is an effective way to refuel your body after exercise? The unique mix of nutrients in delicious Hiland Dairy Lowfat or Fat Free Chocolate Milk makes this drink the ideal post-workout choice! Visit <a href="http://www.refuelwithchocolatemilk.com/" target="_blank">refuelwithchocolatemilk.com</a> to learn about the science behind chocolate milk and why it is the ideal way to refuel.</p>
	<h2>One More Serving &ndash; One Big Difference</h2>
	<p>There&rsquo;s a nutrient gap in America because many of our families are filling up on empty calories instead of essential nutrients. In fact, more than nine out of 10 Americans aren&rsquo;t getting the essential nutrients they need, and don&rsquo;t even know it.</p>
	<p>We know that keeping your family happy and healthy is your number one priority, and it&rsquo;s no easy task. That&rsquo;s why Hiland Dairy and America&rsquo;s Milk Processors are working with Feeding America to give away more than one million servings of milk to families all across the country in the Pour One More campaign.</p>
	<p>Because whether it&rsquo;s in cereal, a latte or simply in a glass, adding one more serving of low-fat milk a day to every family member&rsquo;s diet is an easy way to help build strong families. After all, milk contains nine essential nutrients including calcium, vitamin D and potassium, which most Americans are missing. And with one more serving of milk, you&rsquo;ll be nourishing your family and taking one easy step toward closing your family&rsquo;s nutrient gap.</p>
	<p><a href="http://www.whymilk.com/" target="_blank">Find out more about the power of pouring one more</a> and how you can get a free serving.</p>
	
	<hr style="margin:30px 0px;" />
	
	<h2>Raise Your Glass for Chocolate Milk</h2>
	<p>Did you know that chocolate milk is the most popular milk choice in schools? Some are looking to take away low-fat chocolate milk from cafeterias, but this could do more nutritional harm than good since many kids would drink less milk, therefore getting fewer nutrients if it is removed.<a href="http://www.raiseyourhand4milk.com/" target="_blank">Get more information and sign the petition to keep chocolate milk in schools.</a></p>
	
	<hr style="margin:30px 0px;" />
	
	<h2>Milk &ndash; Building Strong Families</h2>
	<p>Mom&rsquo;s top priority is her family&rsquo;s wellness &ndash; and milk is the perfect ally to keep her family happy and healthy. Visit <a href="http://www.whymilk.com/" target="_blank">whymilk.com</a> for more information on why nutrient-rich milk is an essential part of a family&rsquo;s wellness routine.
	<a href="http://www.whymilk.com/" target="_blank">Learn more</a></p>
	
	<hr style="margin:30px 0px;" />
	
	<h2>Milk is a Nutrient Powerhouse</h2>
	<p>Milk is packed with nine essential nutrients that are important for you and your family. This nutrient-laden liquid is a nutritional bargain &ndash; offering more nutrition for your dollar than virtually any other beverage you can buy. <a href="http://www.whymilk.com/natures_wellness_drink.php" target="_blank">Learn more</a></p>
</div>