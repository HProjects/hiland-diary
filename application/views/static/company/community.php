<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- COMMUNITY -->
		<h1 class="fontface"><img src="/img/community-icons.gif" width="280" height="233" align="right" usemap="#social" />Community</h1>
		<p>Join the conversation about your favorite Hiland Dairy products and recipes!</p>
		<p>Follow us on <a href="https://twitter.com/HilandDairy">Twitter</a>, like us on <a href="https://www.facebook.com/HilandDairy">Facebook</a> for special deals and announcements and pin your favorite recipes to <a href="http://pinterest.com/hilanddairy/">Pinterest</a> today.</p>
		
		<h2>Hiland Kicks off Spring at Little Rock Zoo</h2>
		<p>Hiland Dairy helped kick off Spring at the Little Rock Zoo. Hiland announced it will help the zoo reduce price of admission for the upcoming "Dollar Day" at the Zoo- where admission for the general public will be $1. 
		</p>
	
	<!-- Zoo Images -->
		<div class="farm-owners">
			<p><strong>Zoo Photos</strong></p>
			<a href="/img/zoo-1.jpg" class="zoo colorbox" rel="zoo"><img src="/img/zoo-1-sm.jpg" alt="" /></a>
			<a href="/img/zoo-2.jpg" class="zoo colorbox" rel="zoo"><img src="/img/zoo-2-sm.jpg" alt="" /></a>
			<a href="/img/zoo-3.jpg" class="zoo colorbox" rel="zoo"><img src="/img/zoo-3-sm.jpg" alt="" /></a>
			<a href="/img/zoo-4.jpg" class="zoo colorbox" rel="zoo"><img src="/img/zoo-4-sm.jpg" alt="" /></a>
			<a href="/img/zoo-5.jpg" class="zoo colorbox" rel="zoo"><img src="/img/zoo-5-sm.jpg" alt="" /></a>
			<p class="img-caption"></p>
		</div>
	<!-- End Zoo Images -->
	
<map name="social">
  <area shape="rect" coords="81,41,122,80" href="http://twitter.com/hilanddairy" target="_blank" alt="Twitter">
  <area shape="rect" coords="144,39,186,80" href="http://www.facebook.com/HilandDairy" target="_blank" alt="Facebook">
  <area shape="rect" coords="200,110,239,149" href="http://pinterest.com/hilanddairy/" target="_blank" alt="Pinterest">
</map> 

<!-- =end COMMUNITY -->

	</div>
	<script src="/js/jquery.colorbox-min.js" type="text/javascript"></script>
	<script>	
			$('.zoo.colorbox').colorbox({rel:'zoo'});
	</script>
</div>