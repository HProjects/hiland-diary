<div id="content-container">
	<div id="content" class="sign-up-form">
		
		<?php if($this->input->cookie('reset') === '0'): ?>
			<p>An email has been sent to your inbox. Follow the instructions to reset your password.</p>
		<?php else: ?>
		<form action="/account/send-password-email" method="post">
			<ul class="formsignup">
				<fieldset class="login-info">
					<h2>Reset Password</h2>
					<p>Enter your email to receive instructions on resetting your password.</p>
					<li>
						<label for="email-address">Email Address</label><br />
						<input id="email-address" name="email-address" value="" type="email" placeholder="Email" required />
					</li>
				</fieldset>
				
				<fieldset class="newsletter-submit">
					<li>
						<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
					</li>
				</fieldset>
			</ul>
		</form>
		
		<?php endif; ?>
</div><!--end content-container-->