<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
		<?php echo $sidebar_newsletter_g; ?>
	</div>
	<div id="content" class="grocers-wrapper">

		<h1 class="fontface">Satisfy Your Shoppers with Hiland Dairy</h1>


<p>Hiland Dairy delivers the finest, freshest dairy products to your stores. Find the Hiland Dairy plant in your area, download the latest images of our products for your circulars, and sign up to receive our eNewsletter for grocers - all right here!</p> 

<!--
<h2>Sign Up for our eNewsletter</h2>

<p>Get product updates, packaging changes, and industry news emailed to your inbox with the Hiland Dairy eNewsletter - Grocer edition! We create each short, easy-to-read issue with your time and bottom-line in mind.</p> 

<p><a href="#" class="signupnow_newsletter"><span>Sign Up Today!</span></a></p>
-->


<h2>Hiland Dairy Has a Plant Near You</h2>

<p><img src="/img/land_grocer.jpg" width="320" height="425" align="right">Our manufacturing plants are strategically placed to deliver our fresh, wholesome dairy products to your stores quickly and safely. <a href="/company/contact-us">Contact us</a> to get in touch with the Hiland Dairy manufacturing plant nearest to you.</p>

<p><strong> Hiland Dairy Plant Locations</strong></p>

	<ul>
		<li>Chandler, Oklahoma</li>
		<li>Fort Smith, Arkansas</li>
		<li>Fayetteville, Arkansas</li>
		<li>Kansas City, Missouri</li>
		<li>Little Rock, Arkansas</li>
		<li>Norfolk, Nebraska</li>
		<li>Norman, Oklahoma</li>
		<li>Springfield, Missouri</li>
		<li>Wichita, Kansas</li>
		<li>Des Moines, Iowa</li>
		<li>Kansas City, Missouri</li>
		<li>Omaha, Nebraska</li>
	</ul>
	
<h2>Product Images</h2>

<p>Looking for Hiland Dairy product images and logos? <a href="http://hilanddairy.com/image-library/" target="_blank">Visit our new image library!</a></p>


<h2>Product Questions? Inventory needs? </h2>
<p><a href="/company/contact-us">Contact us</a> today for answers to your questions about which Hiland Dairy products are available in your area. We’ll respond to your inquiry right away! </p>
		



	</div>
</div>