
<div class="recipe-directions">
	<div>
		<img src="http://hilanddairy-drupal.envoydev.com/sites/default/files/<?php echo $data->field_recipe_image->und[0]->filename; ?>" />
	</div>
	<?php if( isset($data->field_prep_time->und[0]->value) ): ?>
	<div><?php echo $data->field_prep_time->und[0]->value; ?></div>
	<div><?php echo $data->field_cook_time->und[0]->value; ?></div>
	<div><?php echo $data->field_servings->und[0]->value; ?></div>
	<?php endif; ?>
	<div>
		<?php echo $data->field_recipe_directions->und[0]->value; ?>
	</div>
</div>