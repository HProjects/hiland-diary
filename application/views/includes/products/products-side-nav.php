<div id="subnav">
	<div class="secondary">
		<ul class="menu">
                
		<!-- BUTTER -->
                <li<?php echo ($this->uri->segment(2) == 'butters') ? ' class="on"' : ''; ?>>
                	<a href="/products/butters/<?php echo strtolower(str_replace(' ', '-', $data->butters[0]->tags)); ?>" class="nav-link">Butter</a>
                </li>
                 <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'butters') ? 'block' : 'none'; ?>">
                    <ul class="tertiary">
                    <?php foreach($data->butters as $butter): ?>
                		<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $butter->tags))) ? ' class="on"' : ''; ?>>
                			<a href="/products/butters/<?php echo strtolower(str_replace(' ', '-', $butter->tags)); ?>"><?php echo $butter->node_title; ?></a>
                		</li>
                	<?php endforeach; ?>
                    </ul>
                </li>
		<!-- =end BUTTER -->
		
		
		<!-- BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->
		        <li<?php echo ($this->uri->segment(2) == 'creams-buttermilks') ? ' class="on"' : ''; ?>>
		        	<a href="/products/creams-buttermilks/<?php echo strtolower(str_replace(' ', '-',  str_replace('%', '-percent', $data->creams_buttermilks[0]->tags))); ?>" class="nav-link">Buttermilk/Half &amp; Half/Whipping Cream</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'creams-buttermilks') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->creams_buttermilks as $buttermilk): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $buttermilk->tags)))) ? ' class="on"' : ''; ?>>
							<a href="/products/creams-buttermilks/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $buttermilk->tags))); ?>"><?php echo $buttermilk->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->
		
		
		<!-- CHEESE -->
		        <li<?php echo ($this->uri->segment(2) == 'cheeses') ? ' class="on"' : ''; ?>>
		        	<a href="/products/cheeses/<?php echo strtolower(str_replace(' ', '-', $data->cheeses[0]->tags)); ?>" class="nav-link">Cheese</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'cheeses') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->cheeses as $cheese): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $cheese->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/cheeses/<?php echo strtolower(str_replace(' ', '-', $cheese->tags)); ?>"><?php echo $cheese->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end CHEESE -->
		
		
		<!-- COTTAGE CHEESE -->
		        <li<?php echo ($this->uri->segment(2) == 'cottage-cheese') ? ' class="on"' : ''; ?>>
		        	<a href="/products/cottage-cheese/<?php echo strtolower(str_replace(' ', '-', $data->cottage_cheese[0]->tags)); ?>" class="nav-link">Cottage Cheese</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'cottage-cheese') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->cottage_cheese as $cottage_cheese): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $cottage_cheese->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/cottage-cheese/<?php echo strtolower(str_replace(' ', '-', $cottage_cheese->tags)); ?>"><?php echo $cottage_cheese->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end COTTAGE CHEESE -->
		
		
		<!-- DIPS -->
		        <li<?php echo ($this->uri->segment(2) == 'dips') ? ' class="on"' : ''; ?>>
		        	<a href="/products/dips/<?php echo strtolower(str_replace(' ', '-', $data->dips[0]->tags)); ?>" class="nav-link">Dips</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'dips') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->dips as $dip): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $dip->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/dips/<?php echo strtolower(str_replace(' ', '-', $dip->tags)); ?>"><?php echo $dip->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end DIPS -->
		
		
		<!-- EGG NOG 
		        <li<?php echo ($this->uri->segment(2) == 'eggnog') ? ' class="on"' : ''; ?>>
		        	<a href="/products/eggnog/<?php echo strtolower(str_replace(' ', '-', $data->eggnog[0]->tags)); ?>" class="nav-link">Egg Nog</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'eggnog') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->eggnog as $eggnog): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $eggnog->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/eggnog/<?php echo strtolower(str_replace(' ', '-', $eggnog->tags)); ?>"><?php echo $eggnog->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		 =end EGG NOG -->
		
		
		<!-- EGG SUBSTITUTE -->
		<?php if(! empty($data->egg_substitute)): ?>
		        <li<?php echo ($this->uri->segment(2) == 'egg-substitute') ? ' class="on"' : ''; ?>>
		        	<a href="/products/egg-substitute/<?php echo strtolower(str_replace(' ', '-', $data->egg_substitute[0]->tags)); ?>" class="nav-link">Egg Substitute</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'egg-substitute') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->egg_substitute as $egg_substitute): ?>
		        		<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $egg_substitute->tags))) ? ' class="on"' : ''; ?>>
		        			<a href="/products/egg-substitute/<?php echo strtolower(str_replace(' ', '-', $egg_substitute->tags)); ?>"><?php echo $egg_substitute->node_title; ?></a>
		        		</li>
		        	<?php endforeach; ?>
		            </ul>
		        </li>
        <?php endif; ?>
		<!-- =end EGG SUBSTITUTE -->
		
		
		<!-- FRUIT DRINKS -->
		<?php if(! empty($data->fruit_drinks)): ?>
		        <li<?php echo ($this->uri->segment(2) == 'fruit-drinks') ? ' class="on"' : ''; ?>>
		        	<a href="/products/fruit-drinks/<?php echo strtolower(str_replace(' ', '-', $data->fruit_drinks[0]->tags)); ?>" class="nav-link">Fruit Drinks</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'fruit-drinks') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->fruit_drinks as $fruit_drink): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $fruit_drink->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/fruit-drinks/<?php echo strtolower(str_replace(' ', '-', $fruit_drink->tags)); ?>"><?php echo $fruit_drink->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<?php endif; ?>
		<!-- =end FRUIT DRINKS -->
		
		
		<!-- ICE CREAM SITE -->
			<li>
				<a href="http://hilandicecream.com/" target="_blank">Ice Cream</a>
			</li>
		<!-- =end ICE CREAM SITE -->
		
		
		<!-- ICED COFFEE -->
		        <li<?php echo ($this->uri->segment(2) == 'iced-coffee') ? ' class="on"' : ''; ?>>
		        	<a href="/products/iced-coffee/<?php echo strtolower(str_replace(' ', '-', $data->iced_coffee[0]->tags)); ?>" class="nav-link">Iced Coffee</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'iced-coffee') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->iced_coffee as $coffee): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $coffee->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/iced-coffee/<?php echo strtolower(str_replace(' ', '-', $coffee->tags)); ?>"><?php echo $coffee->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end ICED COFFEE -->
		
		
		<!-- JUICE -->
		        <li<?php echo ($this->uri->segment(2) == 'juice') ? ' class="on"' : ''; ?>>
		        	<a href="/products/juice/<?php echo strtolower(str_replace(' ', '-', $data->juice[0]->tags)); ?>" class="nav-link">Juice</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'juice') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->juice as $juice): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $juice->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/juice/<?php echo strtolower(str_replace(' ', '-', $juice->tags)); ?>"><?php echo $juice->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end JUICE -->
		
		
		<!-- LACTOSE FREE MILK -->
		        <li<?php echo ($this->uri->segment(2) == 'lactose-free-milk') ? ' class="on"' : ''; ?>>
		        	<a href="/products/lactose-free-milk/<?php echo strtolower(str_replace(' ', '-', $data->lactose_free_milk[0]->tags)); ?>" class="nav-link">Lactose Free Milk</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'lactose-free-milk') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->lactose_free_milk as $lactose): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $lactose->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/lactose-free-milk/<?php echo strtolower(str_replace(' ', '-', $lactose->tags)); ?>"><?php echo $lactose->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end LACTOSE FREE MILK -->
		
                
		<!-- MILK -->
                <li<?php echo ($this->uri->segment(2) == 'milks') ? ' class="on"' : ''; ?>>
                	<a href="/products/milks/<?php echo strtolower(str_replace(' ', '-', $data->milks[0]->tags)); ?>" class="nav-link">Milk</a>
                </li>
                <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'milks') ? 'block' : 'none'; ?>">
                    <ul class="tertiary">
                    <?php foreach($data->milks as $milk): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $milk->tags)))) ? ' class="on"' : ''; ?>><a href="/products/milks/<?php echo strtolower(str_replace(' ', '-',  str_replace('%', '-percent', $milk->tags))); ?>"><?php echo $milk->node_title; ?></a></li>
					<?php endforeach; ?>
                    </ul>
                </li>
		<!-- =end MILK -->
		
		
		<!-- MILK ALTERNATIVES -->
		        <li<?php echo ($this->uri->segment(2) == 'milk-alternatives') ? ' class="on"' : ''; ?>>
		        	<a href="/products/milk-alternatives/<?php echo strtolower(str_replace(' ', '-', $data->milk_alternatives[0]->tags)); ?>" class="nav-link">Milk Alternatives</a>
		        </li>
		        <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'milk-alternatives') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->milk_alternatives as $alternatives): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $alternatives->tags)))) ? ' class="on"' : ''; ?>><a href="/products/milk-alternatives/<?php echo strtolower(str_replace(' ', '-',  str_replace('%', '-percent', $alternatives->tags))); ?>"><?php echo $alternatives->node_title; ?></a></li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end MILK ALTERNATIVES -->
		
		
		<!-- RED DIAMOND TEA -->
		        <li<?php echo ($this->uri->segment(2) == 'red-diamond-tea') ? ' class="on"' : ''; ?>>
		        	<a href="/products/red-diamond-tea/<?php echo strtolower(str_replace(' ', '-', $data->red_diamond_tea[0]->tags)); ?>" class="nav-link">Red Diamond Tea</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'red-diamond-tea') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->red_diamond_tea as $tea): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $tea->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/red-diamond-tea/<?php echo strtolower(str_replace(' ', '-', $tea->tags)); ?>"><?php echo $tea->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end RED DIAMOND TEA -->
		
		
		<!-- SCHOOL MILK -->
		        <li<?php echo ($this->uri->segment(2) == 'school-milk') ? ' class="on"' : ''; ?>>
		        	<a href="/products/school-milk/<?php echo strtolower(str_replace(' ', '-', $data->school_milk[0]->tags)); ?>" class="nav-link">School Milk</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'school-milk') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->school_milk as $schoolmilk): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $schoolmilk->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/school-milk/<?php echo strtolower(str_replace(' ', '-', $schoolmilk->tags)); ?>"><?php echo $schoolmilk->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end SCHOOL MILK -->
		
		
		<!-- SOUR CREAM -->
		        <li<?php echo ($this->uri->segment(2) == 'sour-cream') ? ' class="on"' : ''; ?>>
		        	<a href="/products/sour-cream/<?php echo strtolower(str_replace(' ', '-', $data->sour_cream[0]->tags)); ?>" class="nav-link">Sour Cream</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'sour-cream') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->sour_cream as $sour_cream): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $sour_cream->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/sour-cream/<?php echo strtolower(str_replace(' ', '-', $sour_cream->tags)); ?>"><?php echo $sour_cream->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end SOUR CREAM -->
		
		
		<!-- WATER -->
		<?php if(! empty($data->water)): ?>
		        <li<?php echo ($this->uri->segment(2) == 'water') ? ' class="on"' : ''; ?>>
		        	<a href="/products/water/<?php echo strtolower(str_replace(' ', '-', $data->water[0]->tags)); ?>" class="nav-link">Water</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'water') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->water as $water): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $water->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/water/<?php echo strtolower(str_replace(' ', '-', $water->tags)); ?>"><?php echo $water->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<?php endif; ?>
		<!-- =end WATER -->
		
		
		<!-- YOGURT -->
		        <li<?php echo ($this->uri->segment(2) == 'yogurts') ? ' class="on"' : ''; ?>>
		        	<a href="/products/yogurts/<?php echo strtolower(str_replace(' ', '-', $data->yogurts[0]->tags)); ?>" class="nav-link">Yogurt</a>
		        </li>
		         <li style="list-style: none; display: <?php echo ($this->uri->segment(2) == 'yogurts') ? 'block' : 'none'; ?>">
		            <ul class="tertiary">
		            <?php foreach($data->yogurts as $yogurt): ?>
						<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $yogurt->tags))) ? ' class="on"' : ''; ?>>
							<a href="/products/yogurts/<?php echo strtolower(str_replace(' ', '-', $yogurt->tags)); ?>"><?php echo $yogurt->node_title; ?></a>
						</li>
					<?php endforeach; ?>
		            </ul>
		        </li>
		<!-- =end YOGURT -->
		
		</ul>
	</div>
</div>
