<div id="content-container">
	<div id="content" class="coupons-print">
		<h1 class="fontface"><img src="/img/printcoupongraphic.jpg" width="195" height="227" align="right" />Hiland Dairy Printable Coupons</h1>
		
		<ul>
			<li>40&cent; off any Square Ice Cream</li>
			<li>35&cent; off any Iced Coffees</li>
			<li>25&cent; off any 1/2 Gal. Apple or O.J. </li>
			<li>30&cent; off any 1/2 Gal. Chocolate Milk</li>
	    </ul>
		<a href="/coupons-print/coupon" class="btn_printcoupons" target="_blank"><span>Print Coupons</span></a>
	</div>
</div>