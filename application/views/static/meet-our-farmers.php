<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_farmers; ?>
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="content" class="meet-our-farmers-wrapper">
		
		<h1 class="fontface">Meet Our Farmer-Owners</h1>
		<h2>Know Where Your Milk Comes From: Meet Our Farmer-Owners</h2>
		<p>The secret to the fresh taste of Hiland Dairy milk is our farmer ownership. Every gallon of nutrient-dense, antibiotic and artificial growth hormone-free milk comes from farms—most of which are located within 100 miles of Hiland Dairy plants. Our commitment to selling milk from our farm-owners ensures that you enjoy fresh, local milk and that the dairy farms in your community keep their doors open and their workers employed.</p>
		
		
		<h2>Meet a Few of Our Farmer-Owners</h2>
		<p>Our farmers produce the fresh, wholesome Hiland Dairy milk that your family loves—and they love it too. After all, they know where their milk comes from, and they're proud to share it with you.</p>

		
<!-- FARM OWNERS -->
		<div class="farm-owners">
			<p><strong>Mike and Linda</strong></p>
			<a href="../img/deshazo-1.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-1-sm.jpg" alt="" /></a>
			<a href="../img/deshazo-2.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-2-sm.jpg" alt="" /></a>
			<a href="../img/deshazo-3.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-3-sm.jpg" alt="" /></a>
			<a href="../img/deshazo-4.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-4-sm.jpg" alt="" /></a>
			<a href="../img/deshazo-5.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-5-sm.jpg" alt="" /></a>
			<a href="../img/deshazo-6.jpg" class="deshazo colorbox" rel="deshazo"><img src="../img/deshazo-6-sm.jpg" alt="" /></a>
			<p class="img-caption">The DeShazos produce milk responsibly by reducing waste on their 200-head Double D Dairy farm in Rudy, AR.</p>
		</div>
		<div class="farm-owners">
			<p><strong>David and Nancy</strong></p>
			<a href="../img/crook-1.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-1-sm.jpg" alt="" /></a>
			<a href="../img/crook-2.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-2-sm.jpg" alt="" /></a>
			<a href="../img/crook-3.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-3-sm.jpg" alt="" /></a>
			<a href="../img/crook-4.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-4-sm.jpg" alt="" /></a>
			<a href="../img/crook-5.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-5-sm.jpg" alt="" /></a>
			<a href="../img/crook-6.jpg" class="crook colorbox" rel="crook"><img src="../img/crook-6-sm.jpg" alt="" /></a>
			<p class="img-caption">The entire Crook family from father David and his wife, Nancy, to his son, Eric and brother, Charles have a hand in running Crook Dairy in Humboldt, NE.</p>
		</div>
<!-- =end FARM OWNERS -->

	</div>
	<script src="/js/jquery.colorbox-min.js" type="text/javascript"></script>
	<script>	
			$('.deshazo.colorbox').colorbox({rel:'deshazo'});
			$('.crook.colorbox').colorbox({rel:'crook'});
	</script>
</div>