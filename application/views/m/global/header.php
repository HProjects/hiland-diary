<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>Hiland Dairy</title>

<!-- CSS Styles -->
		<link rel="stylesheet" href="/css/fontface.css" type="text/css" />
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.css" type="text/css" />
		<link rel="stylesheet" href="/css/styles.css" type="text/css" />
<!-- =end CSS Styles -->

<!-- Favicon --><link rel="icon" href="/favicon.ico" type="image/ico">

<!-- jQuery -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script>
			$(document).bind('mobileinit', function () {
		    	$.mobile.activeBtnClass = 'unused';
		    });
		</script>
		<script src="/js/jquery.mobile1.2.0-min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/scripts.js"></script>
<!-- =end jQuery -->

<!-- Pinterest Start -->
<b:if cond='data:blog.pageType == &quot;item&quot;'>
<script src='http://assets.pinterest.com/js/pinit.js' type='text/javascript'/>
<script type='text/javascript'>
function run_pinmarklet() {
    var e=document.createElement(&#39;script&#39;);
    e.setAttribute(&#39;type&#39;,&#39;text/javascript&#39;);
    e.setAttribute(&#39;charset&#39;,&#39;UTF-8&#39;);
    e.setAttribute(&#39;src&#39;,&#39;http://assets.pinterest.com/js/pinmarklet.js?r=&#39; + Math.random()*99999999);
    document.body.appendChild(e);
}
</script>
</b:if>
<!-- =end Pinterest -->
</head>
<body>
<div  data-role="page">
	<div data-role="header" id="header"> 
		<div class="logo">
			<a href="/"><img src="/img/mobile-hiland-logo.png" alt="Hiland Dairy" /></a>
		</div>
	</div>