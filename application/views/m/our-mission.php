<div class="content-container">
	<h1 class="fontface">Our Mission</h1>
	<h2>Hiland Milk Contains No Antibiotics or Artificial Growth Hormones</h2>
	<p>We're proud to be farmer-owned, which allows us to produce high-quality dairy products from milk that is wholesome, delicious and full of important vitamins and minerals&ndash;free of antibiotics or artificial growth hormones.</p>
	

	<h2>From the Land to Your Table: Sustainability at<br />Hiland Dairy</h2>
	<p>At Hiland, our core values include "doing what's right" and "being better than we have to be." Though these are evident in the products we make, they're also a part of a larger philosophy that's rooted in our relationship with the world around us. We know that fertile land produces healthy cows, and healthy cows provide fresh, great tasting milk. So we care deeply about preserving our water, land and natural resources for future generations. To that end, we also reduce waste, reuse resources and recycle what we can so that those same future generations will be able to enjoy the same high quality milk you enjoy today.</p>


	<h2>Our Sustainability Efforts</h2>
	<p>It's one thing to talk sustainability; it's quite another to put it into practice. At Hiland, we're practicing what we preach every day by:</p>
		<ul>
			<li><strong>Reducing</strong> consumption with energy efficient light bulbs, motors, fans, pumps and compressors.</li>
			<li><strong>Reusing</strong> resources like manure to fertilize our fields or eight million gallons of water from reverse osmosis water silos on our farms.</li>
			<li><strong>Recycling</strong> milk crates, batteries, oil, paper, cardboard, aluminum and more than two million pounds of plastic.</li>
		</ul>


	<h2>What We're Saving</h2>
	<p>Our farmer-owned dairies preserve and protect precious resources through more than 1,000 sustainability projects. Here's a glimpse into the results of our efforts. Last year we:</p>
		<ul>
			<li>Saved 508,000 pounds of resin</li>
			<li>Saved 115 million gallons of water</li>
			<li>Recycled 2,000 batteries</li>
			<li>Recycled 31,000 gallons of oil</li>
			<li>Recycled 2,500 tons of cardboard</li>
			<li>Recycled 117,922 pounds of office paper</li>
			<li>Recycled 2 million pounds of plastic</li>
			<li>Recycled 6,000 pounds of aluminum</li>
		</ul>
	<p>Together, these efforts saved 19,085 barrels of oil and a total of 6,662,402 kWh of electricity. That's enough electricity to power the average American home for more than 228,200 days!</p>
</div>