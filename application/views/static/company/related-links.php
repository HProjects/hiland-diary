<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- RELATED LINKS -->
		<h1 class="fontface">Links We Like</h1>
		<ul>
			<li>
				<a href="http://hilandicecream.com/" target="_blank">Hiland Ice Cream</a>
				<p>There’s nothing quite like rich, creamy ice cream. Explore your favorite flavors at Hiland Ice Cream.</p>
			</li>
			<li>
				<a href="http://www.qchekd.com" target="_blank">Quality Chekd Dairies</a>
				<p>Next to every Hiland Dairy product label you’ll see the Quality Chekd symbol, ensuring that our dairy products have been approved for freshness and quality by Quality Chekd, a third party testing association.</p>
			</li>
			<li>
				<a href="http://www.prairiefarms.com" target="_blank">Prairie Farms Dairy</a>
				<p>Hiland Dairy is incorporated by Prairie Farms Dairy, a farmer-owned cooperative committed to producing fresh, wholesome dairy products.</p>
			</li>
			<li>
				<a href="http://www.dfamilk.com" target="_blank">Dairy Farmers of America </a>
				<p>Hiland Dairy is also incorporated by the Dairy Farmers of America, one of the nation’s largest farmer-owned dairy cooperatives.</p>
			</li>
			<li>
				<a href="http://midwestdairy.com/" target="_blank">Midwest Dairy</a>
			</li>
			<li>
				<a href="http://www.nationaldairycouncil.org/Pages/Home.aspx" target="_blank">Healthy Weight with Dairy</a>
				<p>Helpful information about the role dairy plays in healthy weight management.</p>
			</li>
			<li>
				<a href="http://www.nationaldairycouncil.org" target="_blank">National Dairy Council</a>
				<p>Learn about the latest dairy research, browse recipes and discover helpful health tips from the National Dairy Council, a non-profit organization dedicated to healthy living through dairy products.</p>
			</li>
			<li>
				<a href="http://www.milkmustache.com/" target="_blank">The Breakfast Project</a>
				<p>Find out how to make the most of your mornings at this interactive website sponsored by America’s Milk Processors.</p>
			</li>
			<li>
				<a href="http://www.gotchocolatemilk.com/" target="_blank">REFUEL with Chocolate Milk</a>
				<p>Discover why chocolate milk may be the best after-workout recovery drink on the market.</p>
			</li>
		</ul>
<!-- =end RELATED LINKS -->

	</div>
</div>