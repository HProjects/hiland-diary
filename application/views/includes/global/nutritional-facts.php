<?php $nutritional_facts = ( isset($recipe) ) ? array('serving_size' => $recipe->field_recipe_serving_size, 'nutritional_facts' => $recipe->field_recipe_nutritional_facts,  'vitamins_and_minerals' => $recipe->field_recipe_vitamins_and_minera) : array('serving_size' => $product->field_serving_size, 'nutritional_facts' => $product->field_product_nutritional_facts,  'vitamins_and_minerals' => $product->field_vitamins_and_minerals); ?>

<?php if( isset($nutritional_facts['serving_size']->und[0]->field_servings_size) && !empty($nutritional_facts['serving_size']->und[0]->field_servings_size) ): ?>

		<div id="column-two-nutfacts">
			<div id="column-two-nutfacts-white">
				<table width="100%" cellspacing="0" id="nutritiontable">
  					<tr>
  						<td>
  							<h3>Nutrition Facts</h3>
  							<p class="disclaimer"><em>Nutritional information may change. Variations may occur due to formula and ingredient changes.</em></p>
							<p>Serving Size: <span class="servings"><?php echo $nutritional_facts['serving_size']->und[0]->field_servings_size; ?></span><br />
							Servings Per Container: <span class="size"><?php echo $nutritional_facts['serving_size']->und[0]->field_servings_per_container; ?></span></p>
							<hr style="border-width:6px 0 0;" />
							<p>Amount Per Serving</p>
							<hr />
							<table width="98%" cellspacing="0" class="serving-facts-calories">
								<tr>
					  				<td style="border-bottom:3px solid #d9d9d9;"><strong>Calories <span class="calories"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_calories; ?></span></strong></td>
									<td align="right" style="border-bottom:3px solid #d9d9d9;">Calories from Fat <span class="fat-calories"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_calories_from_fat; ?></span></td>
								</tr>
							</table>
							<table width="98%" cellspacing="0" class="serving-facts-nutrition">
								<tr>
									<td colspan="2" style="text-align:right;">% Daily Value*</td>
								</tr>
								
								<tr>
					  				<td><strong>Total Fat</strong> <span class="total-fat"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_total_fat; ?></span>g</td>
									<td align="right"><span class="total-fat-percent"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_total_fat_dv; ?></span>%</td>
								</tr>
							
								
								<tr>
					  				<td>Saturated Fat <span class="saturated-fat"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_saturated_fat; ?></span>g</td>
									<td align="right"><span class="saturated-fat-percent"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_saturated_fat_dv; ?></span>%</td>
								</tr>
								
								
								<tr>
					  				<td><em>trans</em> Fat <span class="trans-fat"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_trans_fat; ?></span>g</td>
									<td align="right"><span class="trans-fat-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_trans_fat_dv; ?></span>%</td>
								</tr>
								</tr>
								
								
								<tr>
					  				<td><strong>Cholesterol</strong> <span class="cholesterol"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_cholesterol; ?></span>mg</td>
									<td align="right"><span class="cholesterol-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_cholesterol_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td><strong>Sodium</strong> <span class="sodium"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_sodium; ?></span>mg</td>
									<td align="right"><span class="sodium-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_sodium_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td><strong>Total Potassium</strong> <span class="potassium"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_potassium; ?></span>g</td>
									<td align="right"><span class="potassium-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_potassium_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td><strong>Total Carbohydrate</strong> <span class="carbohydrate"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_total_carbohydrate; ?></span>g</td>
									<td align="right"><span class="carbohydrate-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_total_carbohydrate_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td>Dietary Fiber <span class="fiber"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_dietary_fiber; ?></span>g</td>
									<td align="right"><span class="fiber-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_dietary_fiber_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td>Sugars <span class="sugars"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_sugars; ?></span>g</td>
					  				<td align="right"><span class="sugars-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_sugars_dv; ?></span>%</td>
								</tr>
								
								<tr>
					  				<td style="border-bottom:none;"><strong>Protein</strong> <span class="protein"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_protein; ?></span>g</td>
									<td align="right" style="border-bottom:none;"><span class="protein-percentage"><?php echo $nutritional_facts['nutritional_facts']->und[0]->field_protein_dv; ?></span>%</td>
								</tr>
								
							</table>
							<table width="98%" cellspacing="0" id="vitamintable" class="serving-facts-vitamin">
								<tr>
									<td>Vitamin A</td>
					  				<td><span class="a"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_vitamin_a; ?></span>%</td>
					 				<td>Vitamin C</td>
					  				<td><span class="c"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_vitamin_c; ?></span>%</td>
								</tr>
								<tr>
									<td>Calcium</td>
					  				<td><span class="calcium"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_calcium; ?></span>%</td>
									<td>Iron</td>
					  				<td><span class="iron"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_iron; ?></span>%</td>
								</tr>
								<tr>
									<td>Vitamin D</td>
					  				<td><span class="vitamin-d"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_vitamin_d; ?></span>%</td>
									<td>Phosphorus</td>
					  				<td><span class="phosphorus"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_phosphorus; ?></span>%</td>
								</tr>
								<tr>
									<td>Magnesium</td>
					  				<td><span class="magnesium"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_magnesium; ?></span>%</td>
									<td>Thiamin</td>
					  				<td><span class="thiamin"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_thiamin; ?></span>%</td>
								</tr>
								<tr>
									<td colspan="2">Folate</td>
					  				<td colspan="2"><span class="folate"><?php echo $nutritional_facts['vitamins_and_minerals']->und[0]->field_folate; ?></span>%</td>
								</tr>
								<tr>
							</table>
							<p class="nut-disclaimer">Percent Daily Values are based on a 2,000-calorie diet. Your daily values may be higher or lower depending on your calorie needs.</p>
							</tr>
					</td>
				</table>
			</div>
		</div>
		<? else: ?>
		<div id="column-two-nutfacts">
			<div id="column-two-nutfacts-white">
				<p class="coming-soon">Nutritional Facts Coming soon.</p>
			</div>
		</div>
		<? endif; // end of nutrition conditional ?>