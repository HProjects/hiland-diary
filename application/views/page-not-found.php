<div id="content-container">
	<div id="content">
		<h1 class="fontface">Don't cry over spilled milk.</h1>
		<h2>We can help you find what you're looking for!</h2>
		<p><img src="../img/spilled.jpg" width="" height="" border="0" align="right" />Please use the search bar in the navigation bar of the page.</p>
		<p style="font-weight:bold;"><a href="/recipes">Looking for a recipe</a> or interested in some of our <a href="/products">products</a>? </p>
		<h3>Or, Find Company Information Below</h3>
		<ul>
			<li><a href="/company/awards">Awards</a></li>
			<li><a href="/company/community">Community</a></li>
			<li><a href="/company/contact-us">Contact Us</a></li>
			<li><a href="/company/employment">Employment</a></li>
			<li><a href="/company/faqs">FAQs</a></li>
			<li><a href="/company/food-service">Food Service</a></li>
			<li><a href="/company/media-center">Media Center</a></li>
			<li><a href="/company/related-links">Related Links</a></li>
			<li><a href="/company/school-milk-program">School Milk Program</a></li>
		</ul>
		
	</div>
</div>