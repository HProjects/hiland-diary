<?php 
	
	if($this->input->get('site') == 'desktop')
	{
		$this->session->set_userdata('site', 'desktop');
	}
	
	if($this->agent->is_mobile() && !$this->agent->is_mobile('ipad') && $this->session->userdata('site') != 'desktop')
	{
		$this->load->helper('url');
		$query = (isset($_SERVER['QUERY_STRING'])) ? '?' . $_SERVER["QUERY_STRING"] : '';
		redirect('http://m.hilanddairy.net/' . $query);	
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="handheldFriendly" content="true" />
<title>Hiland Dairy<?php echo ($this->uri->segment(1, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(1, 0))) : '';?><?php echo ($this->uri->segment(2, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(2, 0))) : '';?><?php echo ($this->uri->segment(3, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(3, 0))) : '';?></title>

<!-- Facebook Sharing Links -->
	<meta property="og:title" content="Hiland Dairy<?php echo ($this->uri->segment(1, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(1, 0))) : '';?><?php echo ($this->uri->segment(2, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(2, 0))) : '';?><?php echo ($this->uri->segment(3, 0)) ? ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment(3, 0))) : '';?>"/>
    <meta property="og:type" content="movie"/>
    <meta property="og:url" content="http://hilanddairy.com" />
    <meta property="og:image" content="http://hilanddata.com/sites/default/files/<?php echo $recipe->field_recipe_image->und[0]->filename;?>" />
    <meta property="og:site_name" content="Hiland Dairy"/>
    <meta property="fb:admins" content="USER_ID"/>
<!-- =end Facebook Sharing Links -->

<!-- CSS Styles -->
		<link rel="stylesheet" href="/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="/css/colorbox.css" type="text/css" />
		<link rel="stylesheet" href="/css/jquery.rating.css" type="text/css" />
		<link rel="stylesheet" href="/css/styles.css" type="text/css" />
		<link rel="stylesheet" href="/css/fontface.css" type="text/css" />
<!-- =end CSS Styles -->

<!-- Favicon --><link rel="icon" href="/favicon.ico" type="image/ico">

<!-- jQuery -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script src="/js/jquery.validate.min.js"></script>
		<script src="/js/jquery.rating.pack.js"></script>
		<script src="/js/scripts.js"></script>
<!-- =end jQuery -->

<!-- Pinterest Start -->
<b:if cond='data:blog.pageType == &quot;item&quot;'>
<script src='http://assets.pinterest.com/js/pinit.js' type='text/javascript'/>
<script type='text/javascript'>
function run_pinmarklet() {
    var e=document.createElement(&#39;script&#39;);
    e.setAttribute(&#39;type&#39;,&#39;text/javascript&#39;);
    e.setAttribute(&#39;charset&#39;,&#39;UTF-8&#39;);
    e.setAttribute(&#39;src&#39;,&#39;http://assets.pinterest.com/js/pinmarklet.js?r=&#39; + Math.random()*99999999);
    document.body.appendChild(e);
}
</script>  
</b:if> 
<!-- =end Pinterest -->

<!-- Put the following javascript before the closing </head> tag. -->
  
<?php if(isset($results) && $results): ?>
<script>
  (function() {
    var cx = '008780795054131092579:kcdce49qn9y';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })();
</script>
<?php endif; ?>
<!--
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-80161-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
-->
</head>
<body>
