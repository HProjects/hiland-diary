<div id="coupon-block">
	<div id="staticleftpink">
		<p>Along with community education, mentoring programs and support groups, the Breast Cancer Foundation of the Ozarks assists with free screening mammography. It is the only local organization providing non-medical financial support to those facing hardships as a result of breast cancer. <a href="http://www.bcfo.org" target="_blank">Read More</a></p>
		<p><strong>Your donation allows us to assist families in need.</strong></p>
		<p><a class="donatenow-cta" href="http://www.bcfo.org/make-a-donation/" target="_blank"><span class="hide">DONATE</span></a></p>
	</div>
</div>
<!--<img src="/img/pinkRibbon.jpg" width="190" height="127" />-->