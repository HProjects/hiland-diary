<div id="subnav">
	<div class="secondary">
		<ul class="menu">
		
		<!-- BUDGET FRIENDLY -->
				<li<?php echo ($this->uri->segment(3) == 'budget-friendly' || $this->uri->segment(2) == 'budget-friendly') ? ' class="on"' : ''; ?>>
					<a href="/recipes/categories/budget-friendly" class="nav-link">Budget Friendly</a>
				</li>
		<!-- =end BUDGET FRIENDLY -->
			
		
		<!-- APPETIZERS -->
				<li<?php echo ($this->uri->segment(3, 0) == 'appetizers' || $this->uri->segment(2) == 'appetizers') ? ' class="on"' : ''; ?>>
					<a href="/recipes/categories/appetizers" class="nav-link">Appetizers</a>
				</li>
		<!-- =end APPETIZERS -->
			
			
		<!-- MAIN COURSES -->
				<li<?php echo ($this->uri->segment(3, 0) == 'main-courses' || $this->uri->segment(2) == 'main-courses') ? ' class="on"' : ''; ?>>
					<a href="/recipes/categories/main-courses" class="nav-link">Main Courses</a>
				</li>
		<!-- =end MAIN COURSES -->
			
			
		<!-- SIDE DISHES -->
				<li<?php echo ($this->uri->segment(3, 0) == 'side-dishes' || $this->uri->segment(2) == 'side-dishes') ? ' class="on"' : ''; ?>>
					<a href="/recipes/categories/side-dishes" class="nav-link">Side Dishes</a>
				</li>
		<!-- =end SIDE DISHES -->
			
			
		<!-- DESSERTS -->
				<li<?php echo ($this->uri->segment(3, 0) == 'desserts' || $this->uri->segment(2) == 'desserts') ? ' class="on"' : ''; ?>>
					<a href="/recipes/categories/desserts" class="nav-link">Desserts</a>
				</li>
		<!-- =end DESSERTS -->
			
			
		<!-- CATEGORIES -->
				<li class="on">
					<a href="/recipes" class="nav-link">Categories</a>
				</li>
				<li style="list-style: none; display: block">
					<ul class="tertiary">
						<?php foreach($recipes_categories as $category_key => $category_value):?>
							<li>
								<a href="/recipes/categories/<?php echo strtolower(str_replace(array(' ', '-'), '-', $category_key)); ?>">
									(<?php echo $category_value;?>) <?php echo strtoupper($category_key);?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				</li>
		<!-- =end CATEGORIES -->
		
		</ul>
	</div><!-- END secondary -->
</div><!-- END subnav -->
