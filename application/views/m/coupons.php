<div class="content-container">
	<h1 class="fontface">Coupons</h1>
	<h2>May coupons will be:</h2>
	<ul>
			<li>40&cent; off any Square Ice Cream</li>
			<li>35&cent; off any Iced Coffees</li>
			<li>25&cent; off any 1/2 Gal. Apple or O.J.</li>
			<li>30&cent; off any 1/2 Gal. Chocolate Milk</li>
	</ul>
	
	<p><em>To retrieve your coupons please visit the desktop site.</em></p>
	
	<hr style="margin:30px 0;" />
	
	<p><strong>Check your local Sunday newspaper inserts for valuable coupons from Hiland Dairy!</strong></p>
	<ul style="margin-bottom:110px;">
		<li>07/28/13</li>
		<li>08/18/13 (Limited Markets)</li>
		<li>09/15/13</li>
		<li>09/29/13</li>
	</ul>
</div>