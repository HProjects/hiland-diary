<?php //print_r($recipe); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
function printpage()
  {
  window.print()
  }
</script>
<div id="content-container" class="specific-recipe">
			<div id="content">
				<div id="content-columnone">
					<h1 class="fontface"><?php echo $recipe->title; ?></h1>
					<img class="main-recipe-img" src="http://a.hilanddairy.envoydev.com/sites/default/files/<?php echo $recipe->field_recipe_image->und[0]->filename; ?>" width="449" height="282" border="0" />
					<?php if( isset($recipe->field_recipe_description->und[0]->value) ): ?>
					<?php echo $recipe->field_recipe_description->und[0]->value; ?>
					<?php endif; ?>
					
	<div id="recipeboxcontain">		
	<?php if( isset($recipe->field_prep_time->und[0]->value) ): ?>
	<div id="time_prep"><div class="big"><?php echo $recipe->field_prep_time->und[0]->value; ?><span class="min">MIN</span></div></div><div id="time_cook"><div class="big"><?php echo $recipe->field_cook_time->und[0]->value; ?><span class="min">MIN</span></div></div><div id="serving_size"><div class="big"><?php echo $recipe->field_servings->und[0]->value; ?></div></div><br clear="all">
	<?php endif; ?>
	</div>
	
	<div class="recipe-ingredients">
		<h2><span class="hidden">Recipe Ingredients</span></h2>
		<?php echo $recipes_ingredients; ?>
	</div>
	
	<div class="recheader_long"><h2>Directions</h2></div>
		<div id="recipedirections">
			<?php foreach($recipe->field_recipe_directions->und as $number => $direction): ?>
				<div class="directions-number">
					<div class="hi"><?php echo $number + 1; ?></div>
				</div>
				<div class="directions_copy"><?php echo $direction->value; ?></div><br clear="all">
				<div class="directions-line"></div><br clear="all">
			<?php endforeach; ?>
		</div>
		<?php if( isset($recipe->field_recipe_description->und[0]->value) ): ?>
		<div class="recheader_long"><h2>Recipe Notes</h2></div>
			<p><?php echo $recipe->field_recipe_description->und[0]->value; ?></p>
		<?php endif; ?>
				</div>
				<div id="content-columntwo">						
					<!--<div class="recipe-ingredients">
						<h2><span class="hidden">Recipe Ingredients</span></h2>
						<?php echo $recipes_ingredients; ?>
					</div>-->
					
					<div id="column-two-socialbox">
						<div class="pinterestpin">
							<a href='javascript:void(run_pinmarklet1())'><img src="http://2.bp.blogspot.com/-lRae8bdMpuA/TzuLrnycXaI/AAAAAAAACQE/YVYUjfs7dm8/s1600/pinmask2.png" style='margin:0; padding:0; border:none;'/></a>
							<script type='text/javascript'>
							function run_pinmarklet1() {
							  var e=document.createElement('script');
							  e.setAttribute('type','text/javascript');
							  e.setAttribute('charset','UTF-8');
							  e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
							  document.body.appendChild(e);
							}
							</script>
						</div>
						<div class="emailrecipe">
							<a href="mailto:?subject=Recipe%20Ingredients&body=<?php echo 'http://' . $_SERVER['SERVER_NAME'] . '/recipes/appetizers/layered-bean-dip'; ?>" id="btn_email2"><span>Email</span></a>
						</div>
					    <div class="fblike">
					    	<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></div>
					    </div>
					</div>
					
					<?php echo $nutritional_facts; ?>
				
					<?php
					/*
	<div class="recheader_short"><h2>Related Recipes</h2></div>
					<div id="relatedrecipes">
					<?php foreach($list as $related): ?>
					<?php $i = 1; ?>
						<?php if($this->uri->segment(3, 0) !== strtolower(str_replace(array(' ', '-'), '_', $related->node_title))): ?>
						<div class="reciperelateds_blue"><a href="<?php echo strtolower(str_replace(array(' ', '-'), '_', $related->node_title)); ?>"><?php echo $related->node_title; ?></a></div>
						<?php $i++; ?>
						<?php endif; ?>
					<?php if($i >= 5): ?>
					<?php break; ?>
					<?php endif; ?>
					<?php endforeach; ?>
					</div>
	*/
				?>
			</div>
		</div>
		
			<br clear="all">
	</div>
</div>