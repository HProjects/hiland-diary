<div id="coupon-block">
	<div id="staticleft">
		<h2>Available Coupons</h2>
		<p>Hiland makes your recipes even more budget-friendly with these valuable coupons.</p>
		<ul>
			<li>40&cent; off any Square Ice Cream</li>
			<li>35&cent; off any Iced Coffees</li>
			<li>25&cent; off any 1/2 Gal. Apple or O.J.</li>
			<li>30&cent; off any 1/2 Gal. Chocolate Milk</li>
		</ul>
		<a class="coupons-cta" href="/coupons"><span class="hide">Get Your Coupons</span></a>
	</div>
</div>