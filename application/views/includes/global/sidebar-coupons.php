<div id="coupon-block">
	<div id="staticleft">
		<h2>Available Coupons</h2>
		<p>Hiland makes your recipes even more budget-friendly with these valuable coupons.</p>
		<ul>
			<li>40&cent; off any Square Ice Cream</li>
			<li>35&cent; off any Iced Coffees</li>
			<li>25&cent; off any 1/2 Gal. Apple or O.J.</li>
			<li>30&cent; off any 1/2 Gal. Chocolate Milk</li>
		</ul>
		<a class="coupons-cta" href="/coupons"><span class="hide">Get Your Coupons</span></a>
		<p style="line-height: 1.1; font-style: italic; font-size: 11px; margin-bottom: 6px; "><em>Please note: we have updated our security, you may need to sign up again to receive our coupons.</em></p>
	</div>
</div>