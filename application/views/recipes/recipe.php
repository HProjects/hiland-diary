<?php /* print_r($recipe); */ ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
function printpage()
  {
  window.print()
  }
</script>
<div id="content-container">
<?php echo $recipes_side_nav; ?>
			<div id="content">
				<div id="content-columnone">
					<h1 class="fontface"><?php echo $recipe->title; ?></h1>
					<div id="recipe-print-box">
						<div id="recipe-print-box-left" style="position:relative;">
							<form id="star_rating" style="margin: 0;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($star_ratings) && $star_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($star_ratings) && $star_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($star_ratings) && $star_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($star_ratings) && $star_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($star_ratings) && $star_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">link</a>
							</form> 
							<div id="ratings-message" style="width: 300px;height: 35px;-moz-border-radius: 3px;border-radius: 3px;background: #f8f4ea;margin: 5px 0px 5px 0px;border: 0px solid #f0dab0; position:absolute; top:-5px; left:0; display:none; padding: 0 10px;"></div>
						</div>
						<div id="recipe-print-box-right"><a href="#" id="btn_print" onclick="printpage()"><span>Print</span></a></div>
					</div>
					<img src="http://a.hilanddairy.envoydev.com/sites/default/files/<?php echo $recipe->field_recipe_image->und[0]->filename; ?>" width="449" height="282" border="0" />
					<?php if( isset($recipe->field_recipe_description->und[0]->value) ): ?>
					<?php echo $recipe->field_recipe_description->und[0]->value; ?>
					<?php endif; ?>
					
	<div id="recipeboxcontain">		
	<?php if( isset($recipe->field_prep_time->und[0]->value) ): ?>
	<div id="time_prep"><div class="big"><?php echo $recipe->field_prep_time->und[0]->value; ?><span class="min">MIN</span></div></div><div id="time_cook"><div class="big"><?php echo $recipe->field_cook_time->und[0]->value; ?><span class="min">MIN</span></div></div><div id="serving_size"><div class="big"><?php echo $recipe->field_servings->und[0]->value; ?></div></div><br clear="all">
	<?php endif; ?>
	</div>
	
	<div class="recheader_long"><h2>Directions</h2></div>
		<div id="recipedirections">
			<?php foreach($recipe->field_recipe_directions->und as $number => $direction): ?>
				<div class="directions-number"><div class="hi" style="height:36px; font: 23px/23px 'CreteRoundRegular', Verdana, sans-serif; color:#ffffff; font-weight:bold; padding:3px 0px 0px 0px; margin:-5px 10px 0px 10px; text-align:center; text-shadow: 1px 1px 1px #174467;"><?php echo $number + 1; ?></div></div>
				<div class="directions_copy"><?php echo $direction->value; ?></div><br clear="all">
				<div class="directions-line"></div><br clear="all">
			<?php endforeach; ?>
		</div>
		<?php if( isset($recipe->field_recipe_description->und[0]->value) ): ?>
		<div class="recheader_long"><h2>Recipe Notes</h2></div>
			<p><?php echo $recipe->field_recipe_description->und[0]->value; ?></p>
		<?php endif; ?>
				</div>
				<div id="content-columntwo">
					<div id="column-two-socialbox">
					<div class="pinterestpin">
					<a href='javascript:void(run_pinmarklet1())'><img src="http://2.bp.blogspot.com/-lRae8bdMpuA/TzuLrnycXaI/AAAAAAAACQE/YVYUjfs7dm8/s1600/pinmask2.png" style='margin:0; padding:0; border:none;'/></a>
						<script type='text/javascript'>
						function run_pinmarklet1() {
						  var e=document.createElement('script');
						  e.setAttribute('type','text/javascript');
						  e.setAttribute('charset','UTF-8');
						  e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
						  document.body.appendChild(e);
						}
						</script>
						</div>
                <div class="fblike">
                	<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></div>
                </div>
						</div>
						
				<?php echo $recipes_ingredients; ?>
				<?php echo $nutritional_facts; ?>
				
				<?php
				/*
<div class="recheader_short"><h2>Related Recipes</h2></div>
				<div id="relatedrecipes">
				<?php foreach($list as $related): ?>
				<?php $i = 1; ?>
					<?php if($this->uri->segment(3, 0) !== strtolower(str_replace(array(' ', '-'), '_', $related->node_title))): ?>
					<div class="reciperelateds_blue"><a href="<?php echo strtolower(str_replace(array(' ', '-'), '_', $related->node_title)); ?>"><?php echo $related->node_title; ?></a></div>
					<?php $i++; ?>
					<?php endif; ?>
				<?php if($i >= 5): ?>
				<?php break; ?>
				<?php endif; ?>
				<?php endforeach; ?>
				</div>
*/
				?>
			</div>
		</div>
		
			<br clear="all">    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'hilanddairy'; // required: replace example with your forum shortname
        var disqus_identifier = '<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
    
	</div>
</div>
<script type="text/javascript">
/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
var disqus_shortname = 'hilanddairy'; // required: replace example with your forum shortname

/* * * DON'T EDIT BELOW THIS LINE * * */
(function () {
    var s = document.createElement('script'); s.async = true;
    s.type = 'text/javascript';
    s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
    (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>