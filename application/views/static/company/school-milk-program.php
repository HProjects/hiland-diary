<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">

<!-- FOOD DIRECTORS -->
		<h1 class="fontface">Welcome School Food Directors!</h1>
		<p>As a School Food Director, you have to be discriminating about what you serve. Hiland Dairy’s fat-free, flavored school milk meets the USDA’s strict, low-sugar requirement for drinks. Only our plain and lower-sugar flavored school milks give your students the taste they want with nine of the essential nutrients they need to grow healthy and strong.<img src="/img/school-milk.jpg" width="308" height="329" align="right" /></p>
<!-- =end FOOD DIRECTORS -->


<!-- HILAND SCHOOL MILK -->
		<h2>The Case for Hiland School Milk</h2>
		<p>Studies show that children who drink flavored milk don’t have higher BMI (body mass index) scores than those who don’t drink milk.</p>
			<ul class="milk-highlights">
				<li>Four out of five moms surveyed believe that kids need healthy choices at school including chocolate milk.</li>
				<li>Offering fat-free plain and flavored milk helps kids make healthier choices about what they eat and drink.</li>
				<li>When kids miss out on milk at lunch, it’s almost impossible for them to make up for the lost vitamin D, calcium and potassium they would have received from one serving of milk.</li>
			</ul><br clear="all">
			<p>
			<div class="schoolmilkcallout"><div align="center"><a href="/products/school-milk/fat-free-vanilla-shake-flavored-milk">View All of Hiland's School Milk Varieties and Nutritional Facts</a></div></div>
			</p>
			<!--<div class="linelong"></div>-->
			<ol>
				<li><em>Murphy MM, Douglas JS, Johnson RK, Spence LA. Drinking flavored or plain milk is positively associated with nutrient intake and is not associated with adverse effects on weight status in U.S. children and adolescents. J Am Diet Assoc. 2008; 108:631-639.</em></li>
				<li><em>1,000 interviews with moms of kids in grades K through 12 between 3/9/12 and 3/14/12. Conducted by KRC Research.</em></li>
				<li><em>Conducted by Brian Wansink, PhD of Cornell Center for Behavioral Economics in Child Nutrition in 2011.</em></li>
				<li><em>The impact on student milk consumption and nutrient intakes from eliminating flavored milk in schools. 2009. MilkPEP research conducted by Prime Consulting Group. Presented at the School Nutrition Association Annual National Conference, 2010.</em></li>
			</ol>

<!-- =end HILAND SCHOOL MILK -->


<!-- MORE ESSENTIAL NUTRIENTS -->
		<h2>Hiland School Milk Has Less Sugar, More Essential Nutrients</h2>
		<p>One, 8 oz. serving of plain milk has 64 percent less sugar and 40 percent fewer calories than a can of soda. One serving of flavored milk only has 132 calories and 10.1 grams of added sugar. Even low-calorie fruit drinks don’t pack the nutritive punch that flavored milk does. Only milk, flavored and plain, has nine essential nutrients - calcium, niacin, phosphorus, potassium, protein, riboflavin, vitamin A, vitamin B12 and vitamin D.</p>
<!-- =end MORE ESSENTIAL NUTRIENTS -->


<!-- FACTS ABOUT FLAVORED MILK -->
		<h2>Facts About Flavored Milk</h2>
		<p>There are many myths circulating about milk, particularly flavored milk. Here are the facts about Hiland Dairy flavored school milks:</p>
			<ul>
				<li>130 calories per 8oz. serving</li>
				<li>40% less added sugar than just five years ago</li>
				<li>2.4 teaspoons of added sugar in each serving</li>
			</ul>
		
		<!--
		<h2>Hiland Dairy School Milk Nutrition Facts</h2>
			<ul>
				<li></li>
			</ul>
		-->
<!-- =end FACTS ABOUT FLAVORED MILK -->

	<h2>Need More Information?</h2>
		<p><a href="/company/school-milk-contact-us">Contact one of your local sales representatives</a> about Hiland Dairy School Milk.</p>
		<p>For a list of Hiland Dairy School Milk varieties and nutrition facts, please visit our <a href="/products/school-milk/fat-free-vanilla-shake-flavored-milk">Products section</a>.</p>

	</div>
</div>