<div id="container-main">
	<div align="center">
<div id="nav-container">
			<div id="nav-logo"><a href="/"><img src="/img/spacer.gif" width="239" height="160" border="0" /></a></div>
			<div id="header-right">
				<div id="header-right-company"><a href="#" id="btn_companyinfo"<?php echo ($this->uri->segment(1) == 'company') ? 'class="active"' : ''; ?>><span>Company Information</span></a></div>
				<div id="header-right-social">
					<div id="socialsearchbox">
						<form action="/search/find/" method="post">
							<input id="search" type="text" name="search-query" value="<?php echo $this->input->get('q'); ?>" />
							<input id="submit" type="submit" value="Search" />
						</form>
						<div class="header-social-icons">
							<a class="header-social-facebook" href="http://www.facebook.com/HilandDairy" target="_blank"><span class="hidden">Hiland Dairy Facebook</span></a>
							<a class="header-social-twitter" href="https://twitter.com/HilandDairy" target="_blank"><span class="hidden">Hiland Dairy Twitter</span></a>
							<a class="header-social-pinterest" href="http://pinterest.com/hilanddairy/" target="_blank"><span class="hidden">Hiland Dairy Pinterest</span></a>
							<a class="header-social-youtube" href="http://www.youtube.com/hilanddairycompany" target="_blank"><span class="hidden">Hiland Dairy YouTube</span></a>
						</div>
					</div><br clear="all">
				</div>
				<div id="hiland-milk-nav">
					<ul>
						<li id="products"<?php echo (isset($tab) && $tab == 'products') ? ' class="on"' : ''; ?>><a href="/products">Products</a></li>
						<li id="recipes"<?php echo (isset($tab) && $tab == 'recipes') ? ' class="on"' : ''; ?>><a href="/recipes">Recipes</a></li>
						<li id="coupons"<?php echo (isset($tab) && $tab == 'coupons') ? ' class="on"' : ''; ?>><a href="/coupons">Coupons</a></li>
						<li id="pledge"<?php echo (isset($tab) && $tab == 'our-pledge') ? ' class="on"' : ''; ?>><a href="/our-mission">Our Pledge</a></li>
						<li id="outabout"<?php echo (isset($tab) && $tab == 'out-and-about') ? ' class="on"' : ''; ?>><a href="/out-and-about">Out &amp; About</a></li>
					</ul>
				</div>
			</div>
		</div>