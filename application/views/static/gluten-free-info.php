<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="content">
		<h1 class="fontface">Gluten Free Information</h1>
		<p>For our customers with gluten intolerance or celiac disease, our gluten-free products are a healthy alternative. Choose from a large list of dairy products and beverages. Please note that our dairy foods and beverages not listed here may contain gluten.</p>
		<h2>Gluten-Free Dairy Foods and Beverages</h2>
		
		<div class="gluten-free-products first">
			<h3>Milk</h3>
			<ul>
				<li>Reduced Fat Milk</li>
				<li>Lowfat Milk</li>
				<li>Skim Milk</li>
				<li>Skim Extra<sup>&reg;</sup> Fat Free Milk</li>
				<li>Chocolate Milk</li>
				<li>Reduced Fat Chocolate Milk</li>
				<li>Lowfat Chocolate Milk</li>
				<li>Fat Free Chocolate Milk</li>
				<li>Reduced Fat Strawberry Milk</li>
				<li>Lowfat Strawberry Milk</li>
				<li>Fat Free Vanilla Shake flavored Milk</li>
				<li>Lowfat Orange Creme Milk</li>
				<li>Lowfat Buttermilk</li>
				<li>Bulgarian Buttermilk</li>
			</ul>
		</div>
		
		<div class="gluten-free-products">
			<h3>Sour Cream and Cottage Cheese</h3>
			<ul>
				<li>Regular Sour Cream</li>
				<li>Light Sour Cream</li>
				<li>French Onion Dip</li>
				<li>Light French Onion Dip</li>
				<li>Vegetable Dip</li>
				<li>Southwest Ranch Dip</li>
				<li>Large Curd Cottage Cheese</li>
				<li>Small Curd Cottage Cheese</li>
				<li>Low Fat 2% Cottage Cheese</li>
				<li>Fat Free Cottage Cheese</li>
				<li>Nonfat No Sugar Added Yogurt &ndash; All Flavor Varieties</li>
				<li>Low Fat Yogurt &ndash; All Flavor Varieties</li>
			</ul>
		</div>
		
		<div class="gluten-free-products">
			<h3>Ice Cream</h3>
			<ul>
				<li>Vanilla Ice Cream</li>
				<li>Real Vanilla Ice Cream</li>
				<li>French Vanilla Ice Cream</li>
				<li>Vanilla Bean Ice Cream</li>
				<li>Chocolate Ice Cream</li>
				<li>All Sherbet Flavors</li>
				<li>Vanilla, Chocolate and Strawberry Frozen Yogurt</li>
				<li>Vanilla, Chocolate and Strawberry Ice Cream Cups</li>
				<li>North Star branded Fudge Bars</li>
				<li>Old Recipe Bars</li>
				<li>Reduced Fat Ice Cream Bars</li>
				<li>Chocolate Coated Vanilla Flavored Ice Cream Bars</li>
			</ul>
		</div>
		
		<div class="gluten-free-products">
			<h3>Shake Mixes</h3>
			<ul>
				<li>Vanilla and Chocolate Shake Mix</li>
				<li>Low Fat Shake Base</li>
				<li>4% and 5% Vanilla and Chocolate Reduced Fat Ice Cream Mix</li>
				<li>6%, 10% and 14% Vanilla Mix</li>
			</ul>
		</div>
		
		<div class="gluten-free-products">
			<h3>Miscellaneous Dairy Products</h3>
			<ul>
				<li>Orange Juice</li>
				<li>Red Diamond Tea &ndash; All Varieties</li>
				<li>Heavy Whipping Cream</li>
				<li>Half and Half</li>
				<li>Butter</li>
			</ul>
		</div>

	</div>
</div>