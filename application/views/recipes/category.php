<div id="content-container">
<?php echo $recipes_side_nav; ?>
			<div id="content">
					<h1 class="fontface"><?php echo $title; ?></h1>
					<?php foreach($list as $k => $item): ?>
					<div class="<?php echo ($k%2 == 0) ? 'left' : 'right' ?> rec-category-contain">
					
					<div class="rec-catimage"><a href="/recipes/<?php echo strtolower($category_link); ?>/<?php echo strtolower(str_replace(array(' ', '-'), '-', $item->node_title)); ?>"  clear="all"><img src="http://a.hilanddairy.envoydev.com/sites/default/files/<?php echo $item->field_recipe_image->filename; ?>" width="330" height="220" border="0"></a></div>
					<p><a href="/recipes/<?php echo strtolower(str_replace('_', '-', $category_link)); ?>/<?php echo strtolower(str_replace(array(' ', '-'), '-', $item->node_title)); ?>"  clear="all"><?php echo $item->node_title; ?></a></p>
					</div>
					<?php endforeach; ?>
					
			</div>
		
			<br clear="all">
	</div>
</div>