<div id="content-container">
	<div id="content" class="sign-up-form">
		
		<form action="/account/sign_up_submit" method="post">
			<input name="restaurant" value="1" type="hidden" />
			<ul class="formsignup">
				<fieldset class="personal-info">
					<legend>Step 1</legend>
					<h2>Personal Information</h2>
					<li>
						<label for="first-name">First Name</label><br />
						<input id="first-name" name="first-name" value="" type="text" placeholder="First Name" />
					</li>
					<li>
						<label for="last-name">Last Name</label><br />
						<input id="last-name" name="last-name" value="" type="text" placeholder="Last Name" />
					</li>
					<li>
						<label for="zip-code">Zip Code</label><br />
						<input id="zip-code" name="zip-code" value="" type="text" placeholder="Zip Code" />
					</li>
				</fieldset>
				
				<fieldset class="login-info">
					<legend>Step 2</legend>
					<h2>Login Information</h2>
					<li>
						<label for="email-address">Email Address</label><br />
						<input id="email-address" name="email-address" value="" type="text" placeholder="Email" />
					</li>
					<li>
						<label for="password">Password</label><br />
						<input id="password" name="password" value="" type="password" placeholder="Password" />
					</li>
					<li>
						<label for="confirm-password">Confirm Password</label><br />
						<input id="confirm-password" name="confirm-password" value="" type="password" placeholder="Confirm Password" />
					</li>
				</fieldset>
				
				<fieldset class="restaurants hidden">
					<li>
						<label for="restaurant">Restaurants</label>
					</li>
				</fieldset>
				
				<fieldset class="newsletter-submit">
					<li>
						<label for="newsletter">Newsletter</label>
						<input id="newsletter" name="newsletter" value="1" type="checkbox" checked="checked" />
						<p>Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy. Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy.</p>
					</li>
					<li>
						<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
					</li>
				</fieldset>
			</ul>
		</form>
</div><!--end content-container-->