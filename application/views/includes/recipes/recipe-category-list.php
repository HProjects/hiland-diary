<div class="budget-friendly">
	<h2>Budget Friendly</h2>
	<ul>
		<li>
			<?php foreach($data->budget_friendly as $budget_friendly):?>
				<div>
					<a href="/recipes/budget_friendly/<?php echo strtolower(str_replace(array(' ', '-'), '-', $budget_friendly->node_title)); ?>">
					<?php echo $budget_friendly->node_title;?></a>
				</div>
			<?php endforeach; ?>
		</li>
	</ul>
</div>