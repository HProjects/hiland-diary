<div id="content-container">
	<div id="staticleft">
		<p>Log in to download the <br />latest coupons.</p>
		<p><a class="btn-coupon-log-in" href="/account/sign-in"><span class="hide">Log In</span></a></p>
		<p><strong>Not a member?</strong><br />Sign in to take advantage of exclusive coupons.</p>
		<p><a href="/account/sign-up" class="btn-coupon-sign-in" href="/coupons"><span class="hide">Sign In</span></a></p>
		<p style="line-height: 1.1; font-style: italic; font-size: 11px; margin-bottom: 6px; "><em>Please note: we have updated our security, you may need to sign up again to receive our coupons.</em></p>
	</div>
	
	
<!--
	
	<div id="staticleft_signup">
		<h2>Get E-mail Updates</h2>
		<p>Sign in to receive e-mail newsletters with recipes, news and information about Hiland Dairy.</p>
		<form action="/account/sign-in" method="post">
	


			<p><label for="email-address">E-mail:</label><br />
			<input name="email-address" type="text" id="email-address" size="23" /></p>
			<p><label for="password">Password</label><br />
			<input type="text" name="password" id="password" /></p>
		<p><input type="submit" value="Subscribe" id="sub" name="sub" class="signup-cta" /></p>
		</form>
	</div>
-->
	
	
	
	
	
	<div id="content">
		<h1 class="fontface">Coupons</h1>
		<p><img src="/img/coupon_graphics3.jpg" width="266" height="331" align="right">
	Hiland Dairy makes it easy to save on fresh, wholesome dairy products with valuable online coupons you can print and take to your grocer. Check with your grocer before use as not all grocers accept all Hiland Dairy coupons. Register today!</p>
		<p style="font-style:italic; font-size:12px;">In an effort to provide greater security for our customers, we've revamped our coupon sign-up process. To receive our exclusive coupons you'll need to sign-up here, even if you have a login from 2012 or before. It's easy, secure, and most importantly, you'll save money!</p>
		<h2>Current coupon offers:</h2>
		<ul>
			<li>40&cent; off any Square Ice Cream</li>
			<li>35&cent; off any Iced Coffees</li>
			<li>25&cent; off any 1/2 Gal. Apple or O.J.</li>
			<li>30&cent; off any 1/2 Gal. Chocolate Milk</li>
		</ul>

		<!--<p><a href="/account/sign-up">Sign up and start saving!</a></p>
		
		
	
	<h3>Get your Coupons</h3>
		<p><a href="/account/sign-up">Sign up for Hiland Dairy Online Coupons.</a></p>
-->
		
		<h3>About Hiland Dairy Online Coupons</h3>
		<p>Redemption of online coupons is at the sole discretion of each individual retailer. Hiland Dairy cannot guarantee online coupon redemption or confirm participating stores; you may want to check with your retailer(s) of choice before attempting to redeem online coupons.</p>
		<p>By signing up, you will be able to access and print Hiland Dairy coupons. New coupons are offered every 30-60 days.</p>
		<p><a href="/account/sign-up">Sign up for our Coupons and eNewsletter</a> and you will also be notified of coupons available for printout that coincide with periodic mailings or special offers.</p>
		<p>We strive to provide only the best services for our loyal customers and we are pleased to offer you a variety of printable online coupons. However, due to the complexities associated with individual computer systems, printers and Internet browsers, we are unable to offer any technical support regarding issues with the printing of online coupons. Please contact your Internet service provider and/or consult the owner’s manual of your computer or printer for further assistance.</p>
		<p><a href="/privacy-policy/">Read our privacy and coupon policies.</a></p>
		<p><img src="/img/line_gauss-one.jpg" width="692" height="30"></p>
		<p><img src="/img/coupon_fsi.jpg" width="172" height="135" align="right"><strong>Check your local Sunday newspaper inserts for valuable coupons from Hiland Dairy!</strong></p>
		<ul>
			<li>07/28/13</li>
			<li>08/18/13 (Limited Markets)</li>
			<li>09/15/13</li>
			<li>09/29/13</li>
		</ul>
		
	</div>
</div>