<div id="content-container" class="content-container-recipes">
	<ul data-role="listview">
		<?php foreach($list as $k => $item): ?>
			<li>
				<a href="/recipes/<?php echo strtolower(str_replace('_', '-', $category_link)); ?>/<?php echo strtolower(str_replace(array(' ', '-'), '-', $item->node_title)); ?>"  clear="all">
					<img class="ui-li-thumb" src="http://a.hilanddairy.envoydev.com/sites/default/files/<?php echo $item->field_recipe_image->filename; ?>">
					<h3><?php echo $item->node_title; ?></h3>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>