<!-- Start Footer -->
</div>
	<div id="footer"><div align="center">
		<div id="footercontainer">
			<div id="footerleft">
				<p><a href="/">Home</a> | <a href="/products">Products</a> | <a href="/recipes">Recipes</a> | <a href="/coupons">Coupons</a> | <a href="/our-mission">Our Mission</a> | <a href="/out-and-about">Promotions</a><br />
				<a href="/privacy-policy">Privacy Policy</a> | <a href="/terms-and-conditions">Terms & Conditions</a> | <a href="/company/contact-us">Contact Us</a></p>
				<hr />
				<p><strong style="color:#fff;">Company Links</strong></p>
				<p><a href="/company/awards">Awards</a> | <a href="/company/community">Community</a> | <a href="/company/contact-us">Contact Us</a> | <a href="/company/employment">Employment</a> | <a href="/company/faqs">FAQS</a> | <a href="/company/food-service">Food Service</a><br />
				<a href="/company/about-us">About Us</a> | <a href="/company/media-center">Media Center</a> | <a href="/company/related-links">Related Links</a> | <a href="/company/school-milk-program">School Milk Program</a>  | <a href="http://hilanddairy.com/image-library/" target="_blank">Image Library</a> 
			</div>
			<div id="footerright"><p>&copy; Copyright <?php echo date('Y'); ?> Hiland Dairy. All Rights Reserved.</p>
			<div class="no-no">
					<p><a href="/our-mission">NO artificial growth hormones</a></p>
				</div>
			</div>
		</div>
		</div></div>		
	</div>
</body>
</html>