<div class="contact-us">
<p class="required-fields contact">All fields are required.</p>
<form action="/contact-us/submit-school-milk" method="post">
	<ul class="form-contact-us">
		<fieldset class="personal-info">
			<legend>Step 1</legend>
			<h2>Personal Information</h2>
			<li>
				<label for="first-name">First Name</label><br />
				<input id="first-name" name="first-name" value="" required type="text" placeholder="First Name" />
			</li>
			<li>
				<label for="last-name">Last Name</label><br />
				<input id="last-name" name="last-name" value="" required type="text" placeholder="Last Name" />
			</li>
			<li>
				<label for="city">City</label><br />
				<input id="city" name="city" required value="" type="text" placeholder="City" />
			</li>
			<li>
				<label for="state">State</label><br />
				<select name="state" id="state"> 
				<option value="" selected="selected">Select a State</option> 
				<option value="AL">Alabama</option> 
				<option value="AK">Alaska</option> 
				<option value="AZ">Arizona</option> 
				<option value="AR">Arkansas</option> 
				<option value="CA">California</option> 
				<option value="CO">Colorado</option> 
				<option value="CT">Connecticut</option> 
				<option value="DE">Delaware</option> 
				<option value="DC">District Of Columbia</option> 
				<option value="FL">Florida</option> 
				<option value="GA">Georgia</option> 
				<option value="HI">Hawaii</option> 
				<option value="ID">Idaho</option> 
				<option value="IL">Illinois</option> 
				<option value="IN">Indiana</option> 
				<option value="IA">Iowa</option> 
				<option value="KS">Kansas</option> 
				<option value="KY">Kentucky</option> 
				<option value="LA">Louisiana</option> 
				<option value="ME">Maine</option> 
				<option value="MD">Maryland</option> 
				<option value="MA">Massachusetts</option> 
				<option value="MI">Michigan</option> 
				<option value="MN">Minnesota</option> 
				<option value="MS">Mississippi</option> 
				<option value="MO">Missouri</option> 
				<option value="MT">Montana</option> 
				<option value="NE">Nebraska</option> 
				<option value="NV">Nevada</option> 
				<option value="NH">New Hampshire</option> 
				<option value="NJ">New Jersey</option> 
				<option value="NM">New Mexico</option> 
				<option value="NY">New York</option> 
				<option value="NC">North Carolina</option> 
				<option value="ND">North Dakota</option> 
				<option value="OH">Ohio</option> 
				<option value="OK">Oklahoma</option> 
				<option value="OR">Oregon</option> 
				<option value="PA">Pennsylvania</option> 
				<option value="RI">Rhode Island</option> 
				<option value="SC">South Carolina</option> 
				<option value="SD">South Dakota</option> 
				<option value="TN">Tennessee</option> 
				<option value="TX">Texas</option> 
				<option value="UT">Utah</option> 
				<option value="VT">Vermont</option> 
				<option value="VA">Virginia</option> 
				<option value="WA">Washington</option> 
				<option value="WV">West Virginia</option> 
				<option value="WI">Wisconsin</option> 
				<option value="WY">Wyoming</option>
				</select>
			</li>
			<li>
				<label for="email-address">Email Address</label><br />
				<input id="email-address" name="email-address" required value="" type="email" placeholder="Email" />
			</li>
			<li>
				<label for="phone-number">Phone Number</label><br />
				<input id="phone-number" name="phone-number" value="" type="text" placeholder="555-555-5555" />
			</li>
		</fieldset>
		
		<fieldset class="concerns">
			<legend>Step 2</legend>
			<li>
				<label for="textarea-concerns">Voice your comments.</label><br />
				<textarea id="textarea-concerns" name="textarea-concerns" required placeholder="Your comments..." cols="73" rows="15"></textarea>
			</li>
		</fieldset>
		
		<fieldset class="submit">
			<legend>Step 3</legend>
			<li>
				<label for="contact-you">Should we contact you regarding this matter?</label><br />
				<input id="contact-you" name="contact-you" value="1" type="checkbox" checked="checked" /><br />
				<span>Yes</span>
			</li>
			<li>
				<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
			</li>
		</fieldset>
	</ul>
</form>
</div>