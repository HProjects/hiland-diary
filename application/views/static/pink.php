<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="contentpink">
<!-- HEADER -->
		<h1 class="fontfacepink">Thank you for supporting the Hiland for PINK – Breast Cancer Awareness campaign!</h1>
		<div class="pinkright">	
		<p>“After a bad day with chemo I came home and opened your letter approving me for financial aid. I fell to my knees and started crying and thanking God and BCFO for answering my prayers. Thank you so much from the bottom of my heart”.</p>
<p>“Thank you so very much for all the work and support that you do for all of us that are going through difficult times faced with cancer. Your organization is truly a blessing. I am so grateful and thankful for all the help BCFO has given me”.</p>
<p>“It is such a blessing to have a local organization like yours that is willing to help a family like ours. The kind of financial aid that BCFO has given my family has truly blessed us and taken so much stress off our lives. Words cannot express how much my family is grateful for all you have
done”.</p>
</div>
	
	
		<p>Because we know the importance of being a good neighbor and community member, Hiland Dairy is helping to raise awareness and funds to support families impacted by breast cancer for the third consecutive year. We actively support local and regional causes, events and organizations that seek to help make our communities better places to live.</p>
	
		
		<h2 class="pinktwo">Thank you for supporting the Hiland for PINK – Breast Cancer Awareness campaign</h2>
		<p>Organizations like the Breast Cancer Foundation of the Ozarks (BCFO) exist solely to bring hope, assistance and resources to those families in our region who have been impacted by breast cancer.

		<h2 class="pinktwo">Product Sales</h2>

		<p>Hiland Dairy will donate 5 cents for each YoPhoria Smoothie, Lactose Free Milk and Iced Coffee sold, and 20 cents for every 4-quart pail of ice cream sold between July 1 and September 30.</p>

		<h2 class="pinktwo">Facebook</h2>

		<p><a href="http://www.facebook.com/HilandDairy" target="_blank">“Like” our Facebook page</a>, and Hiland for PINK will donate $1 for every new Facebook fan benefiting breast cancer awareness.</p>


	<!-- <h2 class="pinktwo">Community Events</h2>
	<p>July 22 – Pink in the Park kickoff event with the Springfield Cardinals. <a href="#">Learn More</a></p>

		<h2 class="pinktwo">SHARE this campaign with family and friends!</h2>

		<p>
		<a href="mailto:?Subject=Help%20Support%20Hiland%20for%20PINK&body=type+your&body=http://hilanddairy.com/pink" target="_top">Help Support Hiland for PINK by sharing this page via Email</a></p>
<p>
<a href="https://twitter.com/HilandDairy" target="_blank"><img src="img/btn_twttr2.gif" width="34" height="29" alt="Twitter" hspace="0" vspace="0" border="0" style="display:block;"/></a>
<a href="http://www.facebook.com/HilandDairy" target="_blank"><img src="img/btn_fb2.gif" width="34" height="29" alt="Facebook" hspace="0" vspace="0" border="0" style="display:block;"/></a>
</p>-->
 
		<p>*For product purchases, including 20 cents for each Hiland Dairy 4-quart ice cream pail sold, and 5 cents for each YoPhoria Smoothie, Lactose Free Milk and Iced Coffee sold through Hiland Dairy as well as $1 for every new Facebook fan on Hiland Dairy’s Facebook page, Hiland Dairy will make a donation to the Breast Cancer Foundation of the Ozarks of up to $25,000 during the promotion period of July 1 – September 30, 2013.</p>
		
		<p><img src="/img/pinkBanner.jpg" width="680" height="150" alt="Hiland for Pink" /></p>
			</div>
</div>