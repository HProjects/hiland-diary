<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
		<?php echo $sidebar_newsletter_r; ?>
	</div>
	<div id="content" class="restaurants-wrapper">

		<h1 class="fontface">Satisfy Diners with Fresh,<br /> Wholesome Dairy Products</h1>
		
<p>Only the finest ingredients and a healthy bottom-line will keep your diners coming back for more. Hiland Dairy helps you achieve both with fresh, wholesome dairy products that are made nearby and add richness, texture and flavor to your dairy dishes.</p>
<!--

<h2>Sign Up for our eNewsletter</h2>

<p>Get recipe ideas and product updates delivered to your email inbox with the Hiland Dairy eNewsletter. </p>

<p><a href="#" class="signupnow_newsletter"><span>Sign Up Today!</span></a></p>
-->


<h2><img src="/img/land_restaurant.jpg" width="320" height="425" align="right">Hiland Dairy Has a Plant Near You!</h2>

<p>Our manufacturing plants are strategically placed to deliver our fresh, wholesome dairy products to your restaurant or food service facility. <a href="/company/contact-us">Contact us</a> today to get in touch with the Hiland Dairy manufacturing plant nearest to you.</p>

<p><strong>Hiland Dairy Plant Locations</strong></p>

	<ul>
		<li>Chandler, Oklahoma</li>
		<li>Fort Smith, Arkansas</li>
		<li>Fayetteville, Arkansas</li>
		<li>Little Rock, Arkansas</li>
		<li>Norfolk, Nebraska</li>
		<li>Norman, Oklahoma</li>
		<li>Springfield, Missouri</li>
		<li>Wichita, Kansas</li>
		<li>Des Moines, Iowa</li>
		<li>Kansas City, Missouri</li>
		<li>Omaha, Nebraska</li>
	</ul>


<h2>Product Questions? Inventory needs? </h2>

<p><a href="/company/contact-us">Contact us</a> for answers to your questions about which Hiland Dairy products are available in your area. We’ll respond to your inquiry right away! </p>
			</div>
</div>