<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- LOCATIONS -->
		<h1 class="fontface">Locations</h1>
		<h2>Serving the Midwest for More Than 75 Years</h2>
		<p>For more than 75 years, Hiland Dairy has been delivering fresh, wholesome dairy products to the Midwest. Hiland is proud to be farmer-owned producing dairy products with milk from local dairy farms, most located within 100 miles of our 11 Hiland Dairy plants in Arkansas, Kansas, Missouri, Nebraska and Oklahoma. As a full-service dairy, Hiland&apos;s quality products include milk, buttermilk, egg nog, ice cream, yogurt, cottage cheese, sour cream and dips, half and half, whipping cream, butter and cheese. Hiland also produces fresh orange and apple juice, lemonade and fruit punch. In addition, Hiland Dairy produces and distributes Red Diamond Tea, Health Wise Probiotic Yogurt and YoPhoria Smoothies. The company serves customers in several states, including Arkansas, Colorado, Iowa, Illinois, Kansas, Missouri, Nebraska, Oklahoma, South Dakota, Wyoming and Texas.</p>

<p>Hiland Dairy remains committed to delivering wholesome dairy products from milk that is naturally delicious and full of important nutrients &mdash; with no antibiotics and no artificial growth hormones. We care deeply about preserving our water, land and natural resources for future generations. To that end, we also reduce waste, reuse resources and recycle what we can so that those same future generations will be able to enjoy the same high quality milk you enjoy today.</p>
		
		<h2><img src="../img/coverage_map.jpg" width="313" height="418" align="right" />Hiland Dairy Plant Information</h2>
		<p>Need to get in touch with one of our local dairy plants? Here’s how:</p>
			<ul>
				<li><strong>Springfield, MO</strong><br />
				1133 East Kearney • Springfield, MO 65803<br />
417-862-9311 • 800-492-4022</li>
				<li><strong>Omaha, NE</strong><br />
				2901  Cuming St. • Omaha, NE 68131<br />
402-344-4321 • 800-779-4321</li>
				<li><strong>Chandler, OK</strong><br />
				1100 Thunderbird Road • Chandler, OK 74834<br />
405-258-3100 • 800-933-2697</li>
				<li><strong>Fayetteville, AR</strong><br />
					301 East 15th Street • Fayetteville, AR 72701<br />
479-521-1707 • 800-541-4912</li>
				<li><strong>Fort Smith, AR</strong><br />
301 N. 10th Street • Fort Smith, AR 72901<br />
479-782-0383 • 800-541-7945</li>
				<li><strong>Norman, OK</strong><br />
				302 South Porter • Norman, OK 73071<br />
405-321-3191 • 800-366-6455</li>
				<li><strong>Kansas City, MO</strong><br />
				3805 S. Emanuel Cleaver II Blvd. • Kansas City, MO 64128<br />
816-921-7370 • 800-279-1692<br /></li>
				<li><strong>Wichita, KS</strong><br />
				700 East Central • Wichita, KS 67202<br />
316-267-4221 • 800-336-0765</li>
				<li><strong>Little Rock, AR</strong><br />
				6901 Interstate 30 • Little Rock, AR 72209<br />
501-748-1700 • 800-365-1551</li>
				<li><strong>Norfolk, NE</strong><br />
				700 E. Omaha Ave. • Norfolk, NE 68701<br />
402-371-3660 • 800-373-6455</li>
			</ul>
<!-- =end LOCATIONS -->
	</div>
</div>