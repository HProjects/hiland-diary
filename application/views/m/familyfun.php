<div class="content-container">
	<h1 class="fontface">Welcome to the Florida Family Fun Getaway Contest!</h1>
	<h2>Enter below to WIN!</h2>
	
	
	<p>Picture you and your family having fun in the sun&mdash;enjoying the sights, sounds and thrills of Orlando's world-famous theme parks! Well, you won't have to picture it when you win a trip for four in the Hiland Dairy Florida Family Fun Getaway! All you have to do is enter your information below and you could be packing for Florida soon!</p>
	
	<p>View the <a href="/familyfunrules">Contest Rules</a>.</p>
	<br clear="all">
	
	<p>
	<form class="user-recipe validate" name="contest" id="familyfun" method="post" action="/familyfun/submit" data-ajax="false" onsubmit="return validateForm()">
	
			<div class="clear" style="margin-top:10px;">
				<label for="name">Full Name:</label><br />
				<p>Field is required.</p>
				<input type="text" name="name" placeholder="Full name" class="required" />
			</div>
			<div class="clear">
				<label for="address">Address:</label><br />
				<p>Field is required.</p>
				<input type="text" name="address" placeholder="Address" class="required" />
			</div>
			<div class="clear">
				<label for="city">City:</label><br />
				<p>Field is required.</p>
				<input type="text" name="city" placeholder="City" class="required" />
			</div>
			<div class="clear">
				<label for="state">State:</label><br />
				<select name="state">
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
				</select>
				</div>
				
				<div class="clear">
					<label for="zipcode">Zipcode:</label><br />
					<p>Field is required.</p>
					<input type="text" name="zipcode" placeholder="Zipcode" class="required" />
				</div>
				
				<div class="clear">
					<label for="phone">Phone Number:</label><br />
					<p>Field is required.</p>
					<input type="text" name="phone" placeholder="555-555-5555" class="required" />
				</div>
				
				<div class="clear">
					<label for="email">Email Address:</label><br />
					<p>Field is required.</p>
					<input type="email" name="email" placeholder="Email" class="required email" />
				</div>
				
				
				<div class="clear">
					<label class="text-checkbox" for="rules">I have read and understood the <strong>contest rules</strong>.<br />
					<p>Field is required.</p>
					<input class="checkbox" type="checkbox" name="rules" value="Yes" class="required" /></label>
					<p class="fineprint fine" style="display:block;">Read <a href="/familyfunrules">Contest Rules</a>.</p>
					
					<label class="text-checkbox" for="signup">Yes, I would like to receive email promotions.<br />
					<input class="checkbox" type="checkbox" name="signup" value="Yes" /></label>
				</div>
				<input data-role="submit" type="submit" value="Enter Now &raquo;" onclick="return emptyField" />
		</form>
		</p>
</div>