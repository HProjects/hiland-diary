<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_assistance; ?>
		<?php echo $sidebar_adobe; ?>
	</div>
<ul class="faq-list hrforms">
	<h1 class="fontface">Human Resources Forms</h1>
	<p class="hr-notice">All documents should be printed on 8 1/2 x 11 paper unless otherwise noted.</p>
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">New Hire</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/new_hire_forms/Guide_to_Completing_New_Hire_Paperwork.pdf">Guide to Completing New Hire Paperwork</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_1_Request_for_New_Hire.pdf">Request for New Hire</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_2_Employment_Application.pdf">Employment Application</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_3_Job_Description.pdf">Job Description</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/release_auth_for_compufact.pdf">Notice and Authorization</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_25_EEOCSelfIDForm.pdf">EEOC Self ID Form</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/Conditional_Job_Offer.pdf">Conditional Job Offer &amp; Medical Review</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_7_Change_Notice.pdf">Change Notice</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_8_I-9_Form.pdf">I-9 Form</a><strong> (Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_9_I-9_Supporting_Documents.pdf">I-9 Supporting Documents</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_10-Form_W-4_2013.pdf">Form W-4 2013</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_11_AR.pdf">Form AR4EC State of Arkansas Employee&rsquo;s Withholding Exemption Certificate</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/Kansas W-4.pdf">Form K-4</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_11_MO.pdf">Form MO W-4</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_12_Felony_and_SexOffenders_Affadavit.pdf">Freedom From Felony or Sex Offenders Convictions Affidavit</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_13_Advance_Pay_Request.pdf">Acknowledgement of Notification of Pay Advance (Commission-paid drivers only)</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_14_Direct_Deposit_Form.pdf">Direct Deposit Form</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH-15-Plant-Safety-Rules.pdf">Plant Safety Rules</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_16_Receipt_of_Fleet_Safety_Rules.pdf">Fleet Safety Rules</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_17_Personal_Hygiene_Policy.pdf">Personal Hygiene Policy</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_18_Code_of_Ethics_Policy_Acknowledgment.pdf">Code of Ethics</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_18-245_Code_of_Ethics_Acknowledgment_for_Teamsters_245.pdf">Code of Ethics - 245</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_18-823_Code_of_Ethics_Acknowledgment_for_Teamsters_823.pdf">Code of Ethics - 823</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/Policy_Against_Harassment.pdf">Policy Against Harassment</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_19-MS-Acknowledgment_for_Managers_Supervisors.pdf">Policy Against Harassment (for Managers and Supervisors)</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_20_Receipt_of_Special_Instructions_Booklet.pdf">Receipt of Special Instructions Sales Drivers&rsquo; Book</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Medical-Dental-Enrollment-Form.pdf">Medical &amp; Dental Insurance Enrollment Form</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Medical-Dental-Enrollment-Form-Wichita.pdf">Medical &amp; Dental Insurance Enrollment Form (Wichita)</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/HR-INS-002_Term_Life_Enrollment_Form_NU.pdf">Term Life Enrollment</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/HR-INS-003_Term_Life_Enrollment_Form_Local_245.pdf">Term Life Enrollment - 245</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_23-T_Term_Life_Enrollment_Form_Teamsters.pdf">Term Life Enrollment - T</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/HR-INS-005_Term_Life_Enrollment_Form_UFCW.pdf">Term Life Enrollment - UFCW</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_24_Access_to_Medical_Exposure_Records.pdf">Access to Medical &amp; Exposure Records</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/VETs_and_Disabled_Self_Identification_Form.pdf">VETs and Disabled Self Identification Form</a></li>
				<li><a target="_blank" href="/pdfs/new_hire_forms/NH_26_PE_Drug_Screen_Physical.pdf">Pre-Employment Drug Screen &amp; Physical</a></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">FMLA and STD</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/fmla_std/FMLALeaveofAbsenceProcedure.pdf">FMLA Leave of Absence Procedure</a></li>
				<!--DOWNLOAD--><li><a href="/pdfs/fmla_std/Short-Term_Disability_Guidelines_Claim_Form.pdf.zip">Short-Term Disability Guidelines and Claim Form</a><br />
				<em>(Download only &ndash; zip file)</em></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Insurance</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/insurance_forms/Request_to_Decline_Insurance.pdf">Request to Decline Insurance</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Life_Beneficiary_Change_Form.pdf">Life&mdash;Beneficiary Change Form</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Life_Dependent_Information_Attach_to_Life_Enrollment.pdf">Life&mdash;Dependent Information&ndash;Attach to Life Enrollment</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/HR-INS-002_Term_Life_Enrollment_Form-NU.pdf">Life&mdash;Term Life Enrollment Form-NU</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Medical-Dental-Enrollment-Form.pdf">Medical &amp; Dental Insurance Enrollment Form</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/Medical-Dental-Enrollment-Form-Wichita.pdf">Medical &amp; Dental Insurance Enrollment Form (Wichita)</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/ProviderSearch.pdf">Medical&mdash;How to Locate a Network Provider</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/PrescriptionMailOrderEnrollmentForm.pdf">Prescription&mdash; Mail Order Enrollment Form</a></li>
				<li><a target="_blank" href="/pdfs/insurance_forms/PrescriptionDrugClaimForm.pdf">Prescription&mdash;Claim Form (for reimbursement)</a></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Accident</h3>
		<div>
			<ul class="hr-forms">
			
				<li class="first faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Fleet Accident Forms</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/accident_forms/Fleet_Accident_Procedures.pdf">Fleet Accident Procedures</a></li>
							<li><a target="_blank" href="/pdfs/accident_forms/Steps_to_Follow.pdf">Steps to follow when you are involved in an accident</a></li>
							<li class="last"><a target="_blank" href="/pdfs/accident_forms/Fleet_Accident_Form.pdf">Fleet Accident Investigation Form</a></li>
						</ul>
					</div>
				</li>
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Work Comp Forms</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/accident_forms/Workers_Comp_Claim_Form.pdf">Work Comp Claim Form</a></li>
							<li><a target="_blank" href="/pdfs/accident_forms/Accident_Investigation_Report_Form.pdf">Work Comp Investigation Form</a></li>
							<li class="last"><a target="_blank" href="/pdfs/accident_forms/Workers_Comp_RTW.pdf">Return to Work Form</a></li>
						</ul>
					</div>
				</li>
				
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Other</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/other_forms/reasonable-suspicion-observed-behavior-form.pdf">Reasonable Suspicion Observed Behavior</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/other_forms/Payroll_Status_Change_Authorization.pdf">Payroll Status Change Authorization</a> <strong>(Fillable Form PDF)</strong></li>
				<!--DOWNLOAD--><li><a href="/pdfs/other_forms/ApplicantLog.xls">Applicant Log</a><br />
				<em>(Download only &ndash; Excel file)</em></li>
				<li><a target="_blank" href="/pdfs/other_forms/ApplicantLogInstructions.pdf">Applicant Log Instructions</a></li>
				<li><a target="_blank" href="/pdfs/other_forms/Change_of_Address.pdf">Change of Address</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/other_forms/Change_Notice.pdf">Change Notice</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/other_forms/Notice_of_Retirement.pdf">Notice of Retirement</a></li>
				<li><a target="_blank" href="/pdfs/other_forms/Request_for_New_Hire.pdf">Request for New Hire</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/other_forms/Vacation_Request_Form.pdf">Vacation Request Form</a> <strong>(Fillable Form PDF)</strong></li>
				<li><a target="_blank" href="/pdfs/other_forms/Voluntary_Resignation.pdf">Voluntary Resignation</a></li>
				<!--DOWNLOAD--><li><a href="/pdfs/other_forms/Antitrust_Compliance_Brochure_and_Instructions_for_Printing.pdf">Antitrust Compliance Brochure</a><br /></li>
				<li><a target="_blank" href="/pdfs/other_forms/Antitrust_Compliance_Disclosure_Form.pdf">Antitrust Compliance Disclosure Statement</a></li>
				<li><a target="_blank" href="/pdfs/other_forms/Verbal_Warning_Form.pdf">Documentation of Verbal Warning</a> <strong>(Fillable Form PDF)</strong></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Job Ads</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/job_postings/HilandDriver4x2Ad.pdf">Hiland Driver 4 x 2 column</a></li>
				<li><a target="_blank" href="/pdfs/job_postings/HilandDriver4x4Ad.pdf">Hiland Driver 4 x 4 column</a></li>
				<li><a target="_blank" href="/pdfs/job_postings/HilandPlantWorker4x2Ad.pdf">Hiland Plant Worker 4 x 2 column</a></li>
				<li><a target="_blank" href="/pdfs/job_postings/HilandPlantWorker4x4Ad.pdf">Hiland Plant Worker 4 x 4 column</a></li>
			</ul>
		</div>
	</li>

	
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Driver Qualification File</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/driver_qualification/DriverQualificationFile.pdf">Driver Qualification Checklist</a></li>
				
				<li class="last"><a target="_blank" href="/pdfs/driver_qualification/Pkg1_DriversApplicationforEmployment0709.pdf">Driver&rsquo;s Application for Employment</a></li>
				
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Inquiry to Previous Employers</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/driver_qualification/previous/3-DIHF.pdf">DIHF</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/previous/4-PreviousPEEmployeeA&DTestStatement.pdf">Alcohol &amp; Drug History</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/previous/5-SafetyPerformanceHistoryRecordsRequest.pdf">Safety Performance History</a></li>
						</ul>
					</div>
				</li>
				
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Inquiry to State Agencies</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/driver_qualification/agencies/6-Request-for-Check-of-Driving-Record-Interactive-Form.pdf">Driving Record</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/agencies/7-State-Report.pdf">State Report</a></li>
						</ul>
					</div>
				</li>
				
				<li><a target="_blank" href="/pdfs/driver_qualification/Pkg4_AnnualReviewofDrivingRecord.pdf">Annual Review of Driving Record</a></li>
				
				<li><a target="_blank" href="/pdfs/driver_qualification/Pkg5_AnnualDriverCertificationofViolations.pdf">Annual Driver&rsquo;s Certification of Violations</a></li>
				
				<!--<li><a target="_blank" href="/pdfs/new_hire_forms/Cell_phone_Policy_Hiland.pdf">Cell Phone Policy - CMV Drivers</a></li> -->
				<li class="last"><a target="_blank" href="/pdfs/driver_qualification/Pkg6_DriverRoadTestCertificateorEquivalenet.pdf">Driver&rsquo;s Road Test Certificate</a></li>
				
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Medical Examinations</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/driver_qualification/medical/12-Medical-Examination-Report.pdf">Commercial Driver Fitness Determination</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/medical/12-Instructions-to-Medical-Examiner.pdf">Instructions to Medical Examiner</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/medical/13-Medical-Examiner's-Certificate.pdf">Medical Examiner's Certificate</a></li>
						</ul>
					</div>
				</li>
				
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Drug and Alcohol Testing</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/driver_qualification/alcohol/Alcohol&Conrolled-Substance.pdf">Company Policy &ndash; Alcohol &amp; Controlled Substance</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/alcohol/Receipt-Alcohol&Conrolled-Substance.pdf">Receipt of Company Policy</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/alcohol/15-Pre-Employment-Drug-Screen-Results.pdf">Drug Screen Results</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/alcohol/16-Commercial-Driver-License.pdf">CDL</a></li>
						</ul>
					</div>
				</li>
				
				
				<li class="faq-question hrform-inner">
					<h3 class="faq-toggler hrform-title">Additional Forms</h3>
					<div>
						<ul class="hr-forms">
							<li class="first"><a target="_blank" href="/pdfs/driver_qualification/additional/17-MVD-Certification-of-Compliance.pdf">Certification of Compliance</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/additional/18-Hours-of-Service-Record.pdf">Hours of Service</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/additional/19-Receipt-FMCSR-Pocketbook.pdf">FMC Pocketbook: Receipt</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/additional/20-Receipt-Drug&Alcohol-Testing.pdf">Drug &amp; Alcohol Testing booklet: Receipt</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/additional/21-Driver-Training-Certificate.pdf">Driver Training Certificate</a></li>
							<li><a target="_blank" href="/pdfs/driver_qualification/additional/Cell-Phone-Policy-for-CMV-Drivers.pdf">Cell Phone Policy for CMV Drivers</a></li>
						</ul>
					</div>
				</li>
				
			</ul>
		</div>
	</li>
	
	
	
	<h1 style="clear:both;" class="fontface">Required Workplace Posters</h1>
	<p class="hr-notice">All documents should be printed on 8 1/2 x 11 paper unless otherwise noted.</p>
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Federal &ndash; All Locations</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/federal/EEOC-THE-LAW.pdf">EEOC - THE LAW</a><br />
				<em>Print two 8 1/2 x 11 sheets and tape together.</em></li>
				<li style="border:0;padding:0;"><a target="_blank" href="/pdfs/federal/EPPA.pdf">EPPA - 11x17</a></li>
				<li><a target="_blank" href="/pdfs/federal/EPPA2-8.5x11.pdf">EPPA - 8 1/2 x 11</a><br />
				<em>Must be printed on one 11 x 17 sheet or two 8 1/2 x 11 sheets and taped together.</em></li>
				<li><a target="_blank" href="/pdfs/federal/FederalMinimumWage.pdf">Federal Minimum Wage</a></li>
				<li><a target="_blank" href="/pdfs/federal/FMLA.pdf">FMLA</a></li>
				<li style="border:0;padding:0;"><a target="_blank" href="/pdfs/federal/NLRA.pdf">NLRA - 11x17</a></li>
				<li><a target="_blank" href="/pdfs/federal/NLRA2-8.5x11.pdf">NLRA - 8 1/2 x 11</a><br />
				<em>Must be printed on one 11 x 17 sheet or two 8 1/2 x 11 sheets and taped together.</em></li>
				<li><a target="_blank" href="/pdfs/federal/OSHA.pdf">OSHA</a><br />
				<em>Must be printed on 8 1/2 x 14 paper.</em></li>
				<li><a target="_blank" href="/pdfs/federal/USERRA.pdf">USERRA</a></li>
				<p class="hr-notice" style="color:#555; margin:15px 0 4px;">Both English and Spanish versions must be posted.</p>
				<li style="border:0;padding:0;"><a target="_blank" href="/pdfs/federal/E-VerifyParticipation-English.pdf">E-Verify Participation- English</a></li>
				<li><a target="_blank" href="/pdfs/federal/E-VerifyParticipation-Spanish.pdf">E-Verify Participation- Spanish</a></li>
				<p class="hr-notice" style="color:#555; margin:15px 0 4px;">Both English and Spanish versions must be posted.</p>
				<li style="border:0;padding:0;"><a target="_blank" href="/pdfs/federal/RightToWork-English.pdf">Right to Work- English</a></li>
				<li><a target="_blank" href="/pdfs/federal/RightToWork-Spanish.pdf">Right to Work- Spanish</a></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Arkansas</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/arkansas/AR-How-to-claim-unemployment-insurance.pdf">AR-How to claim unemployment insurance</a></li>
				<li><a target="_blank" href="/pdfs/arkansas/AR-DOL-Notice-to-Employer-and-Employee.pdf">AR DOL - Notice to Employer and Employee</a><br />
				<em>Must be printed on 8 1/2 x 14 paper.</em></li>
				<li><a target="_blank" href="/pdfs/arkansas/FormP-workersCompensationInstructions.pdf">Form P - workers compensation instructions</a><br />
				<em>Must be printed on 8 1/2 x 14 paper.</em></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Kansas</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/kansas/KS-Work-Comp.pdf">KS - Work Comp</a></li>
				<li><a target="_blank" href="/pdfs/kansas/KS-EEOC-Poster.pdf">KS EEOC Poster</a></li>
				<li><a target="_blank" href="/pdfs/kansas/Unemployment-Insurance.pdf">Unemployment Insurance</a></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Missouri</h3>
		<div>
			<ul class="hr-forms">
				<li style="margin-top:15px;border:0;padding:0;"><a target="_blank" href="/pdfs/missouri/Discrimination_2-8.5x11.pdf">Discrimination 2-8.5x11</a></li>
				<li><a target="_blank" href="/pdfs/missouri/Discrimination_11x17.pdf">Discrimination 11 x 17</a><br />
				<em>Must print on one 11 x 17 sheet; or two 8 1/2 x 11 taped together.</em></li>
				<li><a target="_blank" href="/pdfs/missouri/MO-Minimum-Wage.pdf">MO Minimum Wage</a></li>
				<li style="border:0;padding:0;"><a target="_blank" href="/pdfs/missouri/MO-Work-Comp_2-8.5x11.pdf">MO Work Comp 2-8.5x11</a></li>
				<li><a target="_blank" href="/pdfs/missouri/MO-Work-Comp_11x17.pdf">MO Work Comp 11x17</a><br />
				<em>Must print on one 11 x 17 sheet; or two 8 1/2 x 11 taped together.</em></li>
				<li><a target="_blank" href="/pdfs/missouri/Unemployment-benefits.pdf">Unemployment benefits</a></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Oklahoma</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/oklahoma/OK-Its-the-Law.pdf">OK - It's the Law</a></li>
				<li><a target="_blank" href="/pdfs/oklahoma/OK-MinimumWageAct.pdf">OK - Minimum Wage Act</a></li>
				<li><a target="_blank" href="/pdfs/oklahoma/OK-DiscriminationLaw.pdf">OK Discrimination Law</a></li>
				<li><a target="_blank" href="/pdfs/oklahoma/OK-WorkComp.pdf">OK - Work Comp</a><br />
				<em>Appropriate person at location must sign the poster.</em></li>
			</ul>
		</div>
	</li>
	
	<li class="faq-question">
		<h3 class="faq-toggler hrform-title">Texas</h3>
		<div>
			<ul class="hr-forms">
				<li class="first"><a target="_blank" href="/pdfs/texas/TexasPayDayLaw.pdf">Texas Pay Day Law</a></li>
				<li><a target="_blank" href="/pdfs/texas/TX-OfficeofInjuredEmployeeCounsel.pdf">TX - Office of Injured Employee Counsel</a></li>
				<li><a target="_blank" href="/pdfs/texas/TX-WorkCompPoster.pdf">TX - Work Comp Poster</a></li>
			</ul>
		</div>
	</li>
</ul>
</div>