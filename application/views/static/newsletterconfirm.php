<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="content">
		<h1 class="fontface">Coming Soon to your Inbox!</h1>
		<h2>Thank you again for subscribing to our newsletter.</h2>
		<p><a href="/">Return to Hiland Dairy Home Page</a></p>
	</div>
</div>