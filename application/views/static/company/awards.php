<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- AWARDS -->
		<h1 class="fontface">Awards</h1>
		<p>Hiland Dairy plants are clean, organized and well-managed facilities that consistently produce fresh, high quality, wholesome dairy products. Our commitment to quality shows in the numerous awards our plants have won.</p>
		<h2>Hiland Dairy Company Awards</h2>
		<ul>
			<li>2012 Irving B. Weber Distinguished Award for Quality Excellence- Norfolk, NE</li>
			<li>2011 Wayne Gingrich Award for Production Excellence- Fort Smith, AR</li>
			<li>2011 Harlie F Zimmerman Award for Marketing Excellence</li>
			<li>2010 Irving B. Weber Award for Production Excellence- Springfield, MO</li>
			<li>2010 Wayne Gingrich Award for Production Excellence- Fort Smith, AR</li>
			<li>2008 Wayne Gingrich Award for Production Excellence finalist- Fort Smith, AR</li>
			<li>2008 Wayne Gingrich Award for Production Excellence finalist- Springfield, MO</li>
			<li>2007 John Q Hammons Founder’s Award</li>
			<li>2007 Harlie F Zimmerman Award for Marketing Excellence</li>
			<li>2007 International Dairy Foods Association Excellence Marketing Award</li>
			<li>2005 Wayne Gingrich Award for Production Excellence finalists- Fayetteville, AR, Fort Smith, AR, Norman, OK and Springfield, MO</li>
			<li>2005 Excellence in Ice Cream Quality- Chandler, OK and Norfolk, NE</li>
			<li>2005 Excellence in Cultured Products Quality- Chandler, OK, Norman, OK and Wichita, KS</li>
			<li>2004- Excellence in Milk Quality- Fayetteville, AR, Fort Smith, AR, Norman, OK and Springfield, MO</li>
			<li>2004- Excellence in Ice Cream Quality- Chandler, OK and Springfield, MO</li>
			<li>2004- Excellence in Cultured Products Quality- Chandler, OK, Norman, OK and Wichita, KS</li>
		</ul>
<!-- =end AWARDS -->

	</div>
</div>