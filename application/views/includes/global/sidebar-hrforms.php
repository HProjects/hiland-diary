<div id="hrforms-block">
	<div id="staticleft">
		<h2>For assistance with:</h2>
		<ul>
			<li>New Hire, FMLA and STD forms, contact Haylee Lumley</li>
			<li>Insurance and Accident forms, contact Tracie Heckenlively</li>
			<li>Driver Qualification File, contact Charolette Prince</li>
			<li>Other and Job Ads, contact Tracie Heckenlively or Cherlene Artman</li>
			<li>For assistance with Required Workplace Posters, contact Cherlene Artman.</li>
		</ul>
		<p>Springfield, MO<br />
		(417) 862-9311</p>
	</div>
</div>