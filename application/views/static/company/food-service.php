<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content" class="food-service-wrapper">
<!-- FOOD SERVICE -->
		<h1 class="fontface">Hiland Dairy Makes Food Service Delicious and Nutritious</h1>
		<p>One serving of Hiland Dairy milk, cheese or other dairy food product is packed with fresh flavor you and your guests can taste. Nearly all of our milk is produced with care by local farms within 100 miles of our dairy plants. We've left the antibiotics and hormones out and the nine essential nutrients in so that whether you stock up on our lower-sugar, fat-free, flavored milk or our more traditional offerings, you can serve Hiland Dairy milk in your cafeteria with confidence.</p>
		<p>For more information about Hiland Dairy products for your facility, please <a href="/company/contact-us">email us</a>.</p>
		
		<p>Are you a grocer or retailer looking for information on providing Hiland Dairy products in your stores? Visit our <a href="/grocers">Grocer page</a>.</p>
		
		<p>Are you a restaurant or food service facility looking for information on serving Hiland Dairy products to your customers? Visit our <a href="/restaurants">Restaurant page</a>.</p>
<!-- =end FOOD SERVICE -->

	</div>
</div>