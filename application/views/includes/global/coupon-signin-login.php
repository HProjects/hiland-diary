<div id="coupon-block">
	<div id="staticleft">
		<p>You will need to Log In to get the latest coupons. Not a member? Please sign in below to take advantage of our exclusive coupons.</p>
		<p><a class="btn-coupon-sign-in" href="/coupons"><span class="hide">Sign In</span></a></p>
		<p class="fineprint">-------- or ---------</p>
		<p><a class="btn-coupon-log-in" href="/coupons"><span class="hide">Log In</span></a></p>
	</div>
</div>