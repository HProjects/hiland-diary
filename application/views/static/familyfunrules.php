<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_news_generic; ?>
	</div>
	<div id="content" class="stillhometown-wrapper">
		<h1 class="fontface">Hiland Dairy's Florida Family Fun Trip Giveaway</h1>
		<h2>OFFICIAL SWEEPSTAKES RULES</h2>
		
		<ol>
			<li><p>No PURCHASE NECESSARY. A PURCHASE WILL NOT INCREASE YOUR CHANCES OF WINNING.</p></li>
			
			<li>
				<p><strong>Two ways to enter.</strong></p>
				<p><strong>On-line:</strong> Eligible persons may enter one (1) time per day via the website at <a href="/">www.hilanddairy.com</a>. In the event of a dispute as to the identity of the entrant, the authorized account holder of the email address used if enter, if applicable, will be deemed to be the entrant.  Sponsor reserves the right to disqualify any entrant that tampers or attempts to tamper with the e-mail entry process or the sweepstakes website.</p>
				<p><strong>Mail-in:</strong> Eligible persons may mail-in an entry, limited to one entry per person per day, mailed separately. Entries must be mailed to Hiland Dairy: PO Box 2270, Springfield, MO 65801 and must be clearly labeled &ldquo;Hiland Dairy Florida Family Fun Giveaway&rdquo;. Partially completed entries or mechanically reproduced entries will not be accepted. Sponsor assumes no responsibility for lost, misdirected, damaged, stolen, postage-due, illegible or late entries.  All entries become the property of the Sponsor and will not be returned.    Sweepstakes begins on 12:00:01 a.m. CST on June 17, 2013 and ends on 11:59:59 p.m CST on September 29, 2013.  Mail-in entries must be postmarked on or before September 29, 2013.</p>
			</li>
			
			<li><p>Sweepstakes open to legal residents of Kansas, Missouri, Nebraska, Iowa, Colorado, Wyoming, Texas, Arkansas and Oklahoma, age 18 or older, except employees of Hiland Dairy, its affiliates, subsidiaries, respective advertising, promotion and fulfillment agencies, and the immediate families of each.  Sweepstakes is void where prohibited by law.</p></li>
			
			<li><p>Winner will be randomly drawn from all eligible entries on or about October 4, 2013 by Hiland Dairy or its designated representative, whose decisions are final.  Except where prohibited, by accepting prize, winner consents to the use of his/her name and photograph or likeness by Hiland Dairy for publicity purposes without further compensation.  Winner will be notified by phone and/or mail and winner and travel guest(s) will be required to sign and return an affidavit of eligibility and liability release within fourteen (14) days of notification, or the prize will be forfeited and awarded to an alternate winner. Prize will only be delivered to addresses within the above-mentioned states.  Sponsor will make (2) attempts to contact the potential winner at the address on the entry form.  If after (2) attempts, Sponsor is unable to contact the potential winner, that entry/potential winner will be disqualified and an alternate potential winner's name will be drawn.  Only one Grand Prize will be awarded.</p></li>
			
			<li><p>One Grand Prize will be awarded and will consist of a $4000 voucher good towards a trip for up to four (4) to Orlando, Florida.  The voucher may be used towards round-trip coach-class airfare for up to two adults and two children from a major airport closest to winner?s home to Orlando, Florida, three nights/four days accommodations for two adults and two children, and 2-day park passes for two adults and two children good for use at one of the amusement parks to be selected by Sponsor. Total prize value will not exceed $4,000.00. The Grand Prize Trip must be booked and arranged through E Travel Omaha - <a href="http://www.etravelomaha.com" target="_blank">www.etravelomaha.com</a>. Odds of winning depend on number of eligible entries received.  Prize does not include meals, transfers or any other expenses not specifically included above and any other expenses not mentioned and any and all other expenses of the trip shall be the obligation of the winner and or guest(s). Sponsor reserves the rights to substitute a prize of equal or greater value in the event the promotion cannot be completed as contemplated by these Official Rules.  Some black out dates and other travel restrictions beyond the control of Sponsor may apply. Trip must be taken within one year of notification or prize will be forfeited. If potential prize winner is unable to travel within the time specified, the prize may be forfeited and an alternate winner may be selected from among all remaining entries as time permits. Upon forfeiture for any reason as stated herein, no compensation shall be given.  Sponsor will not be responsible for Acts of God, acts of terrorism, civil disturbances, work stoppages or any other natural disaster outside its control that may cause the cancellation or postponement of the trip.  No cash substitutions under any circumstances unless at the sole discretion of the Sponsor.  Travelers are responsible for all required travel documents.</p></li>
			
			<li><p>Prize is non-transferable, not returnable, and cannot be sold or redeemed for cash.  No substitutions allowed. No groups, clubs, newsletters or organizations may reproduce or distribute any portion of these Official Rules to its members. All federal, state, local taxes on prize are the responsibility of the winner.  By accepting prize, winner and travel guests agree that Hiland and their respective officers, director, agents and employees will have no liability or responsibility for any injuries, losses or damages of any kind resulting from the acceptance, possession or use of any prize and they will be held harmless against any claims of liability arising directly or indirectly from the prize awarded.</p></li>
			
			<li><p>A list of winner(s) can be obtained by sending a self-addressed stamped envelope to Hiland Dairy: PO Box 2270, Springfield, MO 65801 after September 30, 2013 but before October 25, 2013.</p></li>
			
			<li><p>Hiland Dairy is the Sponsor of this Sweepstakes.</p></li>
		</ol>
		
		<p><a href="/familyfun">&laquo; Back to Contest</a></p>
		
	</div>
</div>