<div class="content-container">
<ul data-role="listview">
		<?php foreach($recipes_categories as $category_key => $category_value):?>
			<li class="xpander_blue">
				<a href="/recipes/categories/<?php echo strtolower(str_replace(array(' ', '-'), '-', $category_key)); ?>">
					<img class="ui-li-thumb" src="/img/mobile-recipes/m-reci-<?php echo strtolower(str_replace(array(' ', '-'), '-', $category_key));?>.jpg" alt="" />
					<h3><?php echo strtoupper($category_key);?> (<?php echo $category_value;?>)</h3>
				</a>
			</li>
		<?php endforeach; ?>
</ul>
</div>
<!-- =end COLUMN RIGHT -->