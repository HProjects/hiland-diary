<div id="content-container">
<?php echo $products_side_nav; ?>
			<div id="content">
				<div id="content-column">
					<div id="content-product-contain" class="product-milk">
						<img src="/img/product_page_img.jpg" alt="Our Products!" />
					</div> 
					
					
			<!-- BUTTER -->
					<img src="img/product_butter.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Butter</h1>
					<p>Nothing beats the rich, creamy taste of fresh Hiland Dairy Butter. Whether you serve it with bread or use it in your beloved recipes, only Hiland Dairy Butter and spreads give you and your family a fresh goodness you can taste and see.</p>
					<p>Grab a pound or tub today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->butters as $butter): ?>
						<div class="xpander_blue">
							<a href="/products/butters/<?php echo strtolower(str_replace(' ', '-', $butter->tags)); ?>"><?php echo $butter->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end BUTTER -->
					
					
			<!-- BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->
					<img src="img/product_half-and-half.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Buttermilk / Half-and-Half / Whipping Cream</h1>
					<p>When you want to add the fresh taste of real cream or half-and-half to your drinks and dishes, you need Hiland Dairy. Once you experience the fresh, wholesome difference of Hiland Dairy creams and half-and-half you'll wonder how you cooked without it.</p>
					<p>Get a pint today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->creams_buttermilks as $buttermilk): ?>
						<div class="xpander_blue">
							<a href="/products/creams-buttermilks/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $buttermilk->tags))); ?>"><?php echo $buttermilk->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->


			<!-- CHEESE -->
					<img src="img/product_cheese.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Cheese</h1>
					<p>Check out the ingredients list on Hiland Dairy cheese and you'll see items you recognize. That's because Hiland Dairy cheese is made from real, wholesome milk. From Colby Jack to Sharp Cheddar, Mozzarella or Mexican Blend when you want real, good cheese, buy Hiland Dairy.</p>
					<p>Pick up a block or bag at your local grocery store today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->cheeses as $cheese): ?>
						<div class="xpander_blue">
							<a href="/products/cheeses/<?php echo strtolower(str_replace(' ', '-', $cheese->tags)); ?>"><?php echo $cheese->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end CHEESE -->
			
			
			<!-- COTTAGE CHEESE -->
					<img src="img/product_cottage-cheese.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Cottage Cheese</h1>
					<p>At Hiland Dairy, we know how to make fresh, tasty cottage cheese that you and your kids crave. We lightly salt our cottage cheese to highlight the fresh flavor of the local, hormone and antibiotic-free Hiland Dairy milk it's made from.</p>
					<p>Try one of our varieties of cottage cheese today &ndash; and we think you'll agree it tastes great!</p>
					<br clear="all">
					<p>
						<?php foreach($data->cottage_cheese as $cottage_cheese): ?>
						<div class="xpander_blue">
							<a href="/products/cottage-cheese/<?php echo strtolower(str_replace(' ', '-', $cottage_cheese->tags)); ?>"><?php echo $cottage_cheese->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end COTTAGE CHEESE -->
			
			
			<!-- DIPS -->
					<img src="img/product_dips.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Dips</h1>
					<p>Hiland Dairy dips add zip to any snack. Whether you're planning a pre-game party or just looking for a little something to spice up your chip routine, our fresh flavors are sure to satisfy. Made with real, wholesome Hiland Dairy sour cream, once you dip a chip, you'll wonder how you snacked so long without it.</p>
					<p>Serve our Southwest Ranch, Jalapeño Fiesta or French Onion Dips at your next gathering &ndash; they'll be the hit of the party!</p>
					<br clear="all">
					<p>
						<?php foreach($data->dips as $dip): ?>
						<div class="xpander_blue">
							<a href="/products/dips/<?php echo strtolower(str_replace(' ', '-', $dip->tags)); ?>"><?php echo $dip->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end DIPS -->
			
			
			<!-- EGG NOG 
					<img src="img/product_eggnog.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Egg Nog</h1>
					<p>Nothing says the Holidays like Hiland Dairy Egg Nog. With special Halloween, Holiday and Lite varieties, no other brand offers the sweet, creamy taste that's good enough to build traditions around.</p>
					<p>Start in your home with a seasonal favorite &ndash; Hiland Dairy egg nog!</p>
					<br clear="all">
					<p>
						<?php foreach($data->eggnog as $eggnog): ?>
						<div class="xpander_blue">
							<a href="/products/eggnog/<?php echo strtolower(str_replace(' ', '_', $eggnog->tags)); ?>"><?php echo $eggnog->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			 =end EGG NOG -->
			
			
			<!-- EGG SUBSTITUTE 
					<img src="img/product_eggsubstitute.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Egg Substitute</h1>
					<p>Tasty and satisfying, eggs are a delicious, nutritious anytime treat that are easy to miss while on a restricted diet. Hiland Dairy egg substitute is a healthy alternative to real eggs; with zero cholesterol, zero fat and 99 percent egg whites, you'll love missing eggs.</p>
					<p>Try a container of Hiland Dairy egg substitute today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->egg_substitute as $egg_substitute): ?>
						<div class="xpander_blue">
							<a href="/products/egg_substitute/<?php echo strtolower(str_replace(' ', '_', $egg_substitute->tags)); ?>"><?php echo $egg_substitute->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			 =end EGG SUBSTITUTE -->
			
			
			<!-- FRUIT DRINKS 
					<img src="img/product_fruit_drinks.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Fruit Drinks</h1>
					<p>Hiland Dairy fruit-flavored drinks are a refreshing complement to barbecues, birthday parties and soccer games. Treat your family to a gallon of Hiland Dairy grape, orange, fruit punch or your favorite flavor today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->fruit_drinks as $fruit_drink): ?>
						<div class="xpander_blue">
							<a href="/products/fruit_drinks/<?php echo strtolower(str_replace(' ', '_', $fruit_drink->tags)); ?>"><?php echo $fruit_drink->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			 =end FRUIT DRINKS -->
			
			
			<!-- ICED COFFEE -->
					<img src="img/product_iced_coffee.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Iced Coffee</h1>
					<p>Hiland Dairy Iced Coffees combine coffee with wholesome, healthy milk and add in rich flavors like mocha, caramel and vanilla to create a delicious, creamy beverage you are sure to enjoy!</p>
					<br clear="all">
					<p>
						<?php foreach($data->iced_coffee as $iced_coffee): ?>
						<div class="xpander_blue">
							<a href="/products/iced-coffee/<?php echo strtolower(str_replace(' ', '-', $iced_coffee->tags)); ?>"><?php echo $iced_coffee->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end COFFEE -->

			
			<!-- JUICE -->
					<img src="img/product_juices.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Juices</h1>
					<p>Hiland Dairy starts your morning off right with smooth, refreshing regular or calcium-fortified orange juice. Grab a gallon, half-gallon or six pack container at your local grocery store or mini-mart today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->juice as $juices): ?>
						<div class="xpander_blue">
							<a href="/products/juice/<?php echo strtolower(str_replace(' ', '-', $juices->tags)); ?>"><?php echo $juices->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end JUICE -->
			
			<!-- LACTOSE FREE MILK -->
					<img src="img/product_lactose_free_milk.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Lactose Free Milk</h1>
					<p>Cook with it. Bake with it. Splash it on your cereal or in your coffee. Hiland Dairy Lactose Free Milks gives you that mighty milk magic—without the lactose!</p>
					<br clear="all">
					<p>
						<?php foreach($data->lactose_free_milk as $lactose_free_milk): ?>
						<div class="xpander_blue">
							<a href="/products/lactose-free-milk/<?php echo strtolower(str_replace(' ', '-', $lactose_free_milk->tags)); ?>"><?php echo $lactose_free_milk->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end LACTOSE -->

			<!-- MILK -->
					<img src="img/product_milk.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Milk</h1>
					<p>Creamy, smooth and packed full of nine essential nutrients, Hiland Dairy Milk comes from local dairy farms and does not contain antibiotics or artificial growth hormones. Whether you pour it over cereal, mix it with your coffee or fill your child's sippy cup, Hiland Dairy Milk tastes good and is good for you.</p>
					<p>Grab a gallon or easy-to-pour quart of Hiland Dairy Milk for your fridge today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->milks as $milk): ?>
						<div class="xpander_blue">
							<a href="/products/milks/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $milk->tags))); ?>"><?php echo $milk->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end MILK -->
			
			<!-- MILK ALTERNATIVES -->
					<img src="img/product_milkalt.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Milk Alternatives</h1>
					<p>If you're unable to enjoy dairy milk due to an allergy or other reason, Hiland milk alternatives made from natural almonds or soybeans can give you the rich, silky experience you're looking for. So whether you want to add a splash to your morning coffee or cereal or are cooking up something delicious, vitamin- and protein-packed Hiland Almond and Soy Milks offer a tasty, nutritious alternative to dairy milk!</p>
					<br clear="all">
					<p>
						<?php foreach($data->milk_alternatives as $alternatives): ?>
						<div class="xpander_blue">
							<a href="/products/milk-alternatives/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $alternatives->tags))); ?>"><?php echo $alternatives->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end MILK ALTERNATIVES-->
			
			<!-- TEA -->
					<img src="img/product_tea.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Red Diamond Tea</h1>
					<p>Red Diamond Tea distributed by Hiland Dairy is a time-honored tradition among tea drinkers. When you want the best in sweetened or sugar-free iced tea, look for the Red Diamond!</p>
					<p>Grab a gallon or pint of Red Diamond Tea by Hiland Dairy for your next gathering today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->red_diamond_tea as $tea): ?>
						<div class="xpander_blue">
							<a href="/products/red-diamond-tea/<?php echo strtolower(str_replace(' ', '-', $tea->tags)); ?>"><?php echo $tea->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end TEA -->
			
			<!-- SCHOOL MILK -->
					<img src="img/product_school_milk.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">School Milk</h1>
					<p>Hiland Dairy School Milks are both nutritious and delicious. And it&rsquo;s no wonder both kids and their parents prefer the Hiland brand &ndash; our school milks contain NO high-fructose corn syrup, absolutely NO artificial growth hormones!</p>
					<br clear="all">
					<p>
						<?php foreach($data->school_milk as $school_milk): ?>
						<div class="xpander_blue">
							<a href="/products/school-milk/<?php echo strtolower(str_replace(' ', '-', $school_milk->tags)); ?>"><?php echo $school_milk->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end SCHOOL MILK -->

			
			<!-- SOUR CREAM -->
					<img src="img/product_sour_cream.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Sour Cream</h1>
					<p>Hiland Dairy Sour Cream adds a fresh and tangy zip to all your soups and side dishes. Whether you prefer regular or fat-free, you'll love the farm-fresh, wholesome taste of Hiland Dairy Sour Cream on your table.</p>
					<p>Bring home a container of award-winning Hiland Dairy Sour Cream today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->sour_cream as $sour_cream): ?>
						<div class="xpander_blue">
							<a href="/products/sour-cream/<?php echo strtolower(str_replace(' ', '-', $sour_cream->tags)); ?>"><?php echo $sour_cream->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			<!-- =end SOUR CREAM -->
			
			
			<!-- WATER 
					<img src="img/product_water.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Water</h1>
					<p>Essential to life, Hiland Dairy water is crisp, clear and pure. When you need essential refreshment, you can trust Hiland Dairy.</p>
					<p>Quench your thirst with Hiland Dairy water in one of our convenient sizes today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->water as $water): ?>
						<div class="xpander_blue">
							<a href="/products/water/<?php echo strtolower(str_replace(' ', '_', $water->tags)); ?>"><?php echo $water->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
					<div class="linelong"></div>
					<br clear="all">
			 =end WATER -->
			
			
			<!-- YOGURT -->
					<img src="img/product_yogurt.jpg" width="186" height="160" align="right" border="0" />
					<h1 class="fontface">Yogurt</h1>
					<p>Hiland Dairy cultured yogurt starts with fresh, wholesome skim milk. We add a special bacteria culture and irresistible flavors to create a powerhouse of calcium and protein that tastes good and promotes digestive health.</p>
					<p>Try Hiland Dairy Regular or Sugar-Free Yogurt today!</p>
					<br clear="all">
					<p>
						<?php foreach($data->yogurts as $yogurt): ?>
						<div class="xpander_blue">
							<a href="/products/yogurts/<?php echo strtolower(str_replace(' ', '-', $yogurt->tags)); ?>"><?php echo $yogurt->node_title; ?></a>
						</div>
						<?php endforeach; ?>
					</p>
					<br clear="all">
			<!-- =end YOGURT -->

				</div>
			</div>
		</div>
		
			<br clear="all">
	</div>
</div>