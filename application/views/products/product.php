<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="content-container">
<?php echo $products_side_nav; ?>
	<div id="content">
        <div id="content-columnone">
            <h1 class="fontface"><?php echo $product->title; ?></h1><img src="http://a.hilanddairy.envoydev.com/sites/default/files/<?php echo $product->field_image_upload->und[0]->filename; ?>" width="449" height="282" border="0">

            <?php if( isset($product->field_product_description->und[0]->value) ): ?>
            <?php echo $product->field_product_description->und[0]->value; ?>
            <?php endif; ?>
            
            <?php if( isset($product->field_product_ingredients->und[0]->value) ): ?>
            <p class="fineprint">Ingredients:  
            <?php echo $product->field_product_ingredients->und[0]->value; ?>
            <br>
            * Servings per container may vary based on the size of the container.</p>
            <?php endif; ?>
        </div>

        <div id="content-columntwo">
            <div id="column-two-socialbox">
            	<div class="pinterestpin">
	                <a href='javascript:void(run_pinmarklet1())'><img src="http://2.bp.blogspot.com/-lRae8bdMpuA/TzuLrnycXaI/AAAAAAAACQE/YVYUjfs7dm8/s1600/pinmask2.png" style='margin:0; padding:0; border:none;'></a> <script type='text/javascript'>
	function run_pinmarklet1() {
	                var e=document.createElement('script');
	                e.setAttribute('type','text/javascript');
	                e.setAttribute('charset','UTF-8');
	                e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
	                document.body.appendChild(e);
	                }
	                </script>
                </div>
                <div class="fblike">
                	<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-font="arial"></div>
                </div>
            </div>
            <?php if( isset($product->field_product_size->und[0]->tid) ): ?>
            <div id="column-two-sizebox">
                <img src="/img/hdr_size_availability.gif" width="208" height="18" alt="Available in these sizes"><br>
                <?php foreach($product->field_product_size->und as $size): ?>
                <?php $image_width = getimagesize('/home/hilandn/public_html/img/sizes/' . $size->tid . '.gif'); ?>
                <div class="sizeboxcontain"><img src="/img/sizes/<?php echo $size->tid ?>.gif" width="<?php echo $image_width[0]; ?>" height="69"></div>
                <?php endforeach; ?><br clear="all">
            </div>
            <? endif; ?>
        <?php echo $nutritional_facts; ?>
        </div>
    </div>

    <div id="content-columnall">
        <?php echo $additional_products; ?>
    </div><br clear="all">
