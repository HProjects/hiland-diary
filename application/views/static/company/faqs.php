<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	
	<div id="content">
<ul class="faq-list">

	<h2 style="margin-top:0;">Roberts Dairy is Changing Its Name to Hiland Dairy</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Why did Roberts change its name to Hiland?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">
			Roberts Dairy has been a division of Prairie Farms - Hiland Dairy since 1981. Hiland Dairy has been serving the Midwest for over 75 years, producing pure, fresh-from-the-farms wholesome milk and dairy products. <strong>Nothing has changed except for the name.</strong><br /><br /> The same Roberts employees and production facilities have been making the milk and dairy products you enjoy and trust. Our name change is a business decision that was made to allow us to save on product labeling and marketing costs and to create a strong, unified brand image. Hiland Dairy like Roberts Dairy is known for fresh dairy products made close to home, with no artificial growth hormones. The only thing changing is the name on the package; the product inside is the same fresh, wholesome goodness you and your family have trusted for generations.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Did Roberts Dairy go out of business?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. The decision was made to operate under one name for the entire seven-state distribution area. Because the labels and packaging are now ordered in bulk &ndash; those savings are passed along to our customers. The Roberts Dairy commitment to freshness and quality continues under the new name. Hiland Dairy Foods, like Roberts Dairy is proud of our long tradition that spans decades of bringing milk fresh from our local, farmer-owners to our consumers&rsquo; tables. The company’s products will remain the same high-quality dairy foods, with no artificial growth hormones.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Are Roberts Dairy&apos;s products changing?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. The product line will remain the same and will be sold under the Hiland Dairy label, which features the familiar <em>splash of fresh flavor</em> image that you've come to recognize with Roberts Dairy foods. In fact, Hiland Dairy has an expanded product line that gives our customers access to fresh, high quality, and wholesome products that they might not have had previously.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Does Hiland Dairy milk taste as good and fresh as Roberts Dairy milk?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Yes, we are the same local, farmer-owned dairy that&rsquo;s produced milk and dairy foods for your family for generations. Hiland Dairy milk is the same high quality, fresh &mdash; with no artificial growth hormones &mdash; that you enjoy and trust.</p>
		</div>
	</li>
	<!--<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Does Hiland Dairy milk contain antibiotics or artificial growth hormones?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Our milk does <strong>not</strong> contain antibiotics or artificial growth hormones. </p>
		</div>
	</li>-->
		<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Is Hiland Dairy milk local?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Yes. Our milk comes from our farmer-owners &mdash; with most of them located within 100 miles of each of our 11 plants in Arkansas, Kansas, Missouri, Nebraska and Oklahoma. <strong>Hiland milk is Roberts milk.</strong> It&apos;s the same fresh, local milk &mdash; as are all other Roberts dairy products &mdash; just sold under a different label. </p>
		</div>
	</li>
		<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Will any plants close or staff positions be eliminated, as a result of this name change?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. In fact, our brand consolidation with Hiland Dairy may allow us to hire more workers in the future. Our name change will not alter any aspect of how we run our business. It will not affect any of the existing staff, plant operations, or facilities.</p>
		</div>
	</li>
		<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> How will this affect consumer?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Our research indicates that 87&#37;	of Des Moines, Kansas City and Omaha shoppers will buy Hiland milk if it has the same quality and freshness as Roberts milk and is produced by local farmers. Some shoppers we&apos;ve talked with were under the impression that Hiland and Roberts Dairy were already the same company. </p>
		</div>
	</li>
	
	
	<h2>Coleman Dairy has Changed its Name to Hiland Dairy</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Why is Coleman Dairy changing its name to Hiland Dairy?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Coleman Dairy became a division of Hiland Dairy in 2006. Since then, nothing has changed except for the ownership of the Dairy. The same Coleman employees and production facilities have been making the milk and dairy products you enjoy and trust. We are changing our name now as a business decision, to save on product labeling costs and to create a strong, unified brand image but the milk will continue to be packaged in Little Rock, Arkansas. Coleman, Roberts and Hiland Dairies all stand for the same things &ndash; fresh flavor, dairy products made close to where our customers live, and milk that's free of artificial growth hormones.</p>
			<p class="answer">The only thing changing about Coleman Dairy is the name on the outside of the package; the product inside still embodies the same fresh, wholesome goodness you and your family have trusted for generations.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> When is Coleman Dairy changing its name?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Coleman Dairy will begin marketing its fresh, local, high quality milk and dairy products under the Hiland Dairy label, beginning in January 2013.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Is Coleman Dairy going out of business?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. Coleman Dairy became a division of Hiland Dairy in 2006. Recently, a business decision was made operate under the Hiland Dairy name to save on product labeling costs.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Will Coleman Dairy employees lose their jobs?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. In fact, our brand consolidation with Hiland Dairy may allow Coleman Dairy to hire more workers in the future. Our name change will not alter any aspect of how we run our business. It will not affect any of the existing staff, plant operations, or facilities.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Are Coleman Dairy milk and dairy products changing?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">The product line will remain exactly the same and will be sold under the Hiland Dairy label, which features the familiar milk splash image that you've come to recognize with Coleman Dairy foods. Hiland Dairy's expanded product line gives Coleman Dairy customers access to fresh, high quality, and wholesome products that they might not have had previously.</p>
		</div>
	</li>

	
	
	
	<h2>Our Milk Contains No Artificial Growth Hormones</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What is rbST?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Recombinant bovine growth hormone (also known as rbGH) is a genetically engineered drug. It is injected into dairy cows to induce them to increase milk production, typically by 5-15%. It is estimated that 15-20% of the cows in the United States are injected with this hormone. It was approved by the FDA in 1993.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Does Hiland Dairy use milk from cows treated with rbST?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No, effective February 1, 2008, Hiland Dairy will only use milk from dairy farmers who have pledged not to treat their cows with artificial growth hormones. While the FDA has not found a significant difference between milk from treated and untreated herds, some of our customers prefer their milk to come from untreated cows.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What is the FDA's stance on rbST?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Before the 1993 approval of rbST, the FDA determined that the recombinant, or genetically engineered form of bST, is virtually identical to a cow's natural somatotropin, a hormone produced in the pituitary gland that stimulates the production of milk. During that rbST approval process, the FDA concluded that there is no significant difference between milk from treated and untreated cows. For that reason, the FDA also concluded it does not have the authority to require special labeling for milk and dairy products from rbST-treated cows and that producers have no basis for claiming that milk from cows not treated with rbST is safer than milk from rbST-treated cows.</p>
		</div>
	</li>
	<!--<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Are antibiotics present in the milk supplied to Hiland Dairy?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">No. Hiland Dairy tests all milk for antibiotics prior to accepting it for delivery. Learn more <a href="http://www.dairymakessense.com/farming/animal-care/" target="_blank">here</a>.</p>
		</div>
	</li>-->
	
	
	
	
	<h2>Product Quality</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> How can I preserve the freshness, flavor and quality of my Hiland Dairy products?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">To preserve the quality of milk and other dairy foods, the following practices are recommended for consumers:</p>
			<ul class="answer">
				<li><strong>Containers.</strong> All Hiland Dairy products are packaged in proper containers to achieve maximum shelf life. It is not recommended that you serve or store the products in alternate containers. Proper containers protect milk from exposure to sunlight, bright daylight and strong fluorescent light. This will prevent the development of off-flavors and reductions in light-sensitive nutrients such as riboflavin, ascorbic acid and vitamin B6.</li>
				<li><strong>Storage of milk.</strong> Refrigerate milk at 40&deg;F or less (preferably below 35&deg;F) as soon as possible after purchase. (We recommend keeping a small Styrofoam cooler in your car during hot months to prevent spoilage of milk in the trunk.) Keep milk containers closed to prevent the absorption of other food flavors in the refrigerator. An absorbed flavor changes the taste but the milk is still safe. Storing milk in the door of the refrigerator can cause it to spoil before the expiration date as the door is not as cold as the rest of the refrigerator. Use milk in the order purchased. Return the milk container to the refrigerator immediately to prevent bacterial growth. Temperatures above 40&deg;F reduce the shelf life of milk and other milk products. Never return unused milk to the original container.</li>
				<li><strong>Storing cheese.</strong> Cheeses should be wrapped tightly in the original wrapping, other wrapping or tightly covered refrigerator containers for refrigerator storage. Unopened jars of cheese spreads and cheese foods can be stored at room temperature (70&deg;F) for about 3 months. However, once these products are opened, they should be tightly covered and refrigerated to prevent drying and an unattractive "oiling off." Grated cheeses in moisture-proof packages will keep for about 3 months at room temperature or about 12 months at a refrigerator temperature of 40&deg;F or below.</li>
				<li>Yogurt, buttermilk and acidophilus milk should be stored in closed containers in the refrigerator at 40&deg;F to maintain their quality. Yogurt will keep for about a week and buttermilk and acidophilus milk will keep for about 2 weeks in the refrigerator. Freezing is not recommended for any of these cultured dairy foods.</li>
				<li>Store ice cream tightly covered in the freezer at 0&deg;F. To avoid crystallization and volume loss, scoop ice cream, keeping the surface as level as possible. Cover the surface of ice cream with plastic wrap before re-closing and return to the freezer immediately.</li>
				<li>To preserve Hiland Dairy butter flavor and freshness, refrigerate opened butter in a covered dish in the butter compartment. Unopened, wrapped salted butter may be stored in the refrigerator for up to 2 months. Butter can be frozen in its original wrapper for several months. Unsalted butter is best kept frozen until ready to use. For longer freezer storage, wrap in foil or plastic. Unsalted butter can be kept frozen for about 5 months at 0&deg;F. Salted butter can be frozen for about 6 to 9 months.</li>
				<li>To store cream, keep it refrigerated in its closed container at 40&deg;F or lower. It should be used within one week. Ultra-pasteurized cream keeps several weeks longer, but once opened, it should be handled like pasteurized cream. Freezing is not recommended for unwhipped cream, but once whipped, cream may be frozen. Place dollops of whipped cream on waxed paper and freeze. When frozen, wrap individually for use as needed.</li>
			</ul>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What do the dates on the container mean?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">The date on milk and other dairy food containers indicates when the product was manufactured or meant to be sold by. It is used by the industry to indicate the age of individual packages and does not reflect the shelf life of the product. Generally, proper care ensures freshness for a few days beyond the "sell by" date. Regulation of open dating varies among states and municipalities.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Which of Hiland Dairy's products are gluten-free?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">For our customers with gluten intolerance or celiac disease, our gluten-free products are a healthy alternative. A large list of dairy products and beverages is available to choose from. View our <a href="/gluten-free-info">gluten free products</a>. Please note that our dairy foods and beverages not listed here may contain gluten.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Can I use my Hiland Dairy products after the date on the container?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">If properly refrigerated, milk generally stays fresh for several days after the "sell by" date.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Where does Hiland Dairy get its milk?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">You can count on us for the freshest, best-tasting products around. When you choose Hiland Dairy, you know you're getting hometown quality and freshness from a dairy farm near you. We're proud of our farmer-owners who operate dairy farms with care to ensure we deliver the purest, most wholesome products to your table. The best part is that most of Hiland's farmer-owners live within a 100-mile radius of our processing plants&ndash;giving true meaning to "fresh from our farms to your table."</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Can I freeze my Hiland Dairy products?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">With the exception of butter, ice cream, frozen yogurt, and other frozen dairy desserts, freezing of most dairy foods (e.g., milk, cream, yogurt, milk puddings, soft cheeses) is not recommended. Although natural hard cheeses, semi-hard cheeses and processed cheeses can be frozen, freezing affects their texture, causing them to become crumbly and mealy after thawing. However, they are suitable for cooking and/or for use in salads or salad dressing. If frozen, cheeses should be thawed slowly in the refrigerator for 24 hours. Cheese that has been frozen should be used as soon as possible after thawing.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Where can I buy my favorite Hiland Dairy products?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Look for Hiland Dairy products at your favorite stores in Arkansas, Iowa, Kansas, Missouri, Nebraska, Oklahoma and Texas. We're your hometown favorite!</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Are Hiland Dairy containers recyclable?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Yes. All Hiland Dairy paper and plastic containers are recyclable. Check with your local recycling facility to determine which products they may or may not accept for recycling.<a href="/our-mission">Visit our sustainability page.</a></p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Is it possible to receive Hiland Dairy products if I live outside of the distribution areas?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">To maintain the quality of our products, it is not currently possible for us to ship them outside of our distribution area.</p>
		</div>
	</li>
	
	
	
	
	<h2>Coupons/Donations/Fundraising</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Does Hiland Dairy donate products to special events?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Yes. At Hiland Dairy, we know the importance of being a good neighbor and community member. We actively support local and regional causes, events and organizations that seek to help make our communities better places to live. Although we can't accommodate everyone, every year Hiland donates a variety of healthy, fresh dairy products to several events throughout the region.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Where can I find coupons for my favorite Hiland Dairy products?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer"><a href="/coupons">Visit our coupons page.</a></p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> How can Hiland Dairy assist with my local fundraising efforts?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">At Hiland Dairy, we know the importance of being a good neighbor and community member. We actively support local and regional causes, events and organizations that seek to help make our communities better places to live. Hiland donates a variety of healthy, fresh dairy products to events throughout the region.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Can you send me coupons for your products?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Hiland Dairy is unable to honor requests for free products or coupons. Instead, we offer valuable coupons available online, <a href="/coupons">(see here)</a> and in Sunday newspaper inserts several times during the year.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> My retailer won't accept the Hiland Dairy online coupons?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Redemption of online coupons is at the sole discretion of each individual retailer. Hiland Dairy cannot guarantee online coupon redemption, or confirm participating stores; it is recommended that you verify redemption policies with your retailer(s) of choice before attempting to redeem any online coupons.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> I am having trouble printing the online coupons?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">We strive to provide only the best services for our loyal customers and we are pleased to offer you a variety of printable online coupons. However, due to the complexities associated with individual computer systems, printers and Internet browsers, we are unable to offer any technical support regarding issues with the printing of online coupons.</p>
			<p class="answer">A few general suggestions:</p>
			<ul class="answer">
				<li>Make sure you have created an account at hilanddairy.com/coupons and are logged in</li>
				<li>Make sure your printer is properly connected</li>
				<li>Make sure you disable any pop-up blockers</li>
				<li>Make sure you are set up to print at "best" quality</li>
			</ul>
			<p class="answer">If you are still experiencing trouble, please contact your Internet service provider and/or consult the owner's manual of your computer or printer for further assistance.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What if I forgot my username or password?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">If you are trying to access the Members Only Login Area of our website, but have forgotten your username or password, <a href="/coupons">Click Here</a> and enter your e-mail address. A new password will be <br />e-mailed to you shortly.</p>
		</div>
	</li>
	
	
	
	
	<h2>Business Inquiries</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Will Hiland Dairy make a delivery to my small business?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">To contact a sales representative in your area, please submit your request <a href="/company/contact-us">here</a>.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What should I do if my milk delivery did not come?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">To contact a sales representative in your area, please submit your request <a href="/company/contact-us">here</a>.</p>
		</div>
	</li>
	
	
	
	
	<h2>Employment</h2>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> Can I apply for employment online?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">Unfortunately, Hiland Dairy is not able to accept online employment applications at this time. For employment opportunities, you may contact the Hiland Dairy location of your choice or the local State Department of Employment Security. Applications are accepted only if there is a current job opening. Please do not contact Hiland Dairy via this website regarding employment opportunities.</p>
		</div>
	</li>
	<li class="faq-question">
		<h3 class="faq-toggler"><span class="letter-q">Q.</span> What positions is Hiland currently hiring for?</h3>
		<div>
			<p><span class="letter-a">A.</span></p>
			<p class="answer">For employment opportunities, you may contact the Hiland Dairy location of your choice or the local State Department of Employment Security. Applications are accepted only if there is a current job opening. Please do not contact Hiland Dairy via this website regarding employment opportunities.</p>
		</div>
	</li>
</ul>
</div>
</div>