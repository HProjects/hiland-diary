<div id="content-container">
	<div id="content" class="sign-up-form">
		
		<?php if($this->input->get('r') === 'complete'): ?>
			<script type="text/JavaScript">
			<!--
			setTimeout("location.href = '/coupons-print';",5000);
			-->
			</script>
			<p>Congratulations have successfully reset your password. If you are not redirected in 5 seconds to retrieve your coupons <a href="/coupons-print">click here</a>.
		<?php else: ?>
		<?php if($this->input->cookie('reset') !== '1'): ?>
		<form action="/account/reset_password_data" method="post">
			<ul class="formsignup">
				<fieldset class="login-info">
					<h2>Reset Password</h2>
					<li>
						<label for="email-address">Email Address</label><br />
						<input id="email-address" name="email-address" value="" type="email" placeholder="Email" required />
					</li>
					<li>
						<label for="password">Password</label><br />
						<input id="password" name="password" value="" type="password" placeholder="Password" required />
					</li>
					<li>
						<label for="confirm-password">Confirm Password</label><br />
						<input id="confirm_password" name="confirm_password" value="" type="password" placeholder="Confirm Password" required />
					</li>
				</fieldset>
				
				<fieldset class="newsletter-submit">
					<li>
						<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
					</li>
				</fieldset>
			</ul>
		</form>
		<?php else: ?>
			<p>You have already used this login to reset your password, please <a href="/account/forgot-password">go back</a> to send another email.
		<?php endif; ?>
		<?php endif; ?>
</div><!--end content-container-->