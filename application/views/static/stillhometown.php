<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_news_generic; ?>
	</div>
	<div id="content" class="stillhometown-wrapper">
		<h1 class="fontface"><img src="/img/logo_transition2.png" width="320" height="103" align="right">New Name<br />
Still Hometown
</h1>
<p>Roberts  Dairy has changed their name to Hiland Dairy, but they’re not changing anything else. </p>


<p>Roberts employees and production facilities will continue making the hometown fresh products you&rsquo;ve loved for more than 100 years. Still made right here in Omaha and Kansas City, so they get to your table faster and fresher! The only thing changing is the name on the label.</p>

<h2><img src="/img/land_hometown3.jpg" width="320" height="425" align="right">So, why did we change the name?</h2>

<p>You’ll notice that the new Hiland logo looks familiar. There’s a reason for that. Roberts Dairy has been a division of Prairie Farms &ndash; Hiland Dairy since 1981.
Together, these dairies have been committed to hometown fresh dairy products with no artificial growth hormones. Now, we want everyone to know that when they see the Hiland Dairy logo they are getting:</p>
		<ul>
			<li>The Hiland Splash of Fresh Flavor</li>
			<li>Dairy products that are made close to where they live</li>
			<li>Milk and dairy products with no artificial growth hormones.</li>
		</ul>
<p>PLUS, we’ll be working hard to bring you more new and delicious products from Hiland Dairy including shredded and block cheeses, milk alternatives, new ice cream and dip varieties and more.</p>

<h2>The Most Important Name to Us is Yours</h2>

<p>We are here for one reason: to give you and your family hometown fresh dairy products. So no matter what name is on our label, your name is still the most important one to us.</p>
		<ul>
			<li>If you have any additional questions about why Roberts Dairy is changing its name, please <a href="/company/faqs">click here</a>.</li>
			<li>To stay up to date on the latest Hiland recipes, coupons and news, please sign up to receive our eNewsletter.</li>
			<li>Want to read about what the news media is saying about our name change? <a href="/company/media-center">Click here to read the coverage of the name change</a>.</li>
		</ul>
	</div>
</div>