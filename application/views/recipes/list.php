<div id="content-container">
	<div id="content" class="recipes">
		<div id="content-recipe-contain">
		
		
		<!-- TOP ROTATOR STATIC -->
					<div class="prod-nav-left"><a href="#" id="btn_arrow-rec-left"><span>Left</span></a></div>
					<div id="content-recipes">
							<h1 class="fontface">Main Dishes</h1>
							<div class="content-recipe-highlight"><a href="/recipes/beef/meat-stuffed-manicotti"><img src="/img/recipes/rec_list_manicotti.jpg" width="191" height="110" border="0" />
							<p>Meat Stuffed Manicotti</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($manicotti_ratings) && $manicotti_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($manicotti_ratings) && $manicotti_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($manicotti_ratings) && $manicotti_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($manicotti_ratings) && $manicotti_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($manicotti_ratings) && $manicotti_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/beef/meat-stuffed-manicotti#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/soups/baked-potato-soup"><img src="/img/recipes/rec_list_potatosoup.jpg" width="191" height="110" border="0" />
							<p>Baked Potato Soup</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($baked_potato_ratings) && $baked_potato_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($baked_potato_ratings) && $baked_potato_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($baked_potato_ratings) && $baked_potato_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($baked_potato_ratings) && $baked_potato_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($baked_potato_ratings) && $baked_potato_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/soups/baked-potato-soup#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/main-courses/creamy-broccoli-lasagna"><img src="/img/recipes/rec_list_broclasagna.jpg" width="191" height="110" border="0" />
							<p>Creamy Broccoli Lasagna</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($cream_broccoli_ratings) && $cream_broccoli_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($cream_broccoli_ratings) && $cream_broccoli_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($cream_broccoli_ratings) && $cream_broccoli_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($cream_broccoli_ratings) && $cream_broccoli_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($cream_broccoli_ratings) && $cream_broccoli_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/main-courses/creamy-broccoli-lasagna#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form>
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/salads/mexican-salad"><img src="/img/recipes/rec_list_mexicansalad.jpg" width="191" height="110" border="0" />
							<p>Mexican Salad</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mexican_salad_ratings) && $mexican_salad_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mexican_salad_ratings) && $mexican_salad_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mexican_salad_ratings) && $mexican_salad_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mexican_salad_ratings) && $mexican_salad_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mexican_salad_ratings) && $mexican_salad_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/salads/mexican-salad#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/breakfast/country-breakfast-pie"><img src="/img/recipes/rec_list_breakfastpie.jpg" width="191" height="110" border="0" />
							<p>Country Breakfast Pie</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" class="auto-submit-star"<?php echo (isset($country_breakfast_ratings) && $country_breakfast_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($country_breakfast_ratings) && $country_breakfast_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($country_breakfast_ratings) && $country_breakfast_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($country_breakfast_ratings) && $country_breakfast_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($country_breakfast_ratings) && $country_breakfast_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/breakfast/country-breakfast-pie#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/main-courses/spicy-sweet-pepper-fettuccine"><img src="/img/recipes/rec_list_fettecini.jpg" width="191" height="110" border="0" />
							<p>Spicy Sweet Pepper Fettuccine</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($fettuccine_ratings) && $fettuccine_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($fettuccine_ratings) && $fettuccine_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($fettuccine_ratings) && $fettuccine_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($fettuccine_ratings) && $fettuccine_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($fettuccine_ratings) && $fettuccine_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/main-courses/spicy-sweet-pepper-fettuccine#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/main-courses/beef-kabobs"><img src="/img/recipes/rec_list_kabobs.jpg" width="191" height="110" border="0" />
							<p>Beef Kabobs</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($beef_kabobs_ratings) && $beef_kabobs_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($beef_kabobs_ratings) && $beef_kabobs_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($beef_kabobs_ratings) && $beef_kabobs_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($beef_kabobs_ratings) && $beef_kabobs_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($beef_kabobs_ratings) && $beef_kabobs_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/main-courses/beef-kabobs#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
							<div class="content-recipe-highlight"><a href="/recipes/main-courses/macaroni-and-cheese"><img src="/img/recipes/rec_list_macncheese.jpg" width="191" height="110" border="0" />
							<p>Macaroni and Cheese</a></p>
							<form id="star_rating" style="margin-top: -10px;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mac_cheese_ratings) && $mac_cheese_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mac_cheese_ratings) && $mac_cheese_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mac_cheese_ratings) && $mac_cheese_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mac_cheese_ratings) && $mac_cheese_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($mac_cheese_ratings) && $mac_cheese_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/main-courses/macaroni-and-cheese#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
							</div>
					</div>
					<div class="prod-nav-right"><a href="#" id="btn_arrow-rec-right"><span>Right</span></a></div>
					<br clear="all"><div class="lineextralong"></div><br clear="all">
		<!-- =end TOP ROTATOR STATIC -->
		
		
		<!-- COLUMN LEFT -->
			<div id="recipe-col-left">
				<div id="recipeofthemonth">
					<h2>New Red Potato Salad</h2>
					<div id="recipe-month-box">
						<div id="recipe-print-box-left" style="position:relative;">
							<form id="star_rating" style="margin: 0;">
							<input name="recipe-id" id="recipe-id" type="hidden" value="<?php echo $recipe->nid; ?>"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($new_red_potato_salad_ratings) && $new_red_potato_salad_ratings == '1') ? ' checked="checked"' : ''; ?> value="1"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($new_red_potato_salad_ratings) && $new_red_potato_salad_ratings == '2') ? ' checked="checked"' : ''; ?> value="2"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($new_red_potato_salad_ratings) && $new_red_potato_salad_ratings == '3') ? ' checked="checked"' : ''; ?> value="3"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($new_red_potato_salad_ratings) && $new_red_potato_salad_ratings == '4') ? ' checked="checked"' : ''; ?> value="4"/>
							<input name="star1" type="radio" disabled="disabled" class="auto-submit-star"<?php echo (isset($new_red_potato_salad_ratings) && $new_red_potato_salad_ratings == '5') ? ' checked="checked"' : ''; ?> value="5"/> | 
							<a href="/recipes/salads/new-red-potato-salad#disqus_thread" data-disqus-identifier="<?php echo str_replace(' ', '_', strtolower($recipe->title)); ?>">Read Reviews</a>
							</form> 
						</div>
					</div>
					<a href="/recipes/salads/new-red-potato-salad"><img src="/img/recipes/rotm_potatosalad.jpg" width="273" height="171" border="0" /></a>
					<p>Be the hit of any picnic with this easy and delicious potato salad!</p>
					<a href="/recipes/salads/new-red-potato-salad" id="btn_makethis"><span>Make This</span></a><a href="mailto:?subject=Recipe%20Ingredients&body=<?php echo 'http://' . $_SERVER['SERVER_NAME'] . '/recipes/salads/new-red-potato-salad'; ?>" id="btn_email2"><span>Email</span></a><br clear="all">
					<a href='javascript:void(run_pinmarklet1())'><img src="http://2.bp.blogspot.com/-lRae8bdMpuA/TzuLrnycXaI/AAAAAAAACQE/YVYUjfs7dm8/s1600/pinmask2.png" style='margin:0; padding:0; border:none;'/></a>
				<script type='text/javascript'>
					function run_pinmarklet1() {
						var e=document.createElement('script');
						e.setAttribute('type','text/javascript');
						e.setAttribute('charset','UTF-8');
						e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
						document.body.appendChild(e);
					}
				</script>
				</div>
			</div>
		<!-- =end COLUMN LEFT -->
		
		
		<!-- COLUMN RIGHT -->
			<div id="recipe-col-right">
				<div class="recheader_extralong">
					<h2>Recipe Types</h2>
				</div>
				<p><a href="/recipes/categories/budget-friendly">Budget-Friendly</a>
					<br />Planning dinner on a dime? Hiland Dairy has you covered with these <a href="/recipes/categories/budget-friendly">budget-friendly recipes</a>.</p>
				<hr style="margin:20px 0px 20px 20px;" />
					
				<p><a href="/recipes/categories/appetizers">Appetizers</a>
					<br />Leave your guests wanting more with these amazing <a href="/recipes/categories/appetizers">appetizer recipes</a> from Hiland Dairy.</p>
				<hr style="margin:20px 0px 20px 20px;" />
					
				<p><a href="/recipes/categories/main-courses">Main Courses</a>
					<br />Satisfy your crowd with one of these mouthwatering <a href="/recipes/categories/main-courses">main dish recipes</a> from Hiland Dairy.</p>
				<hr style="margin:20px 0px 20px 20px;" />
					
				<p><a href="/recipes/categories/side-dishes">Side Dishes</a>
					<br />Plan perfectly paired <a href="/recipes/categories/side-dishes">side dishes</a> with these recipes from Hiland Dairy.</p>
				<hr style="margin:20px 0px 20px 20px;" />
				
				<p><a href="/recipes/categories/desserts">Desserts</a>
					<br />Indulge your cravings with these decadent <a href="/recipes/categories/desserts">dessert recipes</a> from Hiland Dairy.</p>
				
				<div class="recheader_extralong">
					<h2>Recipe Categories</h2>
				</div>
				<div id="rec-cat-container">
					<ul>
						<?php foreach($recipes_categories as $category_key => $category_value):?>
							<li class="xpander_blue">
								<a href="/recipes/categories/<?php echo strtolower(str_replace(array(' ', '-'), '-', $category_key)); ?>">
									<?php echo strtoupper($category_key);?> (<?php echo $category_value;?>)</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		<!-- =end COLUMN RIGHT -->
		
		</div><!-- =end content-recipe-contain -->
	</div><!-- =end content -->
	<script src="/js/jquery-cycle.js" type="text/javascript"></script>
	<script>
		//-------------------------------------------------
		// RECIPES ROTATOR
		//-------------------------------------------------
			$('.recipes-rotator .recipes-rotator-wrapper').cycle({
				fx: 'scrollHorz',
				speed: 300,
				timeout: 0,
				fit: true,
				prev: '.prev-rec-cat-button',
				next: '.next-rec-cat-button'
			});
	</script>
</div><!-- =end content-container -->