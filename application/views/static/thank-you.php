<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_news_generic; ?>
	</div>
	<div id="content" class="stillhometown-wrapper">
		
		<h1 class="fontface">Thank you!</h1>
		<p>Thank you for entering in our Family Fun Getaway contest! <br />
		We wish you best of luck!</p>
		
		<a href="/">&laquo; Return to Home Page</a>
		
	</div>
</div>