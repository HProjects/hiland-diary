<div id="content-container">
	<div id="content" class="sign-in-form">
		<form action="/account/login" method="post">
			<ul class="formsignup">
			<li>
				<fieldset class="login-info">
					<h2>Log In</h2>
					<ul>
						<?php if($this->input->get('e') === 'true'): ?>
						<li>
							<div style="color:red;">There was an error with your E-Mail/Password combination. Please try again.</div>
						</li>
						<?php endif; ?>
						<li>
							<label for="email-address">Email Address</label><br />
							<input name="email-address" type="email" id="email-address" value="" required placeholder="Email" />
						</li>
						<li>
							<label for="password">Password</label><br />
							<input name="password" id="password" value="" type="password" required placeholder="Password" />
						</li>
						<li class="submit">
							<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
						</li>
						<li class="forgot-pass">
							<p><a href="/account/forgot-password">Forgot your password</a></p>
						</li>
					</ul>
				</fieldset>
				<fieldset>
					<h2>Not a Member</h2>
					<p>Sign up for our Coupons and eNewsletter and you will be notified of coupons available for printout that coincide with periodic mailings or special offers.</p>
					<p class="sign-up"><a href="/account/sign-up" class="btn-coupon-sign-in" href="/coupons"><span class="hide">Sign In</span></a></p>
				</fieldset>
			</li>
			<!--
<li>	
				<fieldset class="login-info">
					<legend>Step 2</legend>
					<h2>Login Information</h2>
					<ul>
						<li>
							<label for="email-address">Email Address</label><br />
							<input id="email-address" name="email-address" value="" type="text" placeholder="Email" />
						</li>
						<li>
							<label for="password">Password</label><br />
							<input id="password" name="password" value="" type="password" placeholder="Password" />
						</li>
						<li>
							<label for="confirm-password">Confirm Password</label><br />
							<input id="confirm-password" name="confirm-password" value="" type="password" placeholder="Confirm Password" />
						</li>
					</ul>
				</fieldset>
			</li>
			<li>	
				<fieldset class="newsletter-submit">
					<ul>
						<li>
							<label for="newsletter">Newsletter</label>
							<input id="newsletter" name="newsletter" value="1" type="checkbox" checked="checked" />
							<p>Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy. Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy.</p>
						</li>
						<li>
							<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
						</li>
					</ul>
				</fieldset>
			</li>
-->
			</ul>
		</form>
</div><!--end content-container-->