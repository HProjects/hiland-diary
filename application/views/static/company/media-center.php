<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	
	<div id="content">
<!-- MEDIA CENTER -->
		<h1 class="fontface">Media Center</h1><br />
		<img src="/img/divider.gif">
		
		<div class="coleman-dairy">
			<h3>Hiland for PINK July 1 - September 30, 2013</h3>
			<p>Press Release:<br />
				<a href="/pdfs/media/HilandForPink-PR-6.24.13.pdf" style="color:#32637e; font-weight:bold;" target="_blank">Hiland for PINK Press Release</a></p>
		</div>
		<div>
			<a href="/pink"><img src="/img/media/HilandPINKforBCA.jpg"></a>
		</div>
		
<br clear="all">

<img src="/img/divider.gif">
		
		<div class="coleman-dairy">
			<h3>Hiland Dairy Celebrates Family Fun!</h3>
			<p>Consumers have a chance to win a trip for four to Orlando's famous theme parks.</p>
			<p>Press Release:<br />
				<a href="/pdfs/media/Hiland_FloridaFamilyFun_PR.pdf" style="color:#32637e; font-weight:bold;" target="_blank">Florida Family Fun Giveaway!</a></p>
		</div>
		<div>
			<a href="/familyfun"><img src="/img/media/FloridaFamilyFun_image.jpg"></a>
		</div>
		
<br clear="all">

	<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Dairy Industry Economic Impact</h3>
			<p>Hiland Dairy Foods is proud to be a part of the dairy manufacturing industry that is helping our nation's economy grow.  Did you know that a total of 23,297 jobs were supported through the dairy manufacturing industry in Missouri, and those jobs provided $1.2 billion in labor income to Missourians in 2011? Also, Missouri's gross domestic product (GDP) increased by $2 billion due to the value added by the dairy manufacturing industry in 2011. Hiland continues to be one of the Midwest's leading dairy manufacturers, and we take pride in our farmer-owned, locally-produced philosophy that is sustaining employment and contributing to the economy in the Midwest.</p>
			<p>Want more facts on how the dairy industry is positively impacting the state of Missouri's economy? Read the full report on the <a href="/pdfs/media/DairyMfgEconImpact.pdf" style="color:#32637e; text-decoration:underline; font-weight:bold;" target="_blank">Economic Contribution of the Missouri Dairy Product Manufacturing Industry</a> provided by the University of Missouri's Commercial Agriculture Program.</p>
		</div>
		
<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Hiland Dairy and the Little Rock Zoo Announce Dollar Day</h3>
			<p>Press Release:<br />
			<a href="/pdfs/media/dollarday.pdf" target="_blank">Dollar Day Press Release PDF</a></p>
		</div>
		
		<div class="thumbnails-contain">
			<div class="thumbnails-media">
				<a href="/img/media/dollarday1.jpg"><img src="/img/media/dollarday1_thumb.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/dollarday1.jpg">Download JPEG</a></p>
			</div>
			<div class="thumbnails-media">
				<a href="/img/media/dollarday2.jpg"><img src="/img/media/dollarday2_thumb.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/dollarday2.jpg">Download JPEG</a></p>
			</div>
			<div class="thumbnails-media">
				<a href="/img/media/dollarday3.jpg"><img src="/img/media/dollarday3_thumb.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/dollarday3.jpg">Download JPEG</a></p>
			</div>
			<div class="thumbnails-media">
				<a href="/img/media/dollarday4.jpg"><img src="/img/media/dollarday4_thumb.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/dollarday4.jpg">Download JPEG</a></p>
			</div>
		</div>
		
<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Hiland Introduces Lactose Free Milks:</h3>
			<p>Press Release:<br />
			<a href="/pdfs/media/HilandDairy_LactoseFreeMilks_PressRelease_6-5-13.pdf" target="_blank">Lactose Free Milks Press Release PDF</a></p>
		</div>
		
		<div class="thumbnails-contain">
			<div class="thumbnails-media">
				<a href="/img/media/HilandDairyLactoseFreeMilks.zip"><img src="/img/media/lactosefree_thumbnails.jpg" width="190" height="70" border="0" /></a>
				<p><a href="/img/media/HilandDairyLactoseFreeMilks.zip">Download JPEG collection</a></p>
			</div>
		</div>
		
<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Hiland Introduces Iced Coffees:</h3>
			<p>Press Release:<br />
			<a href="/pdfs/media/HilandDairy_IcedCoffee_PressRelease_6-5-13.pdf" target="_blank">Iced Coffee Press Release PDF</a></p>
		</div>
		
		<div class="thumbnails-contain">
			<div class="thumbnails-media">
				<a href="/img/media/HilandDairyIcedCoffees.zip"><img src="/img/media/icedcoffee_thumbnail.jpg" width="126" height="70" border="0" /></a>
				<p><a href="/img/media/HilandDairyIcedCoffees.zip">Download JPEG collection</a></p>
			</div>
		</div>

<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Roberts Dairy Name Change:</h3>
			<p><strong><a href="/company/about-us" target="_blank"> About Hiland Dairy Foods</a></strong></p>
			
			<p>Press Releases:<br />
			<a href="/pdfs/media/Roberts_Dairy_Name_Change-PressRelease.pdf" target="_blank">Read Roberts Renaming Press Release PDF</a></p>
			
			<p>Facts Sheets:<br />
			<a href="/pdfs/media/RobertsNameChange-TalkingPoints.pdf" target="_blank">Read Fact Sheet PDF</a></p>
			
			<p><strong><a href="/company/faqs" target="_blank">FAQs Section</a></p>
			
			<p><strong>Download Logo Files and Product Group Photo Here</strong></p>
		</div>
		
		<div class="thumbnails-contain">
			<div class="thumbnails-media">
				<a href="/pdfs/media/HilandDairyFoods_logo.pdf"><img src="/img/media/HilandDairy100-100.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/pdfs/media/HilandDairyFoods_logo.pdf">Download PDF</a></p>
			</div>

			<div class="thumbnails-media">
				<a href="/img/media/HilandDairyFoods.eps"><img src="/img/media/HilandDairy100-100.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/HilandDairyFoods.eps">Download EPS</a></p>
			</div>

			<div class="thumbnails-media">
				<a href="/img/media/HilandDairyProductsGroup.jpg"><img src="/img/media/products-group100-100.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/HilandDairyProductsGroup.jpg">Download JPEG</a></p>
			</div>
			
			<div class="thumbnails-media">
				<a href="/img/media/HilandMilkwithRobertslabel.jpeg"><img src="/img/media/newLabel-thumb.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/HilandMilkwithRobertslabel.jpeg">Download JPEG</a></p>
			</div>
		</div>
		
<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Hiland Ice Cream Outlaw Run:</h3>
			<p>Press Release:<br />
			<a href="/pdfs/media/OutlawRunPressRelease.pdf" target="_blank">Outlaw Run Press Release PDF</a></p>
		</div>
		
		<div class="thumbnails-contain">
			<div class="thumbnails-media">
				<a href="/img/media/HilandOutlawRunturned.jpg"><img src="/img/media/HilandOutlawRunturned_sm.jpg" width="70" height="70" border="0" /></a>
				<p><a href="/img/media/HilandOutlawRunturned.jpg">Download JPEG</a></p>
			</div>
		</div>

<br clear="all">

		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Hiland Dairy Wins 2013 Weber Award:</h3>
			<p>Press Release:<br />
			<a href="/pdfs/media/2013_Weber_Award_5.2.13.pdf" target="_blank">Read Weber Award Press Release PDF</a></p>
		</div>
		
		<div class="thumbnails-media">
			<a href="/img/media/HilandWeberWinner2013web.jpg"><img src="/img/media/weber2013_thumb.jpg" width="70" height="70" border="0" /></a>
			<p><a href="/img/media/HilandWeberWinner2013web.jpg">Download JPEG</a></p>
		</div>

<br clear="all">

		<img src="/img/divider.gif">
		<h3>View our YouTube Channel</h3>
		<p><a href="http://www.youtube.com/hilanddairycompany" target="_blank">YouTube.com/HilandDairyCompany</a>.</p>
		
		<img src="/img/divider.gif">
		<h3>Hiland Dairy Foods and Hiland Ice Cream Company Finalist for Total Quality Performance Award:</h3>
		<p><a href="/pdfs/media/2013_Weber_Finalist_Hiland_and_Hiland_Ice_CreamF.pdf" target="_blank">Read Press Release</a></p>
		
		<img src="/img/divider.gif">
		<h3>Hiland Dairy kicks off Spring with the Little Rock Zoo:</h3>
		<p><a href="/pdfs/Zoo_Kicks-off_Spring_With_Hiland_Dairy.pdf" target="_blank">Read Press Release</a></p>
		
		<img src="/img/divider.gif">
		<div class="coleman-dairy">
			<h3>Coleman Dairy name change:</h3>
			<p>
				<a href="http://www.katv.com/video?clipId=7953409&flvUri=&partnerclipid=&topVideoCatNo=0&c=&autoStart=true&activePane=info&LaunchPageAdTag=homepage&clipFormat=flv" target="_blank">Watch for more information</a>
				<br />
				Read more about Coleman Dairy's name change in this attached pdf &ndash; <a href="/pdfs/media/Coleman_Renaming_press_release_F.pdf" target="_blank">Read Press Release</a>
				<br />
				Read more &ndash; <a href="http://www.arkansasonline.com/news/2013/jan/04/coleman-dairy-shift-hiland-brand-begin-month/?latest" target="_blank">arkansasonline.com</a>
				<br />
				Read more &ndash; <a href="http://www.theshelbyreport.com/2013/01/04/coleman-markets-fresh-dairy-products-under-hiland-dairy-foods-name/#.UOcFI4njkVd" target="_blank">theshelbyreport.com</a>
			</p>
		</div>
<!-- =end MEDIA CENTER -->

	</div>
</div>