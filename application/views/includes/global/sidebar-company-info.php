<div id="coupon-block">
	<div id="staticleft" class="company-nav">
		
		<ul class="company-info">
			<li<?php echo ($school_milk_program == 'on') ? ' class="on"' : ''; ?>><a href="/company/school-milk-program">
				<h2>School Milks</h2>
				<p>Get the facts!</p>
			</a></li>
			<li<?php echo ($food_service == 'on') ? ' class="on"' : ''; ?>>
				<a href="/company/food-service">
					<h2>Food Services</h2>
					<p>Serve your customers!</p>
				</a>
				<ul>
					<li><a href="/grocers">Grocers/Retailers</a></li>
					<li><a href="/restaurants">Restaurants</a></li>
				</ul>
			</li>
			<li<?php echo ($our_mission == 'on') ? ' class="on"' : ''; ?>><a href="/our-mission">
				<h2>Our Mission</h2>
				<p>Learn about our sustainability!</p>
			</a></li>
			<li<?php echo ($media_center == 'on') ? ' class="on"' : ''; ?>><a href="/company/media-center">
				<h2>Media Center</h2>
				<p>What's in the news?</p>
			</a></li>
			<li<?php echo ($contact_us == 'on') ? ' class="on"' : ''; ?>><a href="/company/contact-us" style="border:0;">
				<h2>Contact Us</h2>
				<p>Have questions or comments?</p>
			</a></li>
		</ul>
		
	</div>
</div>