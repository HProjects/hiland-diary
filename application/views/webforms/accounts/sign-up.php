<div id="content-container">
	<div id="content" class="sign-up-form">
	
		<?php if($this->input->get('e') === 'true'): ?>
		<p style="width: 60%; margin: 15px auto;">There is no account match with your E-Mail. Please sign up to start receiving Hiland Dairy Coupons.</p>
		<?php endif; ?>
		<p class="required-fields">All fields are required.</p>
		<form action="/account/sign_up_submit" method="post">
			<ul class="formsignup">
				<fieldset class="personal-info">
					<legend>Step 1</legend>
					<h2>Personal Information</h2>
					<li>
						<label for="first-name">First Name</label><br />
						<input id="first-name" name="first-name" value="" type="text" required placeholder="First Name" />
					</li>
					<li>
						<label for="last-name">Last Name</label><br />
						<input id="last-name" name="last-name" value="" type="text" required placeholder="Last Name" />
					</li>
					<li>
						<label for="zip-code">Zip Code</label><br />
						<input id="zip-code" name="zip-code" value="" type="text" required placeholder="Zip Code" />
					</li>
				</fieldset>
				
				<fieldset class="login-info">
					<legend>Step 2</legend>
					<h2>Login Information</h2>
					<li>
						<label for="email-address">Email Address</label><br />
						<input id="email-address" name="email-address" value="" type="email" required placeholder="Email" />
					</li>
					<li>
						<label for="password">Password</label><br />
						<input id="password" name="password" value="" type="password" required placeholder="Password" />
					</li>
					<li>
						<label for="confirm_password">Confirm Password</label><br />
						<input id="confirm_password" name="confirm_password" value="" type="password" required placeholder="Confirm Password" />
					</li>
				</fieldset>
				
				<fieldset class="newsletter-submit">
					<li>
						<label for="newsletter">Newsletter</label>
						<input id="newsletter" name="newsletter" value="1" type="checkbox" checked="checked" />
						<p>Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy. Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy.</p>
					</li>
					<li>
						<button id="submit" value="Submit" type="submit"><span class="hidden">Submit</span></button>
					</li>
				</fieldset>
			</ul>
		</form>
		
		<!--<form action="/account/sign_up_submit" method="post">
			<ul class="formsignup">
				<li>
					<label for="email-address">Email Address</label><br />
					<input id="email-address" name="email-address" value="" type="text" />
				</li>
				<li>
					<label for="password">Password</label><br />
					<input id="password" name="password" value="" type="text" />
				</li>
				<li>
					<label for="first-name">First Name</label><br />
					<input id="first-name" name="first-name" value="" type="text" />
				</li>
				<li>
					<label for="last-name">Last Name</label><br />
					<input id="last-name" name="last-name" value="" type="text" />
				</li>
				<li>
					<label for="address1">Address</label><br />
					<input id="address1" name="address1" value="" type="text" />
				</li>
				<li>
					<label for="address2">Address (Apt/Unit/Other)</label><br />
					<input id="address2" name="address2" value="" type="text" />
				</li>
				<li>
					<label for="city">City</label><br />
					<input id="city" name="city" value="" type="text" />
				</li>
				<li>
					<label for="state">State</label><br />
					<input id="state" name="state" value="" type="text" />
				</li>
				<li>
					<label for="zip-code">Zip Code</label><br />
					<input id="zip-code" name="zip-code" value="" type="text" />
				</li>
				<li>
					<label for="phone-number">Phone Number</label><br />
					<input id="phone-number" name="phone-number" value="" type="text" />
				</li>
				<li>
					<label for="newsletter">Newsletter</label><br />
					<input id="newsletter" name="newsletter" value="1" type="checkbox" checked="checked" />
					<div>Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy.
Sign up to receive e-mail newsletters with special offers, recipes, news and information about Hiland Dairy.</div>
				</li>
				<li>
					<input id="submit" name="newsletter" value="Submit" type="submit" />
					</li>
			<ul>
		</form>-->
</div><!--end content-container-->