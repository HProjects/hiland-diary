<div class="content-container" >
<ul data-role="listview">
<!-- PRODUCT LIST -->

	<!-- BUTTER -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-butter.png" alt="" />
				<h3>BUTTER</h3>
				<ul>
			    <?php foreach($data->butters as $butter): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $butter->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/butters/<?php echo strtolower(str_replace(' ', '-', $butter->tags)); ?>/">
							<?php echo $butter->node_title; ?>
						</a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end BUTTER -->
	
	
	<!-- BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-half-and-half.png" alt="" />
				<h3>BUTTERMILK / HALF AND HALF / WHIPPING CREAM</h3>
				<ul>
			    <?php foreach($data->creams_buttermilks as $buttermilk): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $buttermilk->tags)))) ? ' class="on"' : ''; ?>>
						<a href="/products/creams-buttermilks/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $buttermilk->tags))); ?>/"><?php echo $buttermilk->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end BUTTERMILK / HALF AND HALF / WHIPPING CREAM -->
	
	
	<!-- CHEESE -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-cheese.png" alt="" />
				<h3>CHEESE</h3>
				<ul>
			    <?php foreach($data->cheeses as $cheese): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $cheese->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/cheeses/<?php echo strtolower(str_replace(' ', '-', $cheese->tags)); ?>/"><?php echo $cheese->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end CHEESE -->
	
	
	<!-- COTTAGE CHEESE -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-cottage-cheese.png" alt="" />
				<h3>COTTAGE CHEESE</h3>
				<ul>
			    <?php foreach($data->cottage_cheese as $cottage_cheese): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $cottage_cheese->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/cottage-cheese/<?php echo strtolower(str_replace(' ', '-', $cottage_cheese->tags)); ?>/"><?php echo $cottage_cheese->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end COTTAGE CHEESE -->
	
	
	<!-- DIPS -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-dip.png" alt="" />
				<h3>DIPS</h3>
				<ul>
			    <?php foreach($data->dips as $dip): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $dip->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/dips/<?php echo strtolower(str_replace(' ', '-', $dip->tags)); ?>/"><?php echo $dip->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end DIPS -->
	
	
	<!-- EGG NOG 
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-egg-nog.png" alt="" />
				<h3>EGG NOG</h3>
				<ul>
			    <?php foreach($data->eggnog as $eggnog): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $eggnog->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/eggnog/<?php echo strtolower(str_replace(' ', '-', $eggnog->tags)); ?>"><?php echo $eggnog->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	 =end EGG NOG -->
	
	
	<!-- EGG SUBSTITUTE 
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-egg-substitute.png" alt="" />
				<h3>EGG SUBSTITUTE</h3>
				<ul>
			    <?php foreach($data->egg_substitute as $egg_substitute): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $egg_substitute->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/egg-substitute/<?php echo strtolower(str_replace(' ', '-', $egg_substitute->tags)); ?>"><?php echo $egg_substitute->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	 =end EGG SUBSTITUTE -->
	
	
	<!-- FRUIT DRINKS 
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-fruit-drink.png" alt="" />
				<h3>FRUIT DRINKS</h3>
				<ul>
			    <?php foreach($data->fruit_drinks as $fruit_drink): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $fruit_drink->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/fruit-drinks/<?php echo strtolower(str_replace(' ', '-', $fruit_drink->tags)); ?>"><?php echo $fruit_drink->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	 =end FRUIT DRINKS -->
	
	
	<!-- JUICE -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-orange-juice.png" alt="" />
				<h3>JUICE</h3>
				<ul>
			    <?php foreach($data->juice as $juice): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $juice->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/juice/<?php echo strtolower(str_replace(' ', '-', $juice->tags)); ?>/"><?php echo $juice->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end JUICE -->
	
	
	<!-- MILK -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-milk.png" alt="" />
				<h3>MILK</h3>
				<ul>
			    <?php foreach($data->milks as $milk): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $milk->tags)))) ? ' class="on"' : ''; ?>><a href="/products/milks/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $milk->tags))); ?>/"><?php echo $milk->node_title; ?></a></li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end MILK -->
	
	
	<!-- MILK ALTERNATIVES -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-milk-alternative.png" alt="" />
				<h3>MILK ALTERNATIVES</h3>
				<ul>
			    <?php foreach($data->milk_alternatives as $alternatives): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', str_replace('%', '-percent', $alternatives->tags)))) ? ' class="on"' : ''; ?>><a href="/products/milk-alternatives/<?php echo strtolower(str_replace(' ', '-', str_replace('%', '-percent', $alternatives->tags))); ?>/"><?php echo $alternatives->node_title; ?></a></li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end MILK ALTERNATIVES -->
	
	
	<!-- RED DIAMOND TEA -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-tea.png" alt="" />
				<h3>RED DIAMOND TEA</h3>
				<ul>
			    <?php foreach($data->red_diamond_tea as $tea): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $tea->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/red-diamond-tea/<?php echo strtolower(str_replace(' ', '-', $tea->tags)); ?>/"><?php echo $tea->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end RED DIAMOND TEA -->
	
	
	<!-- SOUR CREAM -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-sour-cream.png" alt="" />
				<h3>SOUR CREAM</h3>
				<ul>
			    <?php foreach($data->sour_cream as $sour_cream): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $sour_cream->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/sour-cream/<?php echo strtolower(str_replace(' ', '-', $sour_cream->tags)); ?>/"><?php echo $sour_cream->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end SOUR CREAM -->
	
	
	<!-- WATER 
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-water.png" alt="" />
				<h3>WATER</h3>
				<ul>
			    <?php foreach($data->water as $water): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '_', $water->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/water/<?php echo strtolower(str_replace(' ', '_', $water->tags)); ?>"><?php echo $water->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	 =end WATER -->
	
	
	<!-- YOGURT -->
			<li>
				<img class="ui-li-thumb" src="/img/mobile-products/m-prod-yogurt.png" alt="" />
				<h3>YOGURT</h3>
				<ul>
			    <?php foreach($data->yogurts as $yogurt): ?>
					<li<?php echo ($this->uri->segment(3) == strtolower(str_replace(' ', '-', $yogurt->tags))) ? ' class="on"' : ''; ?>>
						<a href="/products/yogurts/<?php echo strtolower(str_replace(' ', '-', $yogurt->tags)); ?>/"><?php echo $yogurt->node_title; ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</li>
	<!-- =end YOGURT -->
		
			</ul>
<!-- =end PRODUCT LIST -->
		</li>
	</ul>
</div>