<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="content">
		<h1 class="fontface">Terms & Conditions</h1>
		<h2>Your Acceptance of These Terms And Conditions</h2>
<p>Please read these Terms and Conditions carefully. By making use of this website, you agree to be bound by the Terms and Conditions set forth below. If you do not wish to be bound by these Terms and Conditions, do not continue to use or access this website. These Terms and Conditions are applicable regardless of how you accessed this website.</p>

<p>Your continued use of this website constitutes your binding acceptance of these Terms and Conditions, including any changes or modifications made hereafter. The updated, on-line version of these Terms and Conditions shall supersede any prior version. By accepting and agreeing to these Terms and Conditions, you represent that you have full power and authority to enter into and perform this agreement and that such ability is not limited or restricted in any way.</p>

<p>Any use of the term “We”, “Us”, or “Our” herein refers to Hiland Dairy Foods.</p>


<h2>Content</h2>

<p>The descriptions of the various products and services contained in this Hiland Dairy website are provided solely for informational purposes. The information and descriptions contained herein are not intended to be complete descriptions of all details applicable to the products and services.</p>

<h2>Children&rsquo;s Online Privacy Protection Act (COPPA) Compliance</h2>

<p>In accordance to the laws outlined by COPPA, this site will not collect personally identifiable information from those visitors under 13 years of age without first obtaining parental consent. This only applies to those parts of the site where the visitor fills out an online form and does not apply to email, voicemail or postal mail we receive from the site. Visitors who provide an age of 12 or under will be asked to provide an adult&rsquo;s email address from whom we can request parental consent for the collection and use of any information received, to the extent that such consent is required by law. A parent or guardian can review personal information submitted by his or her child, have it deleted, and refuse to permit further collection or use of the child&rsquo;s information by contacting us at info@hilanddairy.com. We have established and maintain reasonable procedures to protect the confidentiality, security and integrity of personal information collected from children and we will not require disclosure of more personal information than is reasonably necessary to participate in the underlying activity.</p>

<p>Guests who are 13 to 17 years of age should seek parental consent before providing any personal identifying information on our site.</p>

<h2>Third Party Content</h2>

<p>Our website may contain links to other Internet sites that are not maintained by Hiland Dairy (“Third Party Sites”). These links are provided solely for convenience. Hiland Dairy makes no warranties or representations about the contents of or any products or services offered by such Third Party Sites. We encourage you to read the terms and conditions of Third Party Sites prior to using them.</p>

<p>These other sites may send their own cookies to users, collect data, or solicit personal information. Please keep in mind that whenever you give out personal information online that information can be collected and used by people you don&rsquo;t know. While Hiland Dairy strives to protect their users&rsquo; personal information and privacy, we cannot guarantee the security of any information you disclose online and you do so at your own risk.</p>

<p>Hiland Dairy policy does not extend to anything that is inherent in the operation of the Internet, and therefore beyond the control of Hiland Dairy, and is not to be applied in any manner contrary to applicable law or governmental regulation.</p>

<h2>Security</h2>

<p>The importance of security for all personally identifiable information associated with our guests is of utmost concern to us. We exercise great care in providing secure transmission of your information from your PC to our servers. Unfortunately, no data transmission over the internet can be guaranteed to be 100% secure. As a result, while we strive to protect your personal information, Hiland Dairy can&rsquo;t ensure or warrant the security of any information you transmit to us or from our online products or services, and you do so at your own risk. Once we receive your transmission, we make our best effort to ensure its security on our systems.</p>

<h2>Unsolicited Materials</h2>

<p>Please note that it is our policy not to accept or consider unsolicited creative, production-related or other materials of any kind. We appreciate your cooperation in adhering to this policy.</p>

<p>Notwithstanding the forgoing, you are solely responsible for any information you provide us or other visitors through your use of this website, and you agree that information you provide will be accurate and will not a) infringe any third party&rsquo;s copyright, trademark or other proprietary rights of publicity or privacy; nor b) be defamatory, libelous, otherwise threatening or harassing, or otherwise violate any applicable law or regulation. You may not use the website to conduct any illegal business or activity.</p>

<h2>Limitation of Liability, Waivers and Disclaimers</h2>

<p>You expressly agree that use of this website is at your sole risk. Neither hiland dairy nor any of its parents, affiliates, subsidiaries, assignees, licensees, officers, agents, or employees warrant that this website will be uninterrupted or error free.</p>

<p>The website is offered “as is” and “as available” with no warranties of any kind, either express or implied, including but not limited to warranties of security, title, merchantability or fitness for a particular purpose. All warranties are disclaimed.</p>

<p>Hiland dairy shall have no liability for any incidental, consequential, indirect, special or punitive damages suffered by you or any other party as a result of the operation, inability to operate or malfunction of the website, regardless of whether or not any such parties have been advised of the possibility of such damages.</p>

<p>You waive any claim against Hiland Dairy based upon the quality or availability of the website, regardless of cause.</p>

<p>You acknowledge that any network communication or activity involves resources beyond Hiland Dairy&rsquo;s control, including, without limitation, e-mail, file transfers, and information gathering, and is subject to risks inherent to the Internet and to communications generally. Hiland Dairy makes no representation that any data sent to or from, or residing at, our website or systems are safe from destruction, corruption, misdirection, or service interruption.</p>

<p>You agree that Hiland Dairy shall not be liable for failure to provide services or access to this website during any period in which it cannot perform due to any cause beyond our control, including without limitation natural disaster, power failure, accident, labor controversy, act of God, or the intervention of any government authority.</p>

<h2>Intellectual Property Rights</h2>

<p>Hiland Dairy its subsidiaries and affiliates retain all right, title and interest in the intellectual property, including all copyrights, trademarks and other intellectual property (“Intellectual Property”), contained within and comprising this website. Except as otherwise provided, any an all actions to copy, sell, exploit, distribute, license, publish, transmit, display, perform, modify, derive works from, or otherwise use any of the Intellectual Property without prior written consent is expressly prohibited. Hiland Dairy authorizes users to view or otherwise access the website and its content, including the Intellectual Property, as well as download and/or print portions thereof, as part of users&rsquo; personal, non-commercial use of the website so long as the website and its contents, including the Intellectual Property, are not altered in any way. Nothing in connection with this permitted use shall be construed as granting any rights other than a revocable, non-exclusive, non-transferable, limited license to the user for the purpose recited above.</p>

<h2>General</h2>

<p>No waiver of any term of these Terms and Conditions shall be deemed a further or continuing waiver of such term or any other term. No waiver shall be effective absent expressed written consent by an authorized representative of Hiland Dairy.</p>

<p>We reserve the right to access and disclose any information necessary to comply with applicable laws and lawful government requests, to operate our systems properly, or to protect us from harm or liability.</p>

<p>These Terms and Conditions shall be governed by the laws of the State of Missouri without regard to its conflict of law provisions. If any provision of these Terms and Conditions should be invalid under applicable laws, such provision shall be ineffective to the extent of such conflict without affecting the remaining provisions of these Terms and Conditions.</p>
	</div>
</div>