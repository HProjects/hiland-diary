<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_company; ?>
	</div>
	<div id="content">
<!-- CONTACT US -->
		<h1 class="fontface">Have a question for Hiland Dairy?</h1>
		<p>We&rsquo;d love to hear what you have to say about our dairy products or any other Hiland Dairy-related topic. However, we may already have an answer for you. Please visit our <a href="/company/faqs">FAQs</a> page to find answers to our most frequently asked questions. If you still do not find what you need, please fill out the form below and your comments/questions will be forwarded to a customer service representative.</p>
		<p class="caps gold">Please note Hiland Dairy does not accept online applications for employment on this website. For information on employment opportunities in your area, <a href="/company/about-us">please contact one of our plants</a>.</p>
		
		<?php
			echo $contact_form;
		?>
<!-- =end CONTACT US -->

	</div>
</div>