<div id="content-container">
	<div id="sidebar">
		<?php echo $sidebar_coupons; ?>
	</div>
	<div id="content">
		<h1 class="fontface">Privacy Policy</h1>
		<h2>Hiland Dairy does not sell or transfer any<br /> information you provide us to third parties.</h2>
		<p>We aim to make our site as informative as possible while respecting your privacy. Please be assured that Hiland Dairy uses its best efforts to protect the privacy of its website visitors.</p>
		<p>Some of our site's registration and survey forms require visitors to disclose initial contact information such as name, address, e-mail address and various demographic information. This data is used solely by Hiland Dairy to respond to your requests for information or in relation to specific promotions. We may also use the information to occasionally notify you about important changes to our site, new products and services, special offers and information we think you'll find valuable.</p>
		<p>The demographic information, collected from our site is used solely by Hiland Dairy to improve the site. Hiland Dairy does not send unsolicited e-mail. You may opt out of receiving any promotional material from Hiland Dairy at any time by contacting us at info@hilanddairy.com or writing Hiland Dairy at P.O. Box 2270, Springfield, MO 65801.
		Hiland Dairy may offer links to other sites. If you visit one of these linked sites, you should review that organization's privacy policy and other policies. Hiland Dairy is not responsible for the content or policies of other organizations. While Hiland Dairy will not pass information to any third-party site, we cannot protect your information if you provide it to that site.</p>
		<p>Hiland Dairy reserves the right to use or disclose any information as needed to satisfy any law, regulation or legal request, to protect the integrity of the site, to fulfill your requests, or to cooperate in any law enforcement investigation or an investigation on a matter of public safety. Hiland Dairy also reserves the right to modify the terms of our Privacy Policy. Therefore, it may be necessary for our visitors to check here periodically to review this information. By using our website, you consent to Hiland Dairy's collection and use of this information.</p>
		<p>If you have any comments or questions about our privacy policy, please contact: <br />
		Hiland Dairy, P.O. Box 2270, Springfield, MO 65801.</p>
	</div>
</div>