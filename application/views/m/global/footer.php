<div data-role="footer" id="footer"  data-position="fixed" data-id="footer">
	<div id="footercopyr">
		<div class="footercopyr_l">&copy; 2013 Hiland Dairy.<br />All Rights Reserved.</div>
		<div class="footercopyr_r">
		<a href="http://www.facebook.com/HilandDairy"><img src="/img/mobile_social_facebook.png" width="" height="" border="0" /></a>
		<a href="https://twitter.com/HilandDairy"><img src="/img/mobile_social_twitter.png" width="" height="" border="0" /></a>
		<a href="http://pinterest.com/hilanddairy/"><img src="/img/mobile_social_pinterest.png" width="" height="" border="0" /></a>
		</div> <br clear="all">
	</div>
	<div data-role="navbar">
		<ul>
			<li>
				<a id="home" href="/" data-icon="custom">
					<span>Home</span>
				</a>
			</li>
			<li>
				<a id="coupons" href="/coupons" data-icon="custom" >
					<span>Coupons</span>
				</a>
			</li>
			<li>
				<a id="recipes" href="/recipes" data-icon="custom">
					<span>Recipes</span>
				</a>
			</li>
			<li>
				<a id="products" href="/products" data-icon="custom">
					<span>Products</span>
				</a>
			</li>
			<li>
				<a id="site" href="http://hilanddairy.com/?site=desktop" data-icon="custom">
					<span>Full Site</span>
				</a>
			</li>
		</ul>
	</div>
</div><!-- /footer -->
<script>
	$('a.native-anchor').bind('click', function(ev) {
	      var target = $( $(this).attr('href') ).get(0).offsetTop;
	      $.mobile.silentScroll(target);
	      return false;
	});
</script>
</div>
<!-- /page -->
</body>
</html>